
# Artbox Development Branch

Build Artbox like you would the Dev GIMP version. Artbox is only possible with the good work of the GIMP developers, they make it feasible for an amateur to flavour the ice-cream. **Please support the GIMP Project anyway you can, testing, promoting or coding.**

**Use at your own risk**, it's _not_ GIMP stable. When working in Artbox I use [incremental saving](https://script-fu.github.io/funky/hub/plug-ins/folder/incremental-save/) and an [auto-save](https://script-fu.github.io/funky/hub/plug-ins/folder/almost-autosave/). Artbox is hardcoded to use a different .config folder called 'Artbox'.

See the Artbox [website](https://script-fu.github.io/artbox/) for more information.

@pixelmixer






















