/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ASSET_SAVE_AS_DIALOG_H__
#define __ASSET_SAVE_AS_DIALOG_H__

gchar     * build_resource_name            (const gchar *type_name,
                                            const gchar *name,
                                            gboolean     use_resource_type);

gchar     * create_and_assign_new_basename (GtkWidget   *dialog,
                                            const gchar *resource_type);

GtkWidget * resources_save_as_dialog       (gpointer        data_source,
                                            const gchar    *title,
                                            const gchar    *resource_type,
                                            gboolean        single_save);

#endif  /*  __ASSET_SAVE_AS_DIALOG_H__  */
