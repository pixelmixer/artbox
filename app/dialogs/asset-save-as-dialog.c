/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpbase/gimpbase.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "actions/actions-types.h"

#include "config/gimpguiconfig.h"

#include "core/gimp.h"
#include "core/gimpbrushgenerated.h"
#include "core/gimpbrushpipe.h"
#include "core/gimpcontext.h"
#include "core/gimpdatafactory.h"
#include "core/gimpdynamics.h"
#include "core/gimpgradient.h"
#include "core/gimpmybrush.h"
#include "core/gimppalette.h"
#include "core/gimppattern.h"
#include "core/gimptoolpreset.h"

#include "dialogs-types.h"

#include "widgets/gimpdataeditor.h"

#include "widgets/gimpdatafactoryview.h"
#include "widgets/gimptoolpreseteditor.h"
#include "widgets/gimpuimanager.h"
#include "widgets/gimpwidgets-utils.h"

#include "gimp-intl.h"

#include "asset-save-as-dialog.h"
#include "preferences-dialog.h"
#include "preferences-dialog-utils.h"

typedef enum
{
  DATA_SOURCE_TYPE_UNKNOWN,
  DATA_SOURCE_TYPE_DATA_EDITOR,
  DATA_SOURCE_TYPE_EDITOR,
  DATA_SOURCE_TYPE_DATA_FACTORY_VIEW
} DataSourceType;

static void          append_with_separator               (GString          *builder,
                                                          const gchar      *str);

static GtkWidget *   create_bold_label                   (const gchar      *text);

/* Public */

gchar *
build_resource_name (const gchar *resource_type,
                     const gchar *name,
                     gboolean     use_resource_type)
{
    GString     *name_builder    = g_string_new (NULL);
    const gchar *resource_prefix = use_resource_type ? resource_type : NULL;

    if (use_resource_type)
      append_with_separator (name_builder, resource_prefix);

    /* Append name */
    append_with_separator (name_builder, name);

    /* Free the GString and return the result */
    return g_string_free (name_builder, FALSE);
}

gchar *
create_and_assign_new_basename (GtkWidget   *dialog,
                                const gchar *resource_type)
{
  const gchar *name_entry        = g_object_get_data (G_OBJECT (dialog), "name-entry");
  const gchar *basename          = gtk_entry_get_text (GTK_ENTRY (name_entry));
  gboolean     use_resource_type = FALSE;

  /* Validate basename before proceeding */
  if (!basename || *basename == '\0')
    return NULL;

  return build_resource_name (resource_type, basename, use_resource_type);
}

/* Private */

static DataSourceType
get_data_source_type (gpointer data_source)
{
  if (GIMP_IS_DATA_EDITOR (data_source))
    {
      // Tool-preset editor, gradient editor, dynamics editor, palette editor
      return DATA_SOURCE_TYPE_DATA_EDITOR;
    }
  else if (GIMP_IS_EDITOR (data_source))
    {
      // Tool Options saving a Tool Preset as...
      return DATA_SOURCE_TYPE_EDITOR;
    }
  else if (GIMP_IS_DATA_FACTORY_VIEW (data_source))
    {
      // Pattern saving as...
      return DATA_SOURCE_TYPE_DATA_FACTORY_VIEW;
    }

  return DATA_SOURCE_TYPE_UNKNOWN;
}

static void
append_with_separator (GString     *builder,
                       const gchar *str)
{
  if (str && *str)
  {
      if (builder->len > 0)
        g_string_append_c (builder, '-');

      g_string_append (builder, str);
  }
}

static GtkWidget *
create_bold_label (const gchar *text)
{
  GString *markup = g_string_new ("<b>");
  GtkWidget *label = gtk_label_new (NULL);

  g_string_append (markup, text);
  g_string_append (markup, "</b>");
  gtk_label_set_markup (GTK_LABEL(label), markup->str);
  gtk_widget_set_halign (label, GTK_ALIGN_START);
  gtk_label_set_xalign (GTK_LABEL(label), 0.0);
  g_string_free (markup, TRUE);

  return label;
}

static GtkWidget *
create_duplicate_options_section (GimpToolPreset *preset,
                                  GtkWidget      *dialog)
{
  Gimp        *gimp    = g_object_get_data (G_OBJECT (dialog), "gimp");
  GimpContext *context = gimp_get_user_context (gimp);
  const gchar *checkbox_header_text = _("Choose Resources to Save");
  GtkWidget   *header_label;
  GtkWidget   *checkbox_grid;
  GtkWidget   *checkbox;
  GtkWidget   *container;
  const gchar *duplicate_tooltip = _("When the resource is saved, <i>duplicate,\n"
                                     "rename</i>, and set as the active resource.");

  GimpContextPropMask serialize_props =
    gimp_context_get_serialize_properties (GIMP_CONTEXT (preset->tool_options));

  /* Define mapping between resource properties and context property masks */
  struct
    {
      const gchar        *label;
      gboolean            active;
      GimpContextPropMask mask;
      const gchar        *property;
    } checkboxes_info[] =
    {
      { "Brush",    preset->resource_save_brush,    GIMP_CONTEXT_PROP_MASK_BRUSH,    "resource-save-brush" },
      { "Dynamics", preset->resource_save_dynamics, GIMP_CONTEXT_PROP_MASK_DYNAMICS, "resource-save-dynamics" },
      { "Gradient", preset->resource_save_gradient, GIMP_CONTEXT_PROP_MASK_GRADIENT, "resource-save-gradient" },
      { "Palette",  preset->resource_save_palette,  GIMP_CONTEXT_PROP_MASK_PALETTE,  "resource-save-palette" },
      { "Pattern",  preset->resource_save_pattern,  GIMP_CONTEXT_PROP_MASK_PATTERN,  "resource-save-pattern" },
    };

  header_label = create_bold_label (checkbox_header_text);
  gtk_widget_set_visible (GTK_WIDGET (header_label), TRUE);
  checkbox_grid = gtk_grid_new ();
  gtk_widget_set_visible (GTK_WIDGET (checkbox_grid), TRUE);
  gtk_grid_set_row_spacing (GTK_GRID (checkbox_grid), 5);
  gtk_grid_set_column_spacing (GTK_GRID (checkbox_grid), 60);

  for (guint i = 0; i < G_N_ELEMENTS (checkboxes_info); i++)
    {

      checkbox = gimp_prop_check_button_new (G_OBJECT (context->tool_preset),
                                             checkboxes_info[i].property,
                                             checkboxes_info[i].label);

      /* Set active state and sensitivity based on the associated context property mask */
      if ((serialize_props & checkboxes_info[i].mask) == 0)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox),FALSE);

      gtk_widget_set_tooltip_markup (checkbox, duplicate_tooltip);

      gtk_widget_set_sensitive (checkbox, (serialize_props & checkboxes_info[i].mask) != 0);
      gtk_grid_attach (GTK_GRID (checkbox_grid), checkbox, i % 5, 0, 1, 1);
      gtk_widget_set_visible (GTK_WIDGET (checkbox), TRUE);
    }

  container = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_pack_start (GTK_BOX (container), header_label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (container), checkbox_grid, FALSE, FALSE, 0);
  gtk_widget_set_visible (GTK_WIDGET (container), TRUE);

  return container;
}

static gboolean
retrieve_gimp_from_editor (GimpEditor *editor, Gimp **gimp_out)
{
  GimpUIManager *ui_manager;

  g_return_val_if_fail (editor != NULL, FALSE);

  ui_manager = gimp_editor_get_ui_manager (editor);
  if (!ui_manager || !ui_manager->gimp)
    {
      g_warning ("retrieve_gimp_from_editor: Failed to get UI Manager or Gimp instance.");
      return FALSE;
    }

  *gimp_out = ui_manager->gimp;
  return TRUE;
}

static gboolean
handle_data_source (gpointer         data_source_ptr,
                    Gimp           **gimp_out,
                    GimpData       **data_out,
                    GimpToolPreset **preset_out)
{
  GimpEditor    *editor = NULL;
  DataSourceType data_source_type;

  if (!data_source_ptr)
    {
      g_warning ("handle_data_source: data_source_ptr is NULL.");
      return FALSE;
    }

  data_source_type = get_data_source_type (data_source_ptr);

  switch (data_source_type)
    {
    case DATA_SOURCE_TYPE_DATA_EDITOR:
      {
        GimpDataEditor *data_editor = GIMP_DATA_EDITOR (data_source_ptr);

        *data_out = data_editor->data;
        editor = GIMP_EDITOR (data_source_ptr);
        break;
      }

    case DATA_SOURCE_TYPE_EDITOR:
      {
        editor = GIMP_EDITOR (data_source_ptr);
        break;
      }

    case DATA_SOURCE_TYPE_DATA_FACTORY_VIEW:
      {
        GimpDataFactoryView *factory_view = GIMP_DATA_FACTORY_VIEW (data_source_ptr);
        GimpDataFactory     *factory      = gimp_data_factory_view_get_data_factory (factory_view);

        *gimp_out = gimp_data_factory_get_gimp (factory);
        break;
      }

    default:
      {
        g_warning ("handle_data_source: Unknown data source type.");
        return FALSE;
      }
    }

  if (!*gimp_out && editor)
    {
      if (!retrieve_gimp_from_editor (editor, gimp_out))
        {
          return FALSE;
        }
    }

  if (*gimp_out)
    {
      GimpContext *user_context = gimp_get_user_context (*gimp_out);
      if (!user_context)
        {
          g_warning ("handle_data_source: Failed to get user context from Gimp.");
          return FALSE;
        }

      *preset_out = gimp_context_get_tool_preset (user_context);
    }

  return TRUE;
}

/* Create the main dialog with provided title and default responses */
static GtkWidget *
create_main_dialog (const gchar *dialog_title)
{
  GtkWidget *dialog;

  dialog = gtk_dialog_new_with_buttons (dialog_title,
                                        NULL,
                                        GTK_DIALOG_MODAL,
                                        "_Cancel", GTK_RESPONSE_CANCEL,
                                        "_Save", GTK_RESPONSE_OK,
                                        NULL);
  gtk_window_set_default_size (GTK_WINDOW (dialog), 400, -1);
  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

  return dialog;
}

static GtkWidget *
create_main_vbox (GtkWidget *dialog)
{
  GtkWidget *content_area;
  GtkWidget *main_vbox;

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  main_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
  gtk_container_set_border_width (GTK_CONTAINER (main_vbox), 10);
  gtk_container_add (GTK_CONTAINER (content_area), main_vbox);
  gtk_widget_set_visible (GTK_WIDGET (main_vbox), TRUE);

  return main_vbox;
}

static void
add_separator (GtkWidget *parent)
{
  GtkWidget *separator;

  separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_widget_set_size_request (separator, -1, 2);
  gtk_box_pack_start (GTK_BOX (parent), separator, FALSE, FALSE, 2);
  gtk_widget_set_visible (GTK_WIDGET (separator), TRUE);
}

static GtkWidget *
add_input_grid_section (Gimp       *gimp,
                        GtkWidget **entry)
{
  GimpContext *context = gimp_get_user_context (gimp);
  GtkWidget   *grid    = gtk_grid_new ();
  GtkWidget   *label   = gtk_label_new (_("Name"));
  GtkWidget   *checkbutton;

  gtk_grid_set_row_spacing (GTK_GRID (grid), 5);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 10);
  gtk_widget_set_visible (GTK_WIDGET (grid), TRUE);

  gtk_widget_set_halign (label, GTK_ALIGN_START);
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);
  gtk_widget_set_visible (label, TRUE);

  *entry = gtk_entry_new ();
  gtk_widget_set_hexpand (*entry, TRUE);
  gtk_grid_attach (GTK_GRID (grid), *entry, 1, 0, 1, 1);
  gtk_widget_set_visible (*entry, TRUE);

  checkbutton = gimp_prop_check_button_new (G_OBJECT (context->tool_preset),
                                            "savename-includes-type",
                                            _("Include type of resource"));

  gtk_grid_attach (GTK_GRID (grid), checkbutton, 0, 1, 2, 1);
  gtk_widget_set_visible (checkbutton, TRUE);

  return grid;
}

/* Add controls for additional save folders (package resource paths) */
static void
add_additional_save_folders (GtkWidget *main_vbox,
                            Gimp       *gimp,
                            GtkWidget  *dialog)
{
  GtkWidget     *grid;
  GimpGuiConfig *gui_config = GIMP_GUI_CONFIG (gimp->config);
  const gchar   *checkbox_header_text = _("Save Locations and Resource Tags");
  GtkWidget     *header_label = create_bold_label (checkbox_header_text);

  static const struct
  {
    const gchar *property_name;
    const gchar *dialog_title;
    const gchar *checkbox_property;
    const gchar *checkbox_label;
    const gchar *tag_folder;
  } dirs[] =
  {
    {
      "package-save-path-default",
      N_("Select a Folder for Package Resource Files"),
      "package-use-save-path-default",
      "",
      "package-tag-path-default",
    },
    {
      "package-save-path-a",
      N_("Select a Folder for Package Resource Files"),
      "package-use-save-path-a",
      "",
      "package-tag-path-a",
    },
    {
      "package-save-path-b",
      N_("Select a Folder for Package Resource Files"),
      "package-use-save-path-b",
      "",
      "package-tag-path-b",
    },
    {
      "package-save-path-c",
      N_("Select a Folder for Package Resource Files"),
      "package-use-save-path-c",
      "",
      "package-tag-path-c",
    },
  };

  gtk_box_pack_start (GTK_BOX (main_vbox), header_label, FALSE, FALSE, 2);
  gtk_widget_set_visible (GTK_WIDGET (header_label), TRUE);

  grid = prefs_grid_new (GTK_CONTAINER (main_vbox));

for (gint i = 0; i < G_N_ELEMENTS (dirs); i++)
  {
    GtkWidget *checkbox;
    GtkWidget *chooser;
    GtkWidget *entry;

    checkbox = gimp_prop_check_button_new (G_OBJECT (gui_config),
                                           dirs[i].checkbox_property,
                                           dirs[i].checkbox_label);
    gtk_grid_attach (GTK_GRID (grid), checkbox, 0, i, 1, 1);
    gtk_widget_set_visible (GTK_WIDGET (checkbox), TRUE);
    g_object_set_data (G_OBJECT (dialog), dirs[i].checkbox_property, checkbox);

    chooser = gimp_prop_file_chooser_button_new (G_OBJECT (gui_config),
                                                 dirs[i].property_name,
                                                 "Save Location",
                                                 GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
    gtk_grid_attach (GTK_GRID (grid), chooser, 1, i, 1, 1);
    gtk_widget_set_visible (GTK_WIDGET (chooser), TRUE);

    entry = gimp_prop_entry_new (G_OBJECT (gui_config), dirs[i].tag_folder, -1);
    gtk_grid_attach (GTK_GRID (grid), entry, 2, i, 1, 1);
    gtk_widget_set_visible (GTK_WIDGET (entry), TRUE);
    gtk_widget_set_hexpand (entry, TRUE);

    g_object_bind_property (checkbox, "active",
                            chooser, "sensitive",
                            G_BINDING_SYNC_CREATE);
    g_object_bind_property (checkbox, "active",
                            entry, "sensitive",
                            G_BINDING_SYNC_CREATE);
  }
}

static void
store_dialog_data (GtkWidget    *dialog,
                   gboolean      single_save,
                   GtkWidget    *name_entry,
                   GimpData     *data,
                   Gimp         *gimp,
                   const gchar  *resource_type)
{
  g_object_set_data (G_OBJECT (dialog), "single-type", GINT_TO_POINTER (single_save));
  g_object_set_data (G_OBJECT (dialog), "name-entry", name_entry);
  g_object_set_data (G_OBJECT (dialog), "data", data);
  g_object_set_data (G_OBJECT (dialog), "gimp", gimp);

  g_object_set_data_full (G_OBJECT (dialog), "resource-type",
                          g_strdup (resource_type), g_free);
}

/* Main function to create the asset save-as dialog */
GtkWidget *
resources_save_as_dialog (gpointer     data_source,
                          const gchar *dialog_title,
                          const gchar *resource_type,
                          gboolean     single_save)
{
  GimpData       *data    = NULL;
  Gimp           *gimp    = NULL;
  GimpToolPreset *preset  = NULL;

  GtkWidget *dialog;
  GtkWidget *main_vbox;
  GtkWidget *name_entry;
  GtkWidget *grid;

  /* The dialog is called from a number of possible resource code areas */
  if (!handle_data_source (data_source, &gimp, &data, &preset))
    {
      g_warning ("resources_save_as_dialog: Failed to handle data source.");
      return NULL;
    }

  dialog = create_main_dialog (dialog_title);
  main_vbox = create_main_vbox (dialog);

  grid = add_input_grid_section (gimp, &name_entry);
  gtk_box_pack_start (GTK_BOX (main_vbox), grid, FALSE, FALSE, 2);
  gtk_widget_set_visible (GTK_WIDGET (grid), TRUE);

  add_separator (main_vbox);

  store_dialog_data (dialog, single_save,
                     name_entry,
                     data,
                     gimp,
                     resource_type);

  /* Simple save */
  if (single_save)
    {
      return dialog;
    }

  /* Called from the Tool Preset Editor to save multiple resources */
  if (!single_save)
    {
      GtkWidget *duplicate_section;

      duplicate_section = create_duplicate_options_section (preset, dialog);
      gtk_box_pack_start (GTK_BOX (main_vbox), duplicate_section, FALSE, FALSE, 2);
      add_separator (main_vbox);
      add_additional_save_folders (main_vbox, gimp, dialog);
    }

  return dialog;
}



