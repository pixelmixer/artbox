/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995-1997 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpcolor/gimpcolor.h"
#include "libgimpconfig/gimpconfig.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "dialogs-types.h"

#include "core/gimp.h"
#include "core/gimpcontext.h"
#include "core/gimptoolpreset.h"

#include "config/gimprc.h"

#include "widgets/gimpcolorpanel.h"
#include "widgets/gimppropwidgets.h"
#include "widgets/gimpwidgets-constructors.h"

#include "preferences-dialog-utils.h"

typedef struct
{
  GtkWidget   *file_chooser;
  GtkWidget   *use_folder_box;
  gboolean     is_install_dir;
  gboolean     is_user_dir;
} CallbackData;

gboolean
is_user_dir (const gchar *property_name)
{
  if (strcmp (property_name, "package-load-path-user") == 0)
    {
      return TRUE;
    }

  return FALSE;
}

gboolean
is_install_dir (const gchar *property_name)
{
  if (strcmp (property_name, "package-load-path-install") == 0)
    {
      return TRUE;
    }

  return FALSE;
}

GtkWidget *
prefs_frame_new (const gchar  *label,
                 GtkContainer *parent,
                 gboolean      expand)
{
  GtkWidget *frame;
  GtkWidget *vbox;

  frame = gimp_frame_new (label);

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  gtk_widget_show (vbox);

  if (GTK_IS_BOX (parent))
    gtk_box_pack_start (GTK_BOX (parent), frame, expand, expand, 0);
  else
    gtk_container_add (parent, frame);

  gtk_widget_show (frame);

  return vbox;
}

GtkWidget *
prefs_grid_new (GtkContainer *parent)
{
  GtkWidget *grid;

  grid = gtk_grid_new ();

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 6);

  if (GTK_IS_BOX (parent))
    gtk_box_pack_start (GTK_BOX (parent), grid, FALSE, FALSE, 0);
  else
    gtk_container_add (parent, grid);

  gtk_widget_show (grid);

  return grid;
}

GtkWidget *
prefs_hint_box_new (const gchar  *icon_name,
                    const gchar  *text)
{
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *label;

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);

  image = gtk_image_new_from_icon_name (icon_name, GTK_ICON_SIZE_BUTTON);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
  gtk_widget_show (image);

  label = gtk_label_new (text);
  gimp_label_set_attributes (GTK_LABEL (label),
                             PANGO_ATTR_STYLE, PANGO_STYLE_ITALIC,
                             -1);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);

  gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
  gtk_widget_show (label);

  gtk_widget_show (hbox);

  return hbox;
}

GtkWidget *
prefs_button_add (const gchar *icon_name,
                  const gchar *label,
                  GtkBox      *box)
{
  GtkWidget *button;

  button = gimp_icon_button_new (icon_name, label);
  gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  return button;
}

GtkWidget *
prefs_check_button_add (GObject     *config,
                        const gchar *property_name,
                        const gchar *label,
                        GtkBox      *vbox)
{
  GtkWidget *button;

  button = gimp_prop_check_button_new (config, property_name, label);

  if (button)
    gtk_box_pack_start (vbox, button, FALSE, FALSE, 0);

  return button;
}

GtkWidget *
prefs_switch_add (GObject      *config,
                  const gchar  *property_name,
                  const gchar  *label,
                  GtkBox       *vbox,
                  GtkSizeGroup *group,
                  GtkWidget   **switch_out)
{
  GtkWidget *box;
  GtkWidget *plabel;

  box = gimp_prop_switch_new (config, property_name, label, &plabel,
                              switch_out);

  if (!box)
    return NULL;

  gtk_box_pack_start (vbox, box, FALSE, FALSE, 0);
  gtk_label_set_xalign (GTK_LABEL (plabel), 0.0);
  if (group)
    gtk_size_group_add_widget (group, plabel);

  return box;
}

GtkWidget *
prefs_check_button_add_with_icon (GObject      *config,
                                  const gchar  *property_name,
                                  const gchar  *label,
                                  const gchar  *icon_name,
                                  GtkBox       *vbox,
                                  GtkSizeGroup *group)
{
  GtkWidget *button;
  GtkWidget *hbox;
  GtkWidget *image;

  button = gimp_prop_check_button_new (config, property_name, label);
  if (! button)
    return NULL;

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
  gtk_box_pack_start (vbox, hbox, FALSE, FALSE, 0);
  gtk_widget_show (hbox);

  image = gtk_image_new_from_icon_name (icon_name, GTK_ICON_SIZE_BUTTON);
  g_object_set (image,
                "margin-start",  2,
                "margin-end",    2,
                "margin-top",    2,
                "margin-bottom", 2,
                NULL);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
  gtk_widget_show (image);

  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

  if (group)
    gtk_size_group_add_widget (group, image);

  return button;
}

GtkWidget *
prefs_widget_add_aligned (GtkWidget    *widget,
                          const gchar  *text,
                          GtkGrid      *grid,
                          gint          grid_top,
                          gboolean      left_align,
                          GtkSizeGroup *group)
{
  GtkWidget *label = gimp_grid_attach_aligned (grid, 0, grid_top,
                                               text, 0.0, 0.5,
                                               widget, 1);
  if (group)
    gtk_size_group_add_widget (group, label);

  if (left_align == TRUE)
    gtk_widget_set_halign (widget, GTK_ALIGN_START);

  return label;
}

GtkWidget *
prefs_color_button_add (GObject      *config,
                        const gchar  *property_name,
                        const gchar  *label,
                        const gchar  *title,
                        GtkGrid      *grid,
                        gint          grid_top,
                        GtkSizeGroup *group,
                        GimpContext  *context)
{
  GtkWidget  *button;
  GParamSpec *pspec;
  gboolean    has_alpha;

  pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (config),
                                        property_name);

  has_alpha = gimp_param_spec_color_has_alpha (pspec);

  button = gimp_prop_color_button_new (config, property_name,
                                       title,
                                       PREFS_COLOR_BUTTON_WIDTH,
                                       PREFS_COLOR_BUTTON_HEIGHT,
                                       has_alpha ?
                                       GIMP_COLOR_AREA_SMALL_CHECKS :
                                       GIMP_COLOR_AREA_FLAT);

  if (button)
    {
      if (context)
        gimp_color_panel_set_context (GIMP_COLOR_PANEL (button), context);

      prefs_widget_add_aligned (button, label, grid, grid_top, TRUE, group);
    }

  return button;
}

GtkWidget *
prefs_entry_add (GObject      *config,
                 const gchar  *property_name,
                 const gchar  *label,
                 GtkGrid      *grid,
                 gint          grid_top,
                 GtkSizeGroup *group)
{
  GtkWidget *entry = gimp_prop_entry_new (config, property_name, -1);

  if (entry)
    prefs_widget_add_aligned (entry, label, grid, grid_top, FALSE, group);

  return entry;
}

GtkWidget *
prefs_spin_button_add (GObject      *config,
                       const gchar  *property_name,
                       gdouble       step_increment,
                       gdouble       page_increment,
                       gint          digits,
                       const gchar  *label,
                       GtkGrid      *grid,
                       gint          grid_top,
                       GtkSizeGroup *group)
{
  GtkWidget *button = gimp_prop_spin_button_new (config, property_name,
                                                 step_increment,
                                                 page_increment,
                                                 digits);

  if (button)
    prefs_widget_add_aligned (button, label, grid, grid_top, TRUE, group);

  return button;
}

GtkWidget *
prefs_memsize_entry_add (GObject      *config,
                         const gchar  *property_name,
                         const gchar  *label,
                         GtkGrid      *grid,
                         gint          grid_top,
                         GtkSizeGroup *group)
{
  GtkWidget *entry = gimp_prop_memsize_entry_new (config, property_name);

  if (entry)
    prefs_widget_add_aligned (entry, label, grid, grid_top, TRUE, group);

  return entry;
}

GtkWidget *
prefs_file_chooser_button_add (GObject      *config,
                               const gchar  *property_name,
                               const gchar  *label,
                               const gchar  *dialog_title,
                               GtkGrid      *grid,
                               gint          grid_top,
                               GtkSizeGroup *group)
{
  GtkWidget *button;

  button = gimp_prop_file_chooser_button_new (config, property_name,
                                              dialog_title,
                                              GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);

  if (button)
    prefs_widget_add_aligned (button, label, grid, grid_top, FALSE, group);

  return button;
}

static gchar *
append_new_path (const gchar *resource_path,
                 const gchar *new_path_suffix)
{
  gchar **paths = g_strsplit (resource_path, ":", -1);
  gboolean exists = FALSE;

  for (gchar **path = paths; *path; ++path)
    {
      if (g_strcmp0 (*path, new_path_suffix) == 0)
        {
          exists = TRUE;
          break;
        }
    }
  g_strfreev (paths);

  if (!exists)
    {
      // Only add ':' if resource_path is non-empty
      if (resource_path && *resource_path != '\0')
        {
          return g_strconcat (resource_path, ":", new_path_suffix, NULL);
        }
      else // If resource_path is empty, return only the new_path_suffix
        {
          return g_strdup (new_path_suffix);
        }
    }

  return g_strdup (resource_path);
}

static gchar *
remove_path_if_exists (const gchar *resource_path,
                       const gchar *remove_path)
{
  gchar  **paths = g_strsplit (resource_path, ":", -1);
  gboolean path_found = FALSE;
  GString *updated_path = g_string_new ("");

  for (gchar **path = paths; *path; ++path)
    {
      if (g_strcmp0 (*path, remove_path) != 0)
        {
          if (updated_path->len > 0)
            {
              g_string_append_c (updated_path, ':');
            }
          g_string_append (updated_path, *path);
        }
      else
        {
          path_found = TRUE;
        }
    }

  g_strfreev (paths);

  if (path_found)
    {
      return g_string_free (updated_path, FALSE);
    }

  g_string_free (updated_path, TRUE);
  return g_strdup (resource_path);
}

static gchar *
artbox_config_build_data_only_path (const gchar *name)
{
  return g_strconcat ("${gimp_data_dir}", G_DIR_SEPARATOR_S, name, NULL);
}

static gchar *
artbox_config_build_user_only_path (const gchar *name)
{
  return g_strconcat ("${gimp_dir}", G_DIR_SEPARATOR_S, name, NULL);
}

static void
update_single_gimp_resource_path (Gimp        *gimp,
                                  const gchar *resource,
                                  gchar       *resource_path,
                                  const gchar *path_suffix,
                                  gboolean     add_path,
                                  gboolean     is_user_dir,
                                  gboolean     is_install_dir)
{

  GimpContext    *context     = gimp_get_user_context (gimp);
  GimpToolPreset *tool_preset = gimp_context_get_tool_preset (context);
  gchar          *new_path;

  if (!gimp || !resource || !resource_path || !path_suffix)
    {
      g_warning ("Invalid arguments in update_single_gimp_resource_path()");
      return;
    }

  if (add_path)
    {
      new_path = append_new_path (resource_path, path_suffix);
    }
  else
    {
      new_path = remove_path_if_exists (resource_path, path_suffix);
    }

  g_object_set (GIMP_CONFIG (gimp->edit_config), resource, new_path, NULL);

  if (GIMP_IS_TOOL_PRESET (tool_preset))
  {
    gimp_context_set_tool_preset (context, tool_preset);
    gimp_context_tool_preset_changed (context);
  }

  g_free (new_path);
  return;
}

/* Function to determine if writable path should be added */
static gboolean
should_add_writable_path (gchar   *resource_path_writable,
                             gboolean is_install_dir,
                             gboolean add_path)
{
  return (resource_path_writable && !is_install_dir);
}

/* There may be two path strings, a non-writable and a writable */
static void
update_gimp_resource_path (Gimp        *gimp,
                           const gchar *resource,
                           const gchar *resource_prop,
                           gchar       *resource_path,
                           gchar       *resource_path_writable,
                           const gchar *path_suffix,
                           gboolean     add_path,
                           gboolean     is_user_dir,
                           gboolean     is_install_dir)
{
  gboolean debug_pref_path = FALSE;

  if (is_user_dir) path_suffix = artbox_config_build_user_only_path (resource);
  if (is_install_dir) path_suffix = artbox_config_build_data_only_path (resource);

  /* Add or remove the non-writable string */
  if (resource_path)
    {
      if (debug_pref_path)
        {
          g_print ("Add/Remove the resource path:\n");
          g_print ("  resource            : %s\n", resource_prop);
          g_print ("  active resource_path: %s\n", resource_path);
          g_print ("  %s path         : %s\n",
                    add_path ? "add" : "remove",
                    path_suffix ? path_suffix : "(null)");
          g_print ("  is_user_dir   : %s\n", is_user_dir ? "TRUE" : "FALSE");
          g_print ("  is_install_dir: %s\n", is_install_dir ? "TRUE" : "FALSE");
        }

      update_single_gimp_resource_path (gimp,
                                        resource_prop,
                                        resource_path,
                                        path_suffix,
                                        add_path,
                                        is_user_dir,
                                        is_install_dir);
    }

  /* Enable the writable state by adding or removing an extra string to the gimprc*/
  if (should_add_writable_path (resource_path_writable, is_install_dir, add_path))
    {
      gchar *writable_resource = g_strconcat (resource_prop, "-writable", NULL);

      update_single_gimp_resource_path (gimp,
                                        writable_resource,
                                        resource_path_writable,
                                        path_suffix,
                                        add_path,
                                        is_user_dir,
                                        is_install_dir);

      g_free (writable_resource);
    }
}

static void
update_resource_paths (GObject    *config,
                       GParamSpec *pspec,
                       gpointer    user_data)
{
  Gimp          *gimp;
  CallbackData  *dialog_data = (CallbackData *) user_data;
  GimpGuiConfig *gui_config  = GIMP_GUI_CONFIG (config);
  const gchar   *folder_path;
  gboolean       use_folder;
  gboolean       debug_path_edits = FALSE;

  g_object_get (config, "gimp", &gimp, NULL);
  g_return_if_fail (gimp != NULL);

  folder_path = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog_data->file_chooser));

  if (!folder_path)
      return;

  /* Extract additional settings */
  use_folder  = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog_data->use_folder_box));

  /* Update each resource path as needed */

  /* Presets */
  if (gimp->edit_config->tool_preset_path  && gui_config->update_presets_paths)
    {
      if (debug_path_edits)
        {
          g_print ("existing config path       : %s\n", gimp->edit_config->tool_preset_path);
          g_print ("existing config write path : %s\n", gimp->edit_config->tool_preset_path_writable);
        }

      update_gimp_resource_path (gimp,
                                 "tool-presets",
                                 "tool-preset-path",
                                 gimp->edit_config->tool_preset_path,
                                 gimp->edit_config->tool_preset_path_writable,
                                 g_build_filename (folder_path, "tool-presets", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
      if (debug_path_edits)
        {
          g_print ("updated config path        : %s\n", gimp->edit_config->tool_preset_path);
          g_print ("updated config write path  : %s\n\n", gimp->edit_config->tool_preset_path_writable);
        }
    }

  /* Brushes */
  if (gimp->edit_config->brush_path && gui_config->update_brushes_paths)
    {
      update_gimp_resource_path (gimp,
                                 "brushes",
                                 "brush-path",
                                 gimp->edit_config->brush_path,
                                 gimp->edit_config->brush_path_writable,
                                 g_build_filename (folder_path, "brushes", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
    }

  /* Dynamics */
  if (gimp->edit_config->dynamics_path && gui_config->update_dynamics_paths)
    {
      update_gimp_resource_path (gimp,
                                 "dynamics",
                                 "dynamics-path",
                                 gimp->edit_config->dynamics_path,
                                 gimp->edit_config->dynamics_path_writable,
                                 g_build_filename (folder_path, "dynamics", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
    }

  /* Gradients */
  if (gimp->edit_config->gradient_path && gui_config->update_gradients_paths)
    {
      update_gimp_resource_path (gimp,
                                 "gradients",
                                 "gradient-path",
                                 gimp->edit_config->gradient_path,
                                 gimp->edit_config->gradient_path_writable,
                                 g_build_filename (folder_path, "gradients", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
    }

  /* Patterns */
  if (gimp->edit_config->pattern_path && gui_config->update_patterns_paths)
    {
      update_gimp_resource_path (gimp,
                                 "patterns",
                                 "pattern-path",
                                 gimp->edit_config->pattern_path,
                                 gimp->edit_config->pattern_path_writable,
                                 g_build_filename (folder_path, "patterns", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
    }

  /* Palettes */
  if (gimp->edit_config->palette_path && gui_config->update_palettes_paths)
    {
      update_gimp_resource_path (gimp,
                                 "palettes",
                                 "palette-path",
                                 gimp->edit_config->palette_path,
                                 gimp->edit_config->palette_path_writable,
                                 g_build_filename (folder_path, "palettes", NULL),
                                 use_folder,
                                 dialog_data->is_user_dir,
                                 dialog_data->is_install_dir);
    }
}

GtkWidget *
prefs_package_dir_chooser_button (GObject      *gimp_config,
                                  const gchar  *property_name,
                                  const gchar  *label,
                                  GtkGrid      *grid,
                                  gint          grid_top,
                                  GtkWidget    *use_folder_box,
                                  const gchar  *checkbox_property)
{
  GtkWidget    *file_chooser;
  CallbackData *callback_data;
  char         *signal_prop_name, *signal_prop_box_name;
  gboolean      system_resource = FALSE;

  g_object_freeze_notify (G_OBJECT (gimp_config));

  file_chooser = gimp_prop_file_chooser_button_new (gimp_config, property_name, "",
                                                    GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);

  if (! file_chooser)
    {
      g_warning ("Failed to create file chooser button for property '%s'.",
                 property_name);
      return NULL;
    }

  if (is_install_dir (property_name))
    {
      const gchar *data_directory = gimp_data_directory ();
      gchar       *tooltip_text   = g_strdup_printf ("Installed resources path: %s", data_directory);

      gtk_file_chooser_set_current_folder_file (GTK_FILE_CHOOSER (file_chooser),
                                                gimp_data_directory_file (NULL),
                                                NULL);
      gtk_widget_set_tooltip_text (file_chooser, tooltip_text);
      system_resource = TRUE;
      g_free (tooltip_text);
    }
  else if (is_user_dir (property_name))
    {
      const gchar *user_directory = gimp_directory ();
      gchar       *tooltip_text   = g_strdup_printf ("Default user path: %s", user_directory);

      gtk_file_chooser_set_current_folder_file (GTK_FILE_CHOOSER (file_chooser),
                                                gimp_directory_file (NULL),
                                                NULL);
      gtk_widget_set_tooltip_text (file_chooser, tooltip_text);
      system_resource = TRUE;
      g_free (tooltip_text);
    }

  prefs_widget_add_aligned (file_chooser, label, grid, grid_top, FALSE, NULL);

  callback_data = g_new (CallbackData, 1);
  callback_data->file_chooser   = file_chooser;
  callback_data->use_folder_box = use_folder_box;
  callback_data->is_install_dir = is_install_dir (property_name);
  callback_data->is_user_dir    = is_user_dir (property_name);

  /* Attach callback data to the file chooser */
  g_object_set_data_full (G_OBJECT (file_chooser),
                          "callback-data",               /* Key */
                          callback_data,                 /* Value */
                          (GDestroyNotify) g_free);      /* Cleanup function */

  /* Extra data for the reset path buttons in the GUI */
  g_object_set_data (G_OBJECT (file_chooser),
                    "config",
                    gimp_config);

  g_object_set_data (G_OBJECT (file_chooser),
                    "use-checkbox",
                      use_folder_box);

  g_object_set_data (G_OBJECT (file_chooser),
                     "is-system-resource",
                     GUINT_TO_POINTER (system_resource));

  g_object_set_data_full (G_OBJECT (file_chooser),
                          "package-path",
                          g_strdup (property_name),
                          g_free);

  signal_prop_name = g_strdup_printf ("notify::%s", property_name);
  g_signal_connect (gimp_config, signal_prop_name,
                        G_CALLBACK (update_resource_paths), callback_data);
  g_free (signal_prop_name);

  g_signal_connect (gimp_config, "notify::update-presets-paths",
                    G_CALLBACK (update_resource_paths), callback_data);
  g_signal_connect (gimp_config, "notify::update-brushes-paths",
                    G_CALLBACK (update_resource_paths), callback_data);
  g_signal_connect (gimp_config, "notify::update-dynamics-paths",
                    G_CALLBACK (update_resource_paths), callback_data);
  g_signal_connect (gimp_config, "notify::update-gradients-paths",
                    G_CALLBACK (update_resource_paths), callback_data);
  g_signal_connect (gimp_config, "notify::update-patterns-paths",
                    G_CALLBACK (update_resource_paths), callback_data);
  g_signal_connect (gimp_config, "notify::update-palettes-paths",
                    G_CALLBACK (update_resource_paths), callback_data);

  signal_prop_box_name = g_strdup_printf ("notify::%s", checkbox_property);
  g_signal_connect (gimp_config, signal_prop_box_name,
                    G_CALLBACK (update_resource_paths), callback_data);
  g_free (signal_prop_box_name);

  g_object_thaw_notify (G_OBJECT (gimp_config));

  /* Perhaps the resource paths should be updated here with the gathered data */
  update_resource_paths (G_OBJECT (gimp_config), NULL, callback_data);

  return file_chooser;
}

GtkWidget *
prefs_enum_combo_box_add (GObject      *config,
                          const gchar  *property_name,
                          gint          minimum,
                          gint          maximum,
                          const gchar  *label,
                          GtkGrid      *grid,
                          gint          grid_top,
                          GtkSizeGroup *group)
{
  GtkWidget *combo = gimp_prop_enum_combo_box_new (config, property_name,
                                                   minimum, maximum);

  if (combo)
    prefs_widget_add_aligned (combo, label, grid, grid_top, FALSE, group);

  return combo;
}

GtkWidget *
prefs_boolean_combo_box_add (GObject      *config,
                             const gchar  *property_name,
                             const gchar  *true_text,
                             const gchar  *false_text,
                             const gchar  *label,
                             GtkGrid      *grid,
                             gint          grid_top,
                             GtkSizeGroup *group)
{
  GtkWidget *combo = gimp_prop_boolean_combo_box_new (config, property_name,
                                                      true_text, false_text);

  if (combo)
    prefs_widget_add_aligned (combo, label, grid, grid_top, FALSE, group);

  return combo;
}

#ifdef HAVE_ISO_CODES
GtkWidget *
prefs_language_combo_box_add (GObject      *config,
                              const gchar  *property_name,
                              GtkBox       *vbox)
{
  GtkWidget *combo = gimp_prop_language_combo_box_new (config, property_name);

  if (combo)
    gtk_box_pack_start (vbox, combo, FALSE, FALSE, 0);

  return combo;
}
#endif

GtkWidget *
prefs_profile_combo_box_add (GObject      *config,
                             const gchar  *property_name,
                             GtkListStore *profile_store,
                             const gchar  *dialog_title,
                             const gchar  *label,
                             GtkGrid      *grid,
                             gint          grid_top,
                             GtkSizeGroup *group,
                             GObject      *profile_path_config,
                             const gchar  *profile_path_property_name)
{
  GtkWidget *combo = gimp_prop_profile_combo_box_new (config,
                                                      property_name,
                                                      profile_store,
                                                      dialog_title,
                                                      profile_path_config,
                                                      profile_path_property_name);

  if (combo)
    prefs_widget_add_aligned (combo, label, grid, grid_top, FALSE, group);

  return combo;
}

GtkWidget *
prefs_compression_combo_box_add (GObject      *config,
                                 const gchar  *property_name,
                                 const gchar  *label,
                                 GtkGrid      *grid,
                                 gint          grid_top,
                                 GtkSizeGroup *group)
{
  GtkWidget *combo = gimp_prop_compression_combo_box_new (config,
                                                          property_name);

  if (combo)
    prefs_widget_add_aligned (combo, label, grid, grid_top, FALSE, group);

  return combo;
}

void
prefs_message (GtkWidget      *dialog,
               GtkMessageType  type,
               gboolean        destroy_with_parent,
               const gchar    *message)
{
  GtkWidget *message_dialog;

  message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog),
                                           destroy_with_parent ?
                                           GTK_DIALOG_DESTROY_WITH_PARENT : 0,
                                           type, GTK_BUTTONS_OK,
                                           "%s", message);

  gtk_dialog_run (GTK_DIALOG (message_dialog));

  gtk_widget_destroy (message_dialog);
}

void
prefs_config_notify (GObject    *config,
                     GParamSpec *param_spec,
                     GObject    *config_copy)
{
  GValue global_value = G_VALUE_INIT;
  GValue copy_value   = G_VALUE_INIT;

  g_value_init (&global_value, param_spec->value_type);
  g_value_init (&copy_value,   param_spec->value_type);

  g_object_get_property (config,      param_spec->name, &global_value);
  g_object_get_property (config_copy, param_spec->name, &copy_value);

  if (g_param_values_cmp (param_spec, &global_value, &copy_value))
    {
      g_signal_handlers_block_by_func (config_copy,
                                       prefs_config_copy_notify,
                                       config);

      g_object_set_property (config_copy, param_spec->name, &global_value);

      g_signal_handlers_unblock_by_func (config_copy,
                                         prefs_config_copy_notify,
                                         config);
    }

  g_value_unset (&global_value);
  g_value_unset (&copy_value);
}

void
prefs_config_copy_notify (GObject    *config_copy,
                          GParamSpec *param_spec,
                          GObject    *config)
{
  GValue copy_value   = G_VALUE_INIT;
  GValue global_value = G_VALUE_INIT;

  g_value_init (&copy_value,   param_spec->value_type);
  g_value_init (&global_value, param_spec->value_type);

  g_object_get_property (config_copy, param_spec->name, &copy_value);
  g_object_get_property (config,      param_spec->name, &global_value);

  if (g_param_values_cmp (param_spec, &copy_value, &global_value))
    {
      if (param_spec->flags & GIMP_CONFIG_PARAM_CONFIRM)
        {
#ifdef GIMP_CONFIG_DEBUG
          g_print ("NOT Applying prefs change of '%s' to edit_config "
                   "because it needs confirmation\n",
                   param_spec->name);
#endif
        }
      else
        {
#ifdef GIMP_CONFIG_DEBUG
          g_print ("Applying prefs change of '%s' to edit_config\n",
                   param_spec->name);
#endif
          g_signal_handlers_block_by_func (config,
                                           prefs_config_notify,
                                           config_copy);

          g_object_set_property (config, param_spec->name, &copy_value);

          g_signal_handlers_unblock_by_func (config,
                                             prefs_config_notify,
                                             config_copy);
        }
    }

  g_value_unset (&copy_value);
  g_value_unset (&global_value);
}

void
prefs_font_size_value_changed (GtkRange      *range,
                               GimpGuiConfig *config)
{
  gdouble value = gtk_range_get_value (range);

  g_signal_handlers_block_by_func (config,
                                   G_CALLBACK (prefs_gui_config_notify_font_size),
                                   range);
  g_object_set (G_OBJECT (config),
                "font-relative-size", value / 100.0,
                NULL);
  g_signal_handlers_unblock_by_func (config,
                                     G_CALLBACK (prefs_gui_config_notify_font_size),
                                     range);
}

void
prefs_gui_config_notify_font_size (GObject    *config,
                                   GParamSpec *pspec,
                                   GtkRange   *range)
{
  g_signal_handlers_block_by_func (range,
                                   G_CALLBACK (prefs_font_size_value_changed),
                                   config);
  gtk_range_set_value (range,
                       GIMP_GUI_CONFIG (config)->font_relative_size * 100.0);
  g_signal_handlers_unblock_by_func (range,
                                     G_CALLBACK (prefs_font_size_value_changed),
                                     config);
}

void
prefs_icon_size_value_changed (GtkRange      *range,
                               GimpGuiConfig *config)
{
  gint value = (gint) gtk_range_get_value (range);

  g_signal_handlers_block_by_func (config,
                                   G_CALLBACK (prefs_gui_config_notify_icon_size),
                                   range);
  g_object_set (G_OBJECT (config),
                "custom-icon-size", (GimpIconSize) value,
                NULL);
  g_signal_handlers_unblock_by_func (config,
                                     G_CALLBACK (prefs_gui_config_notify_icon_size),
                                     range);
}

void
prefs_gui_config_notify_icon_size (GObject    *config,
                                   GParamSpec *pspec,
                                   GtkRange   *range)
{
  GimpIconSize size = GIMP_GUI_CONFIG (config)->custom_icon_size;

  g_signal_handlers_block_by_func (range,
                                   G_CALLBACK (prefs_icon_size_value_changed),
                                   config);
  gtk_range_set_value (range, (gdouble) size);
  g_signal_handlers_unblock_by_func (range,
                                     G_CALLBACK (prefs_icon_size_value_changed),
                                     config);
}
