/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995-1999 Spencer Kimball and Peter Mattis
 *
 * gimpdynamicsoutputeditor.c
 * Copyright (C) 2010 Alexia Death
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpbase/gimpbase.h"
#include "libgimpcolor/gimpcolor.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "widgets-types.h"

#include "core/gimpcurve.h"
#include "core/gimpdynamicsoutput.h"

#include "gimpcurveview.h"
#include "gimpdynamicsoutputeditor.h"
#include "gimpmessagebox.h"
#include "gimpmessagedialog.h"

#include "gimp-intl.h"


#define CURVE_SIZE   185
#define CURVE_BORDER   4


enum
{
  PROP_0,
  PROP_OUTPUT
};

enum
{
  INPUT_COLUMN_INDEX,
  INPUT_COLUMN_USE_INPUT,
  INPUT_COLUMN_NAME,
  INPUT_COLUMN_COLOR,
  INPUT_N_COLUMNS
};


struct
{
  const gchar   *use_property;
  const gchar   *curve_property;
  const gchar   *label;
  const gdouble  color[4];
}
inputs[] =
{
  { "use-pressure",  "pressure-curve",  N_("Pressure"),         { 1.0, 0.0, 0.0, 1.0 } },
  { "use-velocity",  "velocity-curve",  N_("Velocity"),         { 0.0, 1.0, 0.0, 1.0 } },
  { "use-direction", "direction-curve", N_("Direction"),        { 0.0, 0.0, 1.0, 1.0 } },
  { "use-tilt",      "tilt-curve",      N_("Tilt"),             { 1.0, 0.5, 0.0, 1.0 } },
  { "use-wheel",     "wheel-curve",     N_("Wheel / Rotation"), { 1.0, 0.0, 1.0, 1.0 } },
  { "use-random",    "random-curve",    N_("Random"),           { 0.0, 1.0, 1.0, 1.0 } },
  { "use-fade",      "fade-curve",      N_("Fade"),             { 0.5, 0.5, 0.5, 0.0 } }
};

#define INPUT_COLOR(i) (inputs[(i)].color[3] ? &inputs[(i)].color : NULL)


typedef struct _GimpDynamicsOutputEditorPrivate GimpDynamicsOutputEditorPrivate;

struct _GimpDynamicsOutputEditorPrivate
{
  GimpDynamicsOutput *output;

  GtkListStore       *input_list;
  GtkTreeIter         input_iters[G_N_ELEMENTS (inputs)];

  GtkWidget          *curve_view;
  GtkWidget          *input_view;

  GimpCurve          *active_curve;
};

#define GET_PRIVATE(editor) \
        ((GimpDynamicsOutputEditorPrivate *) gimp_dynamics_output_editor_get_instance_private ((GimpDynamicsOutputEditor *) (editor)))


static void   gimp_dynamics_output_editor_constructed    (GObject                  *object);
static void   gimp_dynamics_output_editor_finalize       (GObject                  *object);
static void   gimp_dynamics_output_editor_set_property   (GObject                  *object,
                                                          guint                     property_id,
                                                          const GValue             *value,
                                                          GParamSpec               *pspec);
static void   gimp_dynamics_output_editor_get_property   (GObject                  *object,
                                                          guint                     property_id,
                                                          GValue                   *value,
                                                          GParamSpec               *pspec);

static void   gimp_dynamics_output_editor_curve_reset    (GtkWidget                *button,
                                                          GimpDynamicsOutputEditor *editor);

static void   gimp_dynamics_output_editor_curve_copy     (GtkWidget                *button,
                                                          GimpDynamicsOutputEditor *editor);

static void   gimp_dynamics_output_editor_curve_paste    (GtkWidget                *button,
                                                          GimpDynamicsOutputEditor *editor);

static void   gimp_dynamics_output_editor_input_selected (GtkTreeSelection         *selection,
                                                          GimpDynamicsOutputEditor *editor);

static void   gimp_dynamics_output_editor_input_toggled  (GtkCellRenderer          *cell,
                                                          gchar                    *path,
                                                          GimpDynamicsOutputEditor *editor);

static void   gimp_dynamics_output_editor_activate_input (GimpDynamicsOutputEditor *editor,
                                                          gint                      input);

static void   gimp_dynamics_output_editor_notify_output  (GimpDynamicsOutput       *output,
                                                          const GParamSpec         *pspec,
                                                          GimpDynamicsOutputEditor *editor);


G_DEFINE_TYPE_WITH_PRIVATE (GimpDynamicsOutputEditor,
                            gimp_dynamics_output_editor, GTK_TYPE_BOX)

#define parent_class gimp_dynamics_output_editor_parent_class


static void
gimp_dynamics_output_editor_class_init (GimpDynamicsOutputEditorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed  = gimp_dynamics_output_editor_constructed;
  object_class->finalize     = gimp_dynamics_output_editor_finalize;
  object_class->set_property = gimp_dynamics_output_editor_set_property;
  object_class->get_property = gimp_dynamics_output_editor_get_property;

  g_object_class_install_property (object_class, PROP_OUTPUT,
                                   g_param_spec_object ("output",
                                                        NULL, NULL,
                                                        GIMP_TYPE_DYNAMICS_OUTPUT,
                                                        GIMP_PARAM_READWRITE |
                                                        G_PARAM_CONSTRUCT_ONLY));
}

static void
gimp_dynamics_output_editor_init (GimpDynamicsOutputEditor *editor)
{
  gtk_orientable_set_orientation (GTK_ORIENTABLE (editor),
                                  GTK_ORIENTATION_VERTICAL);

  gtk_box_set_spacing (GTK_BOX (editor), 6);
}

static void
gimp_dynamics_output_editor_constructed (GObject *object)
{
  GimpDynamicsOutputEditor        *editor;
  GimpDynamicsOutputEditorPrivate *private;
  GtkWidget                       *view;
  GtkWidget                       *button;
  GtkWidget                       *scrolled_window;
  GtkWidget                       *vbox;
  GtkWidget                       *hbox;
  GtkCellRenderer                 *cell;
  GtkTreeSelection                *tree_sel;
  gint                             i;
  GimpDynamicsOutputType           output_type;
  const gchar                     *type_desc;

  editor  = GIMP_DYNAMICS_OUTPUT_EDITOR (object);
  private = GET_PRIVATE (object);

  G_OBJECT_CLASS (parent_class)->constructed (object);

  gimp_assert (GIMP_IS_DYNAMICS_OUTPUT (private->output));

  private->curve_view = gimp_curve_view_new ();
  g_object_set (private->curve_view,
                "border-width", CURVE_BORDER,
                NULL);

  g_object_get (private->output,
                "type", &output_type,
                NULL);

  if (gimp_enum_get_value (GIMP_TYPE_DYNAMICS_OUTPUT_TYPE, output_type,
                           NULL, NULL, &type_desc, NULL))
    g_object_set (private->curve_view,
                  "y-axis-label", type_desc,
                  NULL);

  gtk_widget_set_size_request (private->curve_view,
                               CURVE_SIZE + CURVE_BORDER * 2,
                               CURVE_SIZE + CURVE_BORDER * 2);
  gtk_box_pack_start (GTK_BOX (editor), private->curve_view, TRUE, TRUE, 0);
  gtk_widget_set_visible (private->curve_view, TRUE);

  gimp_dynamics_output_editor_activate_input (editor, 0);

  /* Create a horizontal box */
  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
  gtk_box_pack_start (GTK_BOX (editor), hbox, FALSE, FALSE, 0);
  gtk_widget_set_visible (hbox, TRUE);

  /* Create the Reset Curve button */
  button = gtk_button_new_with_mnemonic (_("_Reset Curve"));
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_widget_set_visible (button, TRUE);

  g_signal_connect (button, "clicked",
                    G_CALLBACK (gimp_dynamics_output_editor_curve_reset),
                    editor);

  /* Create the Copy Curve button */
  button = gtk_button_new_with_mnemonic (_("_Copy Curve"));
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_widget_set_visible (button, TRUE);

  g_signal_connect (button, "clicked",
                    G_CALLBACK (gimp_dynamics_output_editor_curve_copy),
                    editor);

  /* Create the Paste Curve button */
  button = gtk_button_new_with_mnemonic (_("_Paste Curve"));
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_widget_set_visible (button, TRUE);

  g_signal_connect (button, "clicked",
                    G_CALLBACK (gimp_dynamics_output_editor_curve_paste),
                    editor);

  private->input_list = gtk_list_store_new (INPUT_N_COLUMNS,
                                            G_TYPE_INT,
                                            G_TYPE_BOOLEAN,
                                            G_TYPE_STRING,
                                            GEGL_TYPE_COLOR);

  for (i = 0; i < G_N_ELEMENTS (inputs); i++)
    {
      gboolean   use_input;
      GeglColor *color = gegl_color_new ("black");

      g_object_get (private->output,
                    inputs[i].use_property, &use_input,
                    NULL);

      gegl_color_set_pixel (color, babl_format ("R'G'B'A double"), inputs[i].color);
      gtk_list_store_insert_with_values (private->input_list,
                                         &private->input_iters[i], -1,
                                         INPUT_COLUMN_INDEX,     i,
                                         INPUT_COLUMN_USE_INPUT, use_input,
                                         INPUT_COLUMN_NAME,      gettext (inputs[i].label),
                                         INPUT_COLUMN_COLOR,     color,
                                         -1);
      g_object_unref (color);
    }

  view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (private->input_list));
  g_object_unref (private->input_list);

  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (view), FALSE);

  cell = gtk_cell_renderer_toggle_new ();

  g_object_set (cell,
                "mode",        GTK_CELL_RENDERER_MODE_ACTIVATABLE,
                "activatable", TRUE,
                NULL);

  g_signal_connect (G_OBJECT (cell), "toggled",
                    G_CALLBACK (gimp_dynamics_output_editor_input_toggled),
                    editor);

  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1, NULL,
                                               gimp_cell_renderer_color_new (),
                                               "color", INPUT_COLUMN_COLOR,
                                               NULL);

  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1, NULL,
                                               cell,
                                               "active", INPUT_COLUMN_USE_INPUT,
                                               NULL);

  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1, NULL,
                                               gtk_cell_renderer_text_new (),
                                               "text", INPUT_COLUMN_NAME,
                                               NULL);

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
  gtk_widget_set_vexpand (vbox, TRUE);

  gtk_box_pack_start (GTK_BOX (vbox), view, TRUE, TRUE, 0);
  gtk_widget_set_visible (view, TRUE);

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (scrolled_window), vbox);
  gtk_widget_set_visible (vbox, TRUE);

  gtk_box_pack_end (GTK_BOX (editor), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_set_visible (scrolled_window, TRUE);

  private->input_view = view;

  tree_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
  gtk_tree_selection_set_mode (tree_sel, GTK_SELECTION_BROWSE);

  gtk_tree_selection_select_iter (tree_sel, &private->input_iters[0]);

  g_signal_connect (G_OBJECT (tree_sel), "changed",
                    G_CALLBACK (gimp_dynamics_output_editor_input_selected),
                    editor);

  g_signal_connect (private->output, "notify",
                    G_CALLBACK (gimp_dynamics_output_editor_notify_output),
                    editor);
}

static void
gimp_dynamics_output_editor_finalize (GObject *object)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (object);

  g_clear_object (&private->output);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gimp_dynamics_output_editor_set_property (GObject      *object,
                                          guint         property_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (object);

  switch (property_id)
    {
    case PROP_OUTPUT:
      private->output = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gimp_dynamics_output_editor_get_property (GObject    *object,
                                          guint       property_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (object);

  switch (property_id)
    {
    case PROP_OUTPUT:
      g_value_set_object (value, private->output);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gimp_dynamics_output_editor_curve_reset (GtkWidget                *button,
                                         GimpDynamicsOutputEditor *editor)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
  GtkWidget *dialog;
  gint       response;

  /* Create the confirmation dialog */
  dialog = gimp_message_dialog_new (_("Reset Curve"), GIMP_ICON_DIALOG_WARNING,
                                    button, GTK_DIALOG_MODAL,
                                    NULL, NULL,

                                    _("_Cancel"), GTK_RESPONSE_CANCEL,
                                    _("_OK"),     GTK_RESPONSE_OK,

                                    NULL);

  /* Set the message text */
  gimp_message_box_set_primary_text (GIMP_MESSAGE_DIALOG (dialog)->box,
                                     _("Resets the curve to linear default"));
  gimp_message_box_set_text (GIMP_MESSAGE_DIALOG (dialog)->box,
                             _("This operation cannot be undone."));

  /* Run the dialog and get the response */
  response = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);

  /* Handle the user's response */
  if (response != GTK_RESPONSE_OK)
    {
      /* User clicked Cancel or closed the dialog */
      return;
    }

  /* Proceed with resetting the curve */
  if (private->active_curve)
    gimp_curve_reset (private->active_curve, TRUE);
}

static void
gimp_dynamics_output_editor_curve_copy (GtkWidget                *button,
                                        GimpDynamicsOutputEditor *editor)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
  GimpCurve *curve;
  gint       n_points, i;
  gdouble    x, y;
  GimpCurvePointType type;
  GString   *file_content;
  gchar     *file_path;

  /* Ensure there's an active curve */
  if (!private->active_curve)
    return;

  curve = private->active_curve;

  /* Get the file path */
  file_path = g_build_filename (gimp_directory (), "dynamics", "copied_curve.txt", NULL);

  /* Prepare the file content */
  file_content = g_string_new ("");
  n_points = gimp_curve_get_n_points (curve);

  for (i = 0; i < n_points; i++)
    {
      /* Get the coordinates and type of each point */
      gimp_curve_get_point (curve, i, &x, &y);
      type = gimp_curve_get_point_type (curve, i);

      /* Append point data and type to the string */
      g_string_append_printf (file_content, "%f %f %d\n", x, y, (gint)type);
    }

  /* Write the content to the file */
  if (!g_file_set_contents (file_path, file_content->str, -1, NULL))
    {
      g_printerr ("Failed to write curve data to file: %s\n", file_path);
    }

  /* Clean up */
  g_string_free (file_content, TRUE);
  g_free (file_path);
}

static void
gimp_dynamics_output_editor_curve_paste (GtkWidget                *button,
                                         GimpDynamicsOutputEditor *editor)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
  GimpCurve *curve;
  gchar     *file_content;
  gchar    **lines;
  gchar    **line_parts;
  gchar     *file_path;
  gdouble    x, y;
  GimpCurvePointType type;
  gint       i, n_lines;
  gint       point_index;
  char      *endptr;

  /* Ensure there's an active curve */
  if (!private->active_curve)
    return;

  curve = private->active_curve;

  /* Get the file path */
  file_path = g_build_filename (gimp_directory (), "dynamics", "copied_curve.txt", NULL);

  /* Read the file content */
  if (!g_file_get_contents (file_path, &file_content, NULL, NULL))
    {
      g_printerr ("Failed to read curve data from file: %s\n", file_path);
      g_free (file_path);
      return;
    }

  /* Clear the curve of existing points */
  gimp_curve_clear_points (curve);

  /* Process the file content */
  lines = g_strsplit (file_content, "\n", -1);
  n_lines = g_strv_length (lines);

  for (i = 0; i < n_lines; i++)
    {
      /* Skip empty lines */
      if (lines[i] == NULL || lines[i][0] == '\0')
        continue;

      /* Split each line into x, y, and possibly type */
      line_parts = g_strsplit (lines[i], " ", 3);
      if (g_strv_length (line_parts) >= 2)
        {
          x = g_ascii_strtod (line_parts[0], NULL);
          y = g_ascii_strtod (line_parts[1], NULL);

          if (g_strv_length (line_parts) == 3)
            {
              /* Parse the type */
              type = (GimpCurvePointType) strtol (line_parts[2], &endptr, 10);
              if (*endptr != '\0')
                type = GIMP_CURVE_POINT_SMOOTH; /* Default type if parsing fails */
            }
          else
            {
              /* Default type if not specified */
              type = GIMP_CURVE_POINT_SMOOTH;
            }

          /* Add the point and set its type */
          point_index = gimp_curve_add_point (curve, x, y);
          gimp_curve_set_point_type (curve, point_index, type);
        }

      g_strfreev (line_parts);
    }

  /* Clean up */
  g_strfreev (lines);
  g_free (file_content);
  g_free (file_path);
}

static void
gimp_dynamics_output_editor_input_selected (GtkTreeSelection         *selection,
                                            GimpDynamicsOutputEditor *editor)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gint input;

      gtk_tree_model_get (model, &iter,
                          INPUT_COLUMN_INDEX, &input,
                          -1);

      gimp_dynamics_output_editor_activate_input (editor, input);
    }
}

static void
gimp_dynamics_output_editor_input_toggled (GtkCellRenderer          *cell,
                                           gchar                    *path,
                                           GimpDynamicsOutputEditor *editor)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
  GtkTreeModel                    *model;
  GtkTreeIter                      iter;

  model = GTK_TREE_MODEL (private->input_list);

  if (gtk_tree_model_get_iter_from_string (model, &iter, path))
    {
      gint     input;
      gboolean use;

      gtk_tree_model_get (model, &iter,
                          INPUT_COLUMN_INDEX,     &input,
                          INPUT_COLUMN_USE_INPUT, &use,
                          -1);

      g_object_set (private->output,
                    inputs[input].use_property, ! use,
                    NULL);
    }
}

static void
gimp_dynamics_output_editor_activate_input (GimpDynamicsOutputEditor *editor,
                                            gint                      input)
{
  GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
  gint                             i;

  gimp_curve_view_set_curve (GIMP_CURVE_VIEW (private->curve_view), NULL, NULL);
  gimp_curve_view_remove_all_backgrounds (GIMP_CURVE_VIEW (private->curve_view));

  for (i = 0; i < G_N_ELEMENTS (inputs); i++)
    {
      gboolean   use_input;
      GimpCurve *input_curve;
      GeglColor *color = gegl_color_new (NULL);

      g_object_get (private->output,
                    inputs[i].use_property,   &use_input,
                    inputs[i].curve_property, &input_curve,
                    NULL);

      if (INPUT_COLOR (i))
        gegl_color_set_pixel (color, babl_format ("R'G'B'A double"), INPUT_COLOR (i));
      if (input == i)
        {
          gimp_curve_view_set_curve (GIMP_CURVE_VIEW (private->curve_view),
                                     input_curve,
                                     (INPUT_COLOR (i) != NULL) ? color : NULL);
          private->active_curve = input_curve;

          gimp_curve_view_set_x_axis_label (GIMP_CURVE_VIEW (private->curve_view),
                                            inputs[i].label);
        }
      else if (use_input)
        {
          gimp_curve_view_add_background (GIMP_CURVE_VIEW (private->curve_view),
                                          input_curve,
                                          (INPUT_COLOR (i) != NULL) ? color : NULL);
        }

      g_object_unref (input_curve);
      g_object_unref (color);
    }
}

static void
gimp_dynamics_output_editor_notify_output (GimpDynamicsOutput       *output,
                                           const GParamSpec         *pspec,
                                           GimpDynamicsOutputEditor *editor)
{
  gint i;

  for (i = 0; i < G_N_ELEMENTS (inputs); i++)
    {
      if (! strcmp (pspec->name, inputs[i].use_property))
        {
          GimpDynamicsOutputEditorPrivate *private = GET_PRIVATE (editor);
          GtkTreeSelection                *sel;
          gboolean                         use_input;
          GimpCurve                       *input_curve;

          sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (private->input_view));

          g_object_get (output,
                        pspec->name,              &use_input,
                        inputs[i].curve_property, &input_curve,
                        NULL);

          gtk_list_store_set (private->input_list, &private->input_iters[i],
                              INPUT_COLUMN_USE_INPUT, use_input,
                              -1);

          if (! gtk_tree_selection_iter_is_selected (sel,
                                                     &private->input_iters[i]))
            {
              if (use_input)
                {
                  GeglColor *color = gegl_color_new (NULL);

                  if (INPUT_COLOR (i))
                    gegl_color_set_pixel (color, babl_format ("R'G'B'A double"), INPUT_COLOR (i));

                  gimp_curve_view_add_background (GIMP_CURVE_VIEW (private->curve_view),
                                                  input_curve,
                                                  (INPUT_COLOR (i) != NULL) ? color : NULL);
                  g_object_unref (color);
                }
              else
                {
                  gimp_curve_view_remove_background (GIMP_CURVE_VIEW (private->curve_view),
                                                     input_curve);
                }

              g_object_unref (input_curve);
            }

          break;
        }
    }
}


/*  public functions  */

GtkWidget *
gimp_dynamics_output_editor_new (GimpDynamicsOutput *output)
{
  g_return_val_if_fail (GIMP_IS_DYNAMICS_OUTPUT (output), NULL);

  return g_object_new (GIMP_TYPE_DYNAMICS_OUTPUT_EDITOR,
                       "output", output,
                       NULL);
}
