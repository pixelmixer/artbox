/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpconfig/gimpconfig.h"
#include "libgimpmath/gimpmath.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "widgets-types.h"

#include "config/gimpguiconfig.h"

#include "core/gimp.h"
#include "core/gimpcontainer.h"
#include "core/gimpdata.h"
#include "core/gimpdatafactory.h"
#include "core/gimppaintinfo.h"
#include "core/gimptoolinfo.h"
#include "core/gimptooloptions.h"
#include "core/gimptoolpreset.h"

#include "paint/gimppaintoptions.h"

#include "gimpcolorpanel.h"
#include "gimpdocked.h"
#include "gimptoolpreseteditor.h"
#include "gimplayermodebox.h"
#include "gimpmenufactory.h"
#include "gimppropwidgets.h"
#include "gimpviewablebox.h"
#include "gimpviewablebutton.h"
#include "gimpwidgets-utils.h"
#include "gimpwidgets-constructors.h"

#include "gimp-intl.h"

enum {
    PRESET_SAVED,
    LAST_SIGNAL
};

static guint tool_preset_editor_signals[LAST_SIGNAL] = { 0 };

struct _GimpToolPresetEditorPrivate
{
  GimpToolPreset *tool_preset_model;

  GtkWidget      *tool_icon;
  GtkWidget      *tool_label;

  GtkWidget      *options_expander;
  GtkWidget      *load_expander;
  GtkWidget      *link_expander;

  GtkWidget      *save_button;
  GtkWidget      *save_as_button;
  GtkWidget      *save_resources_button;
  GtkWidget      *save_as_resources_button;
  GtkWidget      *save_all_button;

  GtkWidget      *foreground_toggle;
  GtkWidget      *background_toggle;
  GtkWidget      *opacity_toggle;
  GtkWidget      *paint_mode_toggle;
  GtkWidget      *brush_toggle;
  GtkWidget      *dynamics_toggle;
  GtkWidget      *mybrush_toggle;
  GtkWidget      *gradient_toggle;
  GtkWidget      *pattern_toggle;
  GtkWidget      *palette_toggle;
  GtkWidget      *font_toggle;

  GtkWidget      *link_foreground_toggle;
  GtkWidget      *link_background_toggle;
  GtkWidget      *link_opacity_toggle;
  GtkWidget      *link_paint_mode_toggle;
  GtkWidget      *link_brush_toggle;
  GtkWidget      *link_dynamics_toggle;
  GtkWidget      *link_mybrush_toggle;
  GtkWidget      *link_gradient_toggle;
  GtkWidget      *link_pattern_toggle;
  GtkWidget      *link_palette_toggle;
  GtkWidget      *link_font_toggle;

  GtkWidget      *foreground_picker;
  GtkWidget      *background_picker;
  GtkWidget      *opacity_slider;
  GtkWidget      *mode_picker;
  GtkWidget      *brush_picker;
  GtkWidget      *dynamics_picker;
  GtkWidget      *mybrush_picker;
  GtkWidget      *gradient_picker;
  GtkWidget      *pattern_picker;
  GtkWidget      *palette_picker;
  GtkWidget      *font_picker;
};

typedef struct {
  GtkWidget   *image;
  const gchar *icon_name_active;
  const gchar *icon_name_active_broken;
  const gchar *icon_name_inactive;
  const gchar *icon_name_inactive_broken;
  const gchar *help_text_active;
  const gchar *help_text_active_broken;
  const gchar *help_text_inactive;
  const gchar *help_text_inactive_broken;
  gboolean     broken_link;
  GimpToolPresetEditor *editor;
} ResourceToggleData;

typedef enum
{
  STORED_FOREGROUND,
  STORED_BACKGROUND,
  STORED_OPACITY,
  STORED_PAINT_MODE,
  STORED_BRUSH,
  STORED_DYNAMICS,
  STORED_MYBRUSH,
  STORED_GRADIENT,
  STORED_PATTERN,
  STORED_PALETTE,
  STORED_FONT,
  STORED_UNKNOWN
} StoredKey;


/*  local function prototypes  */

static void   gimp_tool_preset_editor_constructed    (GObject              *object);
static void   gimp_tool_preset_editor_finalize       (GObject              *object);

static void   gimp_tool_preset_editor_style_updated (GtkWidget             *widget);

static void   gimp_tool_preset_editor_set_data       (GimpDataEditor       *editor,
                                                      GimpData             *data);

static void   gimp_tool_preset_editor_sync_data      (GimpToolPresetEditor *editor);
static void   gimp_tool_preset_editor_notify_model   (GimpToolPreset       *options,
                                                      const GParamSpec     *pspec,
                                                      GimpToolPresetEditor *editor);
static void   gimp_tool_preset_editor_notify_data    (GimpToolPreset       *options,
                                                      const GParamSpec     *pspec,
                                                      GimpToolPresetEditor *editor);

static void   update_reload_resource_toggle          (GtkToggleButton      *toggle_button,
                                                      ResourceToggleData   *toggle_data);


G_DEFINE_TYPE_WITH_CODE (GimpToolPresetEditor, gimp_tool_preset_editor,
                         GIMP_TYPE_DATA_EDITOR,
                         G_ADD_PRIVATE (GimpToolPresetEditor)
                         G_IMPLEMENT_INTERFACE (GIMP_TYPE_DOCKED, NULL))

#define parent_class gimp_tool_preset_editor_parent_class


GtkWidget *
gimp_tool_preset_editor_new (GimpContext     *context,
                             GimpMenuFactory *menu_factory)
{
  g_return_val_if_fail (GIMP_IS_MENU_FACTORY (menu_factory), NULL);
  g_return_val_if_fail (GIMP_IS_CONTEXT (context), NULL);

  return g_object_new (GIMP_TYPE_TOOL_PRESET_EDITOR,
                       "menu-factory",    menu_factory,
                       "menu-identifier", "<ToolPresetEditor>",
                       "ui-path",         "/tool-preset-editor-popup",
                       "data-factory",    context->gimp->tool_preset_factory,
                       "context",         context,
                       "data",            gimp_context_get_tool_preset (context),
                       NULL);
}


/*  private functions  */

static StoredKey
get_stored_key_from_string (const gchar *field_key)
{
  if (g_strcmp0 (field_key, "foreground") == 0)
    return STORED_FOREGROUND;
  if (g_strcmp0 (field_key, "background") == 0)
    return STORED_BACKGROUND;
  if (g_strcmp0 (field_key, "opacity") == 0)
    return STORED_OPACITY;
  if (g_strcmp0 (field_key, "paint-mode") == 0)
    return STORED_PAINT_MODE;
  if (g_strcmp0 (field_key, "brush") == 0)
    return STORED_BRUSH;
  if (g_strcmp0 (field_key, "dynamics") == 0)
    return STORED_DYNAMICS;
  if (g_strcmp0 (field_key, "mybrush") == 0)
    return STORED_MYBRUSH;
  if (g_strcmp0 (field_key, "gradient") == 0)
    return STORED_GRADIENT;
  if (g_strcmp0 (field_key, "pattern") == 0)
    return STORED_PATTERN;
  if (g_strcmp0 (field_key, "palette") == 0)
    return STORED_PALETTE;
  if (g_strcmp0 (field_key, "font") == 0)
    return STORED_FONT;

  return STORED_UNKNOWN;
}

static void
gimp_tool_preset_editor_class_init (GimpToolPresetEditorClass *klass)
{
  GObjectClass        *object_class = G_OBJECT_CLASS (klass);
  GimpDataEditorClass *editor_class = GIMP_DATA_EDITOR_CLASS (klass);
  GtkWidgetClass      *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = gimp_tool_preset_editor_constructed;
  object_class->finalize    = gimp_tool_preset_editor_finalize;

  editor_class->set_data    = gimp_tool_preset_editor_set_data;
  editor_class->title       = _("Tool Preset Editor");

  widget_class->style_updated = gimp_tool_preset_editor_style_updated;
}

static void
gimp_tool_preset_editor_init (GimpToolPresetEditor *editor)
{
  editor->priv = gimp_tool_preset_editor_get_instance_private (editor);

  editor->priv->tool_preset_model       = NULL;

  editor->priv->tool_icon               = NULL;
  editor->priv->tool_label              = NULL;

  editor->priv->options_expander        = NULL;
  editor->priv->load_expander           = NULL;
  editor->priv->link_expander           = NULL;

  editor->priv->foreground_toggle       = NULL;
  editor->priv->background_toggle       = NULL;
  editor->priv->opacity_toggle          = NULL;
  editor->priv->paint_mode_toggle       = NULL;
  editor->priv->brush_toggle            = NULL;
  editor->priv->dynamics_toggle         = NULL;
  editor->priv->mybrush_toggle          = NULL;
  editor->priv->gradient_toggle         = NULL;
  editor->priv->pattern_toggle          = NULL;
  editor->priv->palette_toggle          = NULL;
  editor->priv->font_toggle             = NULL;

  editor->priv->link_foreground_toggle  = NULL;
  editor->priv->link_background_toggle  = NULL;
  editor->priv->link_opacity_toggle     = NULL;
  editor->priv->link_paint_mode_toggle  = NULL;
  editor->priv->link_brush_toggle       = NULL;
  editor->priv->link_dynamics_toggle    = NULL;
  editor->priv->link_mybrush_toggle     = NULL;
  editor->priv->link_gradient_toggle    = NULL;
  editor->priv->link_pattern_toggle     = NULL;
  editor->priv->link_palette_toggle     = NULL;
  editor->priv->link_font_toggle        = NULL;

  editor->priv->foreground_picker       = NULL;
  editor->priv->background_picker       = NULL;
  editor->priv->opacity_slider          = NULL;
  editor->priv->mode_picker             = NULL;
  editor->priv->brush_picker            = NULL;
  editor->priv->dynamics_picker         = NULL;
  editor->priv->mybrush_picker          = NULL;
  editor->priv->gradient_picker         = NULL;
  editor->priv->pattern_picker          = NULL;
  editor->priv->palette_picker          = NULL;
  editor->priv->font_picker             = NULL;
}

static const char *
layer_mode_to_string (GimpLayerMode mode)
{
  GType type = gimp_layer_mode_get_type ();
  GEnumValue *value;

  /* Retrieve the enum value corresponding to the mode */
  value = g_enum_get_value (g_type_class_ref (type), mode);

  if (value)
    return value->value_nick; /* This is the short string (e.g., "normal") */

  g_warning ("Invalid layer mode: %d", mode);

  return NULL;
}

GimpToolPreset *
deserialize_preset (GimpContext *context)
{
  GFile          *resource_file = NULL;
  GError         *error         = NULL;
  GScanner       *scanner       = NULL;
  GimpToolPreset *stored_preset = NULL;
  gchar          *contents      = NULL;

  g_return_val_if_fail (context != NULL, NULL);

  if (!GIMP_DATA (context->tool_preset))
    {
      return NULL;
    }

  resource_file = gimp_data_get_file (GIMP_DATA (context->tool_preset));
  if (!resource_file)
    {
      return NULL;
    }

  /* Check file existence */
  if (!g_file_test (g_file_peek_path (resource_file), G_FILE_TEST_EXISTS))
    {
      return NULL;
    }

  /* Load the file contents */
  if (!g_file_load_contents (resource_file, NULL, &contents, NULL, NULL, &error))
    {
      return NULL;
    }

  /* Check for empty or invalid contents */
  if (!contents || strlen (contents) == 0)
    {
      g_free (contents);
      return NULL;
    }

  /* Initialize the scanner */
  scanner = gimp_scanner_new_string (contents, -1, &error);
  if (!scanner)
    {
      g_print("deserialize_preset: Failed to initialize scanner: %s\n", error ? error->message : "Unknown error");
      g_clear_error (&error);
      g_free (contents);
      return NULL;
    }

  /* Create and deserialize the tool preset */
  stored_preset = g_object_new (GIMP_TYPE_TOOL_PRESET, "gimp", context->gimp, NULL);
  if (!stored_preset || !gimp_config_deserialize (GIMP_CONFIG (stored_preset), scanner, 0, NULL))
    {
      g_scanner_destroy (scanner);
      g_free (contents);
      g_object_unref (stored_preset);
      stored_preset = NULL;
    }

  /* Cleanup */
  g_scanner_destroy (scanner);
  g_free (contents);

  return stored_preset;
}

static gchar *
extract_color_value (GeglColor *color)
{
  gdouble r, g, b, a;

  if (!color)
    return g_strdup ("None");

  gegl_color_get_rgba (color, &r, &g, &b, &a);

  return g_strdup_printf ("%.6f %.6f %.6f %.6f", r, g, b, a);
}

static gchar *
extract_preset_value (GimpToolPreset *stored_preset,
                      const gchar    *field_key)
{
  GimpContext *context = GIMP_CONTEXT (stored_preset->tool_options);
  StoredKey    key     = get_stored_key_from_string (field_key);

  // This is the stored and loaded context not the active context
  switch (key)
    {
    case STORED_OPACITY:
      return g_strdup_printf ("%.6f", context->opacity);

    case STORED_PAINT_MODE:
      return g_strdup (layer_mode_to_string (context->paint_mode));

    case STORED_PATTERN:
      return g_strdup (context->pattern ? context->pattern_name : "None");

    case STORED_GRADIENT:
      return g_strdup (context->gradient ? context->gradient_name : "None");

    case STORED_DYNAMICS:
      return g_strdup (context->dynamics_name ? context->dynamics_name : "None");

    case STORED_BRUSH:
      return g_strdup (context->brush_name ? context->brush_name : "None");

    case STORED_MYBRUSH:
      return g_strdup (context->mybrush_name ? context->mybrush_name : "None");

    case STORED_PALETTE:
      return g_strdup (context->palette_name ? context->palette_name : "None");

    case STORED_FONT:
      return g_strdup (context->font_name ? context->font_name : "None");

    case STORED_FOREGROUND:
      return extract_color_value (context->foreground);

    case STORED_BACKGROUND:
      return extract_color_value (context->background);

    case STORED_UNKNOWN:
    default:
      g_print ("Unknown stored key: %s\n", field_key);
      return g_strdup ("None");
    }
}

static gchar *
get_active_font_name (GimpContext *context)
{
  return g_strdup (context->font_name ? context->font_name : "None");
}

static gchar *
get_active_gradient_name (GimpContext *context)
{
  return g_strdup (context->gradient_name ? context->gradient_name : "None");
}

static gchar *
get_active_brush_name (GimpContext *context)
{
  return g_strdup (context->brush_name ? context->brush_name : "None");
}

static gchar *
get_active_mybrush_name (GimpContext *context)
{
  return g_strdup (context->mybrush_name ? context->mybrush_name : "None");
}

static gchar *
get_active_dynamics_name (GimpContext *context)
{
  return g_strdup (context->dynamics_name ? context->dynamics_name : "None");
}

static gchar *
get_active_pattern_name (GimpContext *context)
{
  return g_strdup (context->pattern_name ? context->pattern_name : "None");
}

static gchar *
get_active_palette_name (GimpContext *context)
{
  return g_strdup (context->palette_name ? context->palette_name : "None");
}

static gchar *
get_active_opacity (GimpContext *context)
{
  gdouble opacity = gimp_context_get_opacity (context);
  return g_strdup_printf ("%.6f", opacity);  // Use sufficient precision
}

static gchar *
get_active_paint_mode (GimpContext *context)
{
  GimpLayerMode mode = gimp_context_get_paint_mode (context);
  return g_strdup (layer_mode_to_string (mode));
}

static gchar *
get_active_foreground_colour (GimpContext *context)
{
  GeglColor *foreground = gimp_context_get_foreground (context);
  gdouble r, g, b, a;

  if (foreground == NULL)
    return g_strdup ("None");

  gegl_color_get_rgba (foreground, &r, &g, &b, &a);

  return g_strdup_printf ("%.6f %.6f %.6f %.6f", r, g, b, a);
}

static gchar *
get_active_background_colour (GimpContext *context)
{
  GeglColor *background = gimp_context_get_background (context);
  gdouble r, g, b, a;

  if (background == NULL)
    return g_strdup ("None");

  gegl_color_get_rgba (background, &r, &g, &b, &a);

  return g_strdup_printf ("%.6f %.6f %.6f %.6f", r, g, b, a);
}

static void
compare_values (const gchar *active_value,
                const gchar *stored_value,
                gboolean    *matches)
{
  gchar   *endptr_active = NULL;
  gchar   *endptr_stored = NULL;
  gdouble  active_number;
  gdouble  stored_number;

  if (!active_value || !stored_value)
    {
      *matches = FALSE;
      return;
    }

  // "None" indicates a default value, or no value in the saved file
  if (g_strcmp0 (stored_value, "None") == 0)
    {
      *matches = TRUE;
      return;
    }

  // Attempt numeric comparison
  active_number = g_ascii_strtod (active_value, &endptr_active);
  stored_number = g_ascii_strtod (stored_value, &endptr_stored);

  if (*endptr_active == '\0' && *endptr_stored == '\0')
    {
      const gdouble epsilon = 1e-6;

      *matches = (fabs (active_number - stored_number) < epsilon);
      return;
    }

  // String comparison
  *matches = (g_strcmp0 (stored_value, active_value) == 0);
}

static void
update_resource_gui (GtkWidget *link_toggle,
                     gboolean   matches_active)
{
  ResourceToggleData *toggle_data;

  /* Retrieve the ResourceToggleData from link_toggle */
  toggle_data = g_object_get_data (G_OBJECT (link_toggle), "toggle-data");

  if (toggle_data)
    {
      /* Update the broken_link field based on matches_active */
      toggle_data->broken_link = ! matches_active;

      /* Update the toggle icon */
      update_reload_resource_toggle (GTK_TOGGLE_BUTTON (link_toggle), toggle_data);
    }
}

static void
update_entry_text (GtkWidget *picker,
                   gboolean   matches_active)
{
  GtkWidget         *entry = NULL;
  const gchar       *current_text;
  PangoAttrList     *attr_list = NULL;
  PangoAttribute    *attr = NULL;

  if (!picker)
    {
      return;
    }

  entry = g_object_get_data (G_OBJECT (picker), "viewable-entry");

  if (!entry)
    {
      return;
    }

  current_text = gtk_entry_get_text (GTK_ENTRY (entry));

  attr_list = pango_attr_list_new();

  if (!matches_active)
    {
      /* Apply italic attribute */
      attr = pango_attr_style_new (PANGO_STYLE_ITALIC);
      attr->start_index = 0;
      attr->end_index = (gint)strlen(current_text); /* Apply to full text */
      pango_attr_list_insert (attr_list, attr);
    }

  /* Apply the attribute list to the entry */
  gtk_entry_set_attributes (GTK_ENTRY (entry), attr_list);
  pango_attr_list_unref (attr_list);
}

static void
debug_out_of_sync (const gchar *resource_name,
                   const gchar *active_name,
                   const gchar *active_value,
                   const gchar *stored_value)
{
  g_print ("\nPreset out of sync : %s\n", active_name);
  g_print ("Data out of sync   : %s\n", resource_name);
  g_print (" active_preset_value : %s\n", active_value);
  g_print (" stored_preset_value : %s\n", stored_value);
}

typedef gchar* (*ActiveFieldGetter)(GimpContext *context);

static void
update_resource_info (GimpToolPresetEditor *editor,
                      const gchar          *resource_name,
                      ActiveFieldGetter     active_getter,
                      const gchar          *field_key,
                      GtkWidget            *link_toggle,
                      GtkWidget            *picker)
{
  GimpContext    *context;
  GimpToolPreset *stored_preset;
  gchar          *stored_preset_value;
  gchar          *active_preset_value;
  gboolean stored_matches_active = TRUE;
  gboolean debug_comparison      = FALSE;

  if (! GIMP_IS_DATA_EDITOR (editor))
    return;

  context       = GIMP_DATA_EDITOR (editor)->context;
  stored_preset = deserialize_preset (context);

  if (! stored_preset)
   return;

  active_preset_value = active_getter (context);
  stored_preset_value = extract_preset_value (stored_preset, field_key);
  compare_values (active_preset_value, stored_preset_value, &stored_matches_active);

  if (! stored_matches_active && debug_comparison)
    {
      debug_out_of_sync (resource_name,
                         context->tool_preset_name,
                         active_preset_value,
                         stored_preset_value);
    }

  update_entry_text (picker, stored_matches_active);

  update_resource_gui (link_toggle, stored_matches_active);

  g_free (stored_preset_value);
  g_free (active_preset_value);

  if (stored_preset)
    g_object_unref (stored_preset);
}

static void
update_opacity_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Opacity",
                        get_active_opacity,
                        "opacity",
                        editor->priv->link_opacity_toggle,
                        editor->priv->opacity_slider);
}

static void
update_paint_mode_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Paint Mode",
                        get_active_paint_mode,
                        "paint-mode",
                        editor->priv->link_paint_mode_toggle,
                        NULL);
}

static void
update_palette_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Palette",
                        get_active_palette_name,
                        "palette",
                        editor->priv->link_palette_toggle,
                        editor->priv->palette_picker);
}

static void
update_font_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Font",
                        get_active_font_name,
                        "font",
                        editor->priv->link_font_toggle,
                        editor->priv->font_picker);
}

static void
update_gradient_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Gradient",
                        get_active_gradient_name,
                        "gradient",
                        editor->priv->link_gradient_toggle,
                        editor->priv->gradient_picker);
}

static void
update_brush_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Brush",
                        get_active_brush_name,
                        "brush",
                        editor->priv->link_brush_toggle,
                        editor->priv->brush_picker);

}

static void
update_mybrush_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "MyBrush",
                        get_active_mybrush_name,
                        "mybrush",
                        editor->priv->link_mybrush_toggle,
                        NULL);
}

static void
update_dynamics_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Dynamics",
                        get_active_dynamics_name,
                        "dynamics",
                        editor->priv->link_dynamics_toggle,
                        editor->priv->dynamics_picker);
}

static void
update_pattern_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Pattern",
                        get_active_pattern_name,
                        "pattern",
                        editor->priv->link_pattern_toggle,
                        editor->priv->pattern_picker);
}

static void
update_foreground_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Foreground",
                        get_active_foreground_colour,
                        "foreground",
                        editor->priv->link_foreground_toggle,
                        NULL);
}

static void
update_background_info (GimpToolPresetEditor *editor)
{
  update_resource_info (editor,
                        "Background",
                        get_active_background_colour,
                        "background",
                        editor->priv->link_background_toggle,
                        NULL);
}

static void
add_toggle_button (GtkWidget **toggle_button,
                   GtkWidget  *parent,
                   GObject    *object,
                   const gchar *property_name)
{
  *toggle_button = gimp_prop_check_button_new (object, property_name, NULL);

  gtk_box_pack_start (GTK_BOX (parent), *toggle_button, FALSE, FALSE, 0);
}

static void
preset_application_filter (GimpDataEditor *data_editor,
                           GimpToolPreset *preset)
{
  GtkWidget *expander;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *scrolled_window;
  gboolean debug_visible = FALSE;

  GimpToolPresetEditor *editor = GIMP_TOOL_PRESET_EDITOR (data_editor);

  expander = gtk_expander_new (_("Resource Reloading"));
  gtk_widget_set_tooltip_text (expander,
                               _("Reloading the active Preset\n"
                                 "will replace this resource."));
  editor->priv->load_expander = expander;

  frame = gimp_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (expander), frame);
  gtk_widget_set_visible (frame, TRUE);

  /* Create a scrolled window */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_NEVER,
                                  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (frame), scrolled_window);
  gtk_widget_set_visible (scrolled_window, TRUE);
  gtk_widget_set_size_request (scrolled_window, -1, 300);

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_container_add (GTK_CONTAINER (scrolled_window), vbox);
  gtk_widget_set_visible (vbox, TRUE);

  /* Add toggle buttons */
  add_toggle_button (&editor->priv->foreground_toggle, vbox, G_OBJECT (preset), "use-foreground");
  add_toggle_button (&editor->priv->background_toggle, vbox, G_OBJECT (preset), "use-background");
  add_toggle_button (&editor->priv->opacity_toggle, vbox, G_OBJECT (preset), "use-opacity");
  add_toggle_button (&editor->priv->paint_mode_toggle, vbox, G_OBJECT (preset), "use-paint-mode");
  add_toggle_button (&editor->priv->brush_toggle, vbox, G_OBJECT (preset), "use-brush");
  add_toggle_button (&editor->priv->dynamics_toggle, vbox, G_OBJECT (preset), "use-dynamics");
  add_toggle_button (&editor->priv->mybrush_toggle, vbox, G_OBJECT (preset), "use-mypaint-brush");
  add_toggle_button (&editor->priv->gradient_toggle, vbox, G_OBJECT (preset), "use-gradient");
  add_toggle_button (&editor->priv->pattern_toggle, vbox, G_OBJECT (preset), "use-pattern");
  add_toggle_button (&editor->priv->palette_toggle, vbox, G_OBJECT (preset), "use-palette");
  add_toggle_button (&editor->priv->font_toggle, vbox, G_OBJECT (preset), "use-font");

  gtk_box_pack_start (GTK_BOX (data_editor), expander, FALSE, FALSE, 0);
  gtk_widget_set_visible (expander, debug_visible);
  gtk_widget_set_sensitive (expander, preset == NULL);
}

GtkWidget *
force_custom_icon (GtkWidget   *picker,
                   const gchar *icon_name)
{
  GtkWidget *button;
  GtkWidget *image;
  GtkWidget *existing_child;

  /* Get the "viewable-button" from the picker */
  button = g_object_get_data (G_OBJECT (picker), "viewable-button");
  if (! button)
    {
      g_warning ("Could not find 'viewable-button' in picker");
      return NULL;
    }

  /* Create the custom image with the appropriate size */
  image = gtk_image_new_from_icon_name (icon_name, GTK_ICON_SIZE_LARGE_TOOLBAR);

  /* Remove the existing child widget, if present */
  if ((existing_child = gtk_bin_get_child (GTK_BIN (button))))
    {
      gtk_container_remove (GTK_CONTAINER (button), existing_child);
    }

  /* Add and display the new custom image */
  gtk_container_add (GTK_CONTAINER (button), image);
  gtk_widget_show (image);

  return button;
}

static void
set_view_properties (GObject *data)
{
  g_object_set_data (data, "view-type", GINT_TO_POINTER (GIMP_VIEW_TYPE_LIST));
  g_object_set_data (data, "view-size", GINT_TO_POINTER (GIMP_VIEWABLE_MAX_BUTTON_SIZE));
}

static void
clear_view_properties (GObject *data)
{
  g_object_set_data (data, "view-type", NULL);
  g_object_set_data (data, "view-size", NULL);
}

static GtkWidget *
get_brush_picker (GimpContext          *context,
                  GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->brush_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->brush_picker = gimp_prop_brush_box_new (GIMP_CONTAINER (data),
                                                        GIMP_CONTEXT (context),
                                                        _("Brush"), 2,
                                                        "view-type",
                                                        "view-size",
                                                        NULL,
                                                        NULL);

  force_custom_icon (editor->priv->brush_picker, GIMP_ICON_BRUSH);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->brush_picker;
}

static GtkWidget *
get_dynamics_picker (GimpContext          *context,
                     GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->dynamics_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->dynamics_picker = gimp_prop_dynamics_box_new (GIMP_CONTAINER (data),
                                                              GIMP_CONTEXT (context),
                                                              _("Dynamics"), 2,
                                                              "view-type",
                                                              "view-size",
                                                              NULL,
                                                              NULL);

  force_custom_icon (editor->priv->dynamics_picker, GIMP_ICON_DYNAMICS);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->dynamics_picker;
}

static GtkWidget *
get_mybrush_picker (GimpContext          *context,
                    GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->mybrush_factory);


  g_object_set_data (G_OBJECT (data), "view-type",
                     GINT_TO_POINTER (GIMP_VIEW_TYPE_GRID));
  g_object_set_data (G_OBJECT (data), "view-size",
                     GINT_TO_POINTER (GIMP_VIEW_SIZE_LARGE));

  editor->priv->mybrush_picker = gimp_prop_mybrush_box_new (GIMP_CONTAINER (data),
                                                            GIMP_CONTEXT (context),
                                                            _("MyBrush"), 2,
                                                            NULL,
                                                            NULL);

  force_custom_icon (editor->priv->mybrush_picker, GIMP_ICON_MYPAINT_BRUSH);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->mybrush_picker;
}

static GtkWidget *
get_gradient_picker (GimpContext          *context,
                     GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->gradient_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->gradient_picker = gimp_prop_gradient_box_new (GIMP_CONTAINER (data),
                                                              GIMP_CONTEXT (context),
                                                              _("Gradient"), 2,
                                                              "view-type",
                                                              "view-size",
                                                              NULL,
                                                              NULL,
                                                              NULL,
                                                              NULL);

  force_custom_icon (editor->priv->gradient_picker, GIMP_ICON_GRADIENT);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->gradient_picker;
}

static GtkWidget *
get_pattern_picker (GimpContext          *context,
                    GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->pattern_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->pattern_picker = gimp_prop_pattern_box_new (GIMP_CONTAINER (data),
                                                            GIMP_CONTEXT (context),
                                                            _("Pattern"), 2,
                                                            "view-type",
                                                            "view-size");

  force_custom_icon (editor->priv->pattern_picker, GIMP_ICON_PATTERN);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->pattern_picker;
}

static GtkWidget *
get_palette_picker (GimpContext          *context,
                    GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->palette_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->palette_picker = gimp_prop_palette_box_new (GIMP_CONTAINER (data),
                                                            GIMP_CONTEXT (context),
                                                            _("Palette"), 2,
                                                            "view-type",
                                                            "view-size",
                                                            NULL,
                                                            NULL);

  force_custom_icon (editor->priv->palette_picker, GIMP_ICON_PALETTE);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->palette_picker;
}

static GtkWidget *
get_font_picker (GimpContext          *context,
                 GimpToolPresetEditor *editor)
{
  GimpContainer *data = gimp_data_factory_get_container (context->gimp->palette_factory);

  set_view_properties (G_OBJECT (data));

  editor->priv->font_picker = gimp_prop_font_box_new (GIMP_CONTAINER (data),
                                                      GIMP_CONTEXT (context),
                                                      _("Font"), 2,
                                                      "view-type",
                                                      "view-size");

  force_custom_icon (editor->priv->font_picker, GIMP_ICON_FONT);
  clear_view_properties (G_OBJECT (data));

  return editor->priv->font_picker;
}

static void
update_reload_resource_toggle (GtkToggleButton    *toggle_button,
                               ResourceToggleData *toggle_data)
{
  const gchar *icon_name;
  const gchar *help_text;

  if (gtk_toggle_button_get_active (toggle_button))
    {
      if (toggle_data->broken_link)
        {
          icon_name = toggle_data->icon_name_active_broken;
          help_text = toggle_data->help_text_active_broken;
        }
      else
        {
          icon_name = toggle_data->icon_name_active;
          help_text = toggle_data->help_text_active;
        }
    }
  else
    {
      if (toggle_data->broken_link)
        {
          icon_name = toggle_data->icon_name_inactive_broken;
          help_text = toggle_data->help_text_inactive_broken;
        }
      else
        {
          icon_name = toggle_data->icon_name_inactive;
          help_text = toggle_data->help_text_inactive;
        }
    }

  gtk_image_set_from_icon_name (GTK_IMAGE (toggle_data->image),
                                icon_name,
                                GTK_ICON_SIZE_LARGE_TOOLBAR);

  gimp_help_set_help_data (GTK_WIDGET (toggle_button), help_text, NULL);
}

static GtkWidget *
create_toggle_button_with_resource_data (GimpToolPresetEditor *editor,
                                         GtkWidget            *reload_checkbox,
                                         const gchar          *icon_name_active,
                                         const gchar          *icon_name_active_broken,
                                         const gchar          *icon_name_inactive,
                                         const gchar          *icon_name_inactive_broken,
                                         const gchar          *help_text_active,
                                         const gchar          *help_text_active_broken,
                                         const gchar          *help_text_inactive,
                                         const gchar          *help_text_inactive_broken,
                                         gboolean              initial_state)
{
  GtkWidget          *toggle_button;
  ResourceToggleData *toggle_data;

  /* Create the toggle button */
  toggle_button = gtk_toggle_button_new ();

  /* Initialize ResourceToggleData */
  toggle_data = g_new0 (ResourceToggleData, 1);
  toggle_data->icon_name_active          = icon_name_active;
  toggle_data->icon_name_active_broken   = icon_name_active_broken;
  toggle_data->icon_name_inactive        = icon_name_inactive;
  toggle_data->icon_name_inactive_broken = icon_name_inactive;
  toggle_data->help_text_active          = help_text_active;
  toggle_data->help_text_active_broken   = help_text_active_broken;
  toggle_data->help_text_inactive        = help_text_inactive;
  toggle_data->help_text_inactive_broken = help_text_inactive_broken;
  toggle_data->broken_link               = FALSE;
  toggle_data->editor                    = editor;

  /* Create the image and add it to the toggle button */
  toggle_data->image = gtk_image_new_from_icon_name (toggle_data->icon_name_active,
                                                     GTK_ICON_SIZE_LARGE_TOOLBAR);
  gtk_container_add (GTK_CONTAINER (toggle_button), toggle_data->image);
  gtk_widget_show (toggle_data->image);

  /* Manage memory for toggle_data */
  g_object_set_data_full (G_OBJECT (toggle_button), "toggle-data", toggle_data, g_free);

  /* Initialize the toggle state */
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle_button), initial_state);

  /* Show the toggle button */
  gtk_widget_show (toggle_button);

  /* Connect the toggled signal with the appropriate update function */
  gtk_widget_set_sensitive (toggle_button, gtk_widget_get_sensitive (reload_checkbox));

  g_signal_connect (toggle_button, "toggled",
                    G_CALLBACK (update_reload_resource_toggle),
                    toggle_data);

  g_object_bind_property (reload_checkbox, "active",
                          toggle_button, "active",
                          G_BINDING_BIDIRECTIONAL);

  /* Bind properties for synchronization */
  g_object_bind_property (reload_checkbox, "sensitive",
                          toggle_button, "sensitive",
                          G_BINDING_DEFAULT);

  return toggle_button;
}

static GtkWidget *
create_link_resource_toggle (GimpToolPresetEditor *editor,
                             GtkWidget            *reload_checkbox)
{
  gboolean initial_state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (reload_checkbox));

  return create_toggle_button_with_resource_data (
    editor,
    reload_checkbox,
    GIMP_ICON_CHAIN_HORIZONTAL,
    GIMP_ICON_CHAIN_HORIZONTAL_DATA_LOSS,
    GIMP_ICON_CHAIN_HORIZONTAL_BROKEN,
    GIMP_ICON_CHAIN_HORIZONTAL_DATA_LOSS,
    _("Toggle <b>on</b>: Reloading a tool preset\n"
      "restores the resource link to the saved value."),
    _("Toggle <b>on</b>: Active resource link is out of sync\n"
      "with the preset's saved link."),
    _("Toggle <b>off</b>: Reloading a tool preset\n"
      "does not change the resource link."),
    _("Toggle <b>off</b>: Active resource link is out of sync\n"
      "with the preset's saved resource link, reloading\n"
      "a preset does not change the resource link."),
    initial_state);
}

static GtkWidget *
create_link_option_toggle (GimpToolPresetEditor *editor,
                           GtkWidget            *reload_checkbox)
{
  gboolean initial_state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (reload_checkbox));

  return create_toggle_button_with_resource_data (
    editor,
    reload_checkbox,
    GIMP_ICON_CHAIN_HORIZONTAL,
    GIMP_ICON_CHAIN_HORIZONTAL_DATA_LOSS,
    GIMP_ICON_CHAIN_HORIZONTAL_BROKEN,
    GIMP_ICON_CHAIN_HORIZONTAL_DATA_LOSS,
    _("Toggle <b>on</b>: Reloading a tool preset\n"
      "restores the tool option to the saved value."),
    _("Toggle <b>on</b>: Active tool option out of sync\n"
      "with the preset's saved tool option value."),
    _("Toggle <b>off</b>: Reloading a tool preset\n"
      "does not change the tool option value."),
    _("Toggle <b>off</b>: Active tool option out of sync with the\n"
      "preset's saved tool option value, reloading a preset does\n"
      "not change the tool option."),
    initial_state);
}

static GtkWidget *
create_picker_with_toggles (GimpToolPresetEditor *editor,
                            GtkWidget            *picker_widget,
                            GtkWidget            *reload_checkbox,
                            GtkWidget           **link_resource_toggle)
{
  GtkWidget *hbox;

  // Create a horizontal box to hold the picker and toggles
  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_widget_set_visible (hbox, TRUE);

  /* Bind sensitive property for synchronization */
  g_object_bind_property (reload_checkbox, "sensitive",
                          hbox , "visible",
                          G_BINDING_DEFAULT);

  // Add the picker widget to the hbox
  gtk_box_pack_start (GTK_BOX (hbox), picker_widget, TRUE, TRUE, 2);
  gtk_widget_set_visible (picker_widget, TRUE);

  // Create and add the reload resource toggle
  *link_resource_toggle = create_link_resource_toggle (editor, reload_checkbox);
  gtk_box_pack_end (GTK_BOX (hbox), *link_resource_toggle, FALSE, FALSE, 2);

  return hbox;
}

static GtkWidget *
add_picker_to_vbox (GtkWidget            *vbox,
                    GimpToolPresetEditor *editor,
                    GtkWidget            *(*get_picker_func) (GimpContext *, GimpToolPresetEditor *editor),
                    GtkWidget            *reload_checkbox,
                    GtkWidget           **link_resource_toggle,
                    GimpContext          *context)
{
  GtkWidget *picker_widget;
  GtkWidget *picker_with_toggles;

  // Retrieve the picker widget using the provided getter function
  picker_widget = get_picker_func (context, editor);
  if (! picker_widget)
    {
      g_warning ("Failed to retrieve picker widget.");
      return NULL;
    }

  // Initialize the picker state manually to avoid SYNC binding issues
  gtk_widget_set_sensitive (picker_widget, gtk_widget_get_sensitive (reload_checkbox));

  // Create a horizontal box containing the picker and its toggles
  picker_with_toggles = create_picker_with_toggles (editor, picker_widget,
                                                    reload_checkbox,
                                                    link_resource_toggle);
  if (! picker_with_toggles)
    {
      g_warning ("Failed to create picker with toggles.");
      return NULL;
    }

  // Pack the hbox into the vbox
  gtk_box_pack_start (GTK_BOX (vbox), picker_with_toggles, FALSE, FALSE, 2);
  gtk_widget_set_visible (picker_with_toggles, TRUE);

  return picker_with_toggles;
}

static void
on_dynamics_change (GObject    *config,
                    GParamSpec *pspec,
                    GtkWidget  *dynamics_box)
{
  gtk_widget_set_visible (dynamics_box, GIMP_PAINT_OPTIONS (config)->dynamics_enabled);
}

static void
preset_resources (GimpDataEditor *data_editor,
                  GimpContext    *context)
{
  GimpToolPresetEditor *editor        = GIMP_TOOL_PRESET_EDITOR (data_editor);
  GimpPaintOptions     *paint_options = (gimp_context_get_paint_info (context))->paint_options;

  GtkWidget *expander;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *dynamics_box;
  GtkWidget *scrolled_window;

  g_return_if_fail (GIMP_IS_CONTEXT (context));

  // Create an expander for active resources
  expander = gtk_expander_new (_("Preset Resources"));
  gtk_widget_set_tooltip_text (expander,
      _("Active resources that are linked in\n"
        "the tool preset file when saved."));
  editor->priv->link_expander = expander;
  gtk_expander_set_expanded (GTK_EXPANDER (expander), TRUE);

  // Create a frame and add it to the expander
  frame = gimp_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (expander), frame);
  gtk_widget_set_visible (frame, TRUE);

  // Create a scrolled window and add it to the frame
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_NEVER,
                                  GTK_POLICY_AUTOMATIC);
  gtk_widget_set_vexpand (scrolled_window, TRUE);
  gtk_widget_set_hexpand (scrolled_window, TRUE);
  gtk_container_add (GTK_CONTAINER (frame), scrolled_window);
  gtk_widget_set_visible (scrolled_window, TRUE);

  // Create a vertical box to hold all pickers
  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_container_add (GTK_CONTAINER (scrolled_window), vbox);
  gtk_widget_set_visible (vbox, TRUE);

  // Add pickers and their toggles
  // Brush picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_brush_picker,
                      editor->priv->brush_toggle,
                      &editor->priv->link_brush_toggle,
                      context);

  // Dynamics picker and resource toggles
  dynamics_box = add_picker_to_vbox (vbox, editor, get_dynamics_picker,
                                     editor->priv->dynamics_toggle,
                                     &editor->priv->link_dynamics_toggle,
                                     context);

  // MyBrush picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_mybrush_picker,
                      editor->priv->mybrush_toggle,
                      &editor->priv->link_mybrush_toggle,
                      context);

  // Gradient picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_gradient_picker,
                      editor->priv->gradient_toggle,
                      &editor->priv->link_gradient_toggle,
                      context);

  // Palette picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_palette_picker,
                      editor->priv->palette_toggle,
                      &editor->priv->link_palette_toggle,
                      context);

  // Pattern picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_pattern_picker,
                      editor->priv->pattern_toggle,
                      &editor->priv->link_pattern_toggle,
                      context);

  // Font picker and resource toggles
  add_picker_to_vbox (vbox, editor, get_font_picker,
                      editor->priv->font_toggle,
                      &editor->priv->link_font_toggle,
                      context);

  // Context aware dynamics box
  g_signal_connect (G_OBJECT (paint_options), "notify::dynamics-enabled",
                    G_CALLBACK (on_dynamics_change),
                    dynamics_box);

  // Pack the expander into the main data editor
  gtk_box_pack_start (GTK_BOX (data_editor), expander, TRUE, TRUE, 2);
  gtk_widget_set_visible (expander, TRUE);
  gtk_widget_set_sensitive (expander, context->tool_preset_name != NULL);
}

static void
preset_tool_options (GimpDataEditor *data_editor,
                     GimpContext    *context)
{
  GimpToolPresetEditor *editor = GIMP_TOOL_PRESET_EDITOR (data_editor);
  GtkWidget *vbox, *hbox, *frame;
  GObject   *config;

  g_return_if_fail (GIMP_IS_CONTEXT (context));

  config = G_OBJECT (context->paint_info->paint_options);

  // Create an expander for active resources
  editor->priv->options_expander = gtk_expander_new (_("Preset Options"));
  gtk_widget_set_tooltip_text (editor->priv->options_expander, _("Active tool options that are saved\n"
                                                           "as data in the tool preset file."));
  gtk_expander_set_expanded (GTK_EXPANDER (editor->priv->options_expander), TRUE);

  // Create a frame and add it to the expander
  frame = gimp_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (editor->priv->options_expander ), frame);
  gtk_widget_set_visible (frame, TRUE);

  // Create a vertical box to hold the options
  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  gtk_widget_set_visible (vbox, TRUE);

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 2);
  gtk_widget_set_visible (hbox, TRUE);

  g_object_bind_property (editor->priv->paint_mode_toggle, "sensitive",
                          hbox, "visible",
                          G_BINDING_DEFAULT);

  editor->priv->mode_picker = gimp_prop_layer_mode_box_new (config, "paint-mode",
                                                    GIMP_LAYER_MODE_CONTEXT_PAINT);
  gimp_layer_mode_box_set_label (GIMP_LAYER_MODE_BOX (editor->priv->mode_picker), _("Mode"));
  gimp_layer_mode_box_set_ellipsize (GIMP_LAYER_MODE_BOX (editor->priv->mode_picker), PANGO_ELLIPSIZE_END);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->mode_picker, TRUE, TRUE, 2);
  editor->priv->link_paint_mode_toggle = create_link_option_toggle (editor, editor->priv->paint_mode_toggle);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->link_paint_mode_toggle, FALSE, FALSE, 2);

  // Initialize the picker state manually to avoid sync binding issues
  gtk_widget_set_sensitive (editor->priv->mode_picker,
                            gtk_widget_get_sensitive (editor->priv->paint_mode_toggle));

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 2);
  gtk_widget_set_visible (hbox, TRUE);

  g_object_bind_property (editor->priv->opacity_toggle, "sensitive",
                          hbox, "visible",
                          G_BINDING_DEFAULT);

  editor->priv->opacity_slider = gimp_prop_spin_scale_new (config, "opacity", 0.01, 0.1, 0);
  gimp_spin_scale_set_constrain_drag (GIMP_SPIN_SCALE (editor->priv->opacity_slider), TRUE);
  gimp_prop_widget_set_factor (editor->priv->opacity_slider, 100.0, 1.0, 10.0, 1);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->opacity_slider, TRUE, TRUE, 0);
  editor->priv->link_opacity_toggle = create_link_option_toggle (editor, editor->priv->opacity_toggle);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->link_opacity_toggle, FALSE, FALSE, 2);

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 2);
  gtk_widget_set_visible (hbox, TRUE);

  g_object_bind_property (editor->priv->foreground_toggle, "sensitive",
                          hbox, "visible",
                          G_BINDING_DEFAULT);

  editor->priv->foreground_picker = gimp_prop_color_button_new (config, "foreground", _("Foreground Color"),
                                                          40, 24, GIMP_COLOR_AREA_FLAT);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->foreground_picker, TRUE, TRUE, 0);
  gimp_color_panel_set_context (GIMP_COLOR_PANEL (editor->priv->foreground_picker), context);

  editor->priv->link_foreground_toggle = create_link_option_toggle (editor, editor->priv->foreground_toggle);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->link_foreground_toggle, FALSE, FALSE, 2);

  editor->priv->background_picker = gimp_prop_color_button_new (config, "background", _("Background Color"),
                                                          40, 24, GIMP_COLOR_AREA_FLAT);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->background_picker, TRUE, TRUE, 0);
  gimp_color_panel_set_context (GIMP_COLOR_PANEL (editor->priv->background_picker), context);

  editor->priv->link_background_toggle = create_link_option_toggle (editor, editor->priv->background_toggle);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->link_background_toggle, FALSE, FALSE, 2);

  // Pack the expander into the main data editor
  gtk_box_pack_start (GTK_BOX (data_editor), editor->priv->options_expander, FALSE, FALSE, 2);
  gtk_widget_set_visible (editor->priv->options_expander , TRUE);
  gtk_widget_set_sensitive (editor->priv->options_expander, context->tool_preset_name != NULL);
}

static void
on_tool_changed (GimpContext  *context,
                 GimpToolInfo *tool,
                 gpointer      user_data)
{
  GtkWidget *button = GTK_WIDGET (user_data);

  if (context->tool_preset)
    {
      GimpToolInfo *preset_tool = context->tool_preset->tool_options->tool_info;

      gtk_widget_set_sensitive (button, (preset_tool == tool));
    }
}

static void
connect_signals (GimpContext          *context,
                 GimpToolPresetEditor *editor)
{
  /* Define the signal-to-callback mappings */
  struct {
    const gchar *signal_name; /* Signal name to listen for */
    GCallback    callback;    /* Callback function to invoke */
  } connections[] = {
    { "pattern-changed",      G_CALLBACK (update_pattern_info) },
    { "tool-preset-changed",  G_CALLBACK (update_pattern_info) },
    { "palette-changed",      G_CALLBACK (update_palette_info) },
    { "tool-preset-changed",  G_CALLBACK (update_palette_info) },
    { "font-changed",         G_CALLBACK (update_font_info) },
    { "tool-preset-changed",  G_CALLBACK (update_font_info) },
    { "gradient-changed",     G_CALLBACK (update_gradient_info) },
    { "tool-preset-changed",  G_CALLBACK (update_gradient_info) },
    { "brush-changed",        G_CALLBACK (update_brush_info) },
    { "tool-preset-changed",  G_CALLBACK (update_brush_info) },
    { "mybrush-changed",      G_CALLBACK (update_mybrush_info) },
    { "tool-preset-changed",  G_CALLBACK (update_mybrush_info) },
    { "dynamics-changed",     G_CALLBACK (update_dynamics_info) },
    { "tool-preset-changed",  G_CALLBACK (update_dynamics_info) },
    { "paint-mode-changed",   G_CALLBACK (update_paint_mode_info) },
    { "tool-preset-changed",  G_CALLBACK (update_paint_mode_info) },
    { "opacity-changed",      G_CALLBACK (update_opacity_info) },
    { "tool-preset-changed",  G_CALLBACK (update_opacity_info) },
    { "foreground-changed",   G_CALLBACK (update_foreground_info) },
    { "tool-preset-changed",  G_CALLBACK (update_foreground_info) },
    { "background-changed",   G_CALLBACK (update_background_info) },
    { "tool-preset-changed",  G_CALLBACK (update_background_info) },
  };

  /* Loop through the connections array and connect each signal */
  for (gint i = 0; i < G_N_ELEMENTS (connections); i++)
    {
      g_signal_connect_swapped (context,
                                connections[i].signal_name,
                                connections[i].callback,
                                editor);
    }
}

static void
setup_tool_preset_model (GimpToolPresetEditor *editor,
                         GimpContext          *context)
{
  GimpToolPreset *preset;

  preset = editor->priv->tool_preset_model =
    g_object_new (GIMP_TYPE_TOOL_PRESET, "gimp", context->gimp, NULL);

  g_signal_connect (preset, "notify",
                    G_CALLBACK (gimp_tool_preset_editor_notify_model),
                    editor);
}

static GtkWidget *
create_tool_preset_icon_picker (GimpToolPresetEditor *editor,
                       GimpDataEditor       *data_editor,
                       GimpContext          *context)
{
  GtkWidget *hbox;
  GtkWidget *button;

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_box_pack_start (GTK_BOX (data_editor), hbox, FALSE, FALSE, 0);
  gtk_widget_set_visible (hbox, TRUE);

  editor->priv->tool_icon = gtk_image_new ();
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->tool_icon,
                      FALSE, FALSE, 0);
  gtk_widget_set_visible (editor->priv->tool_icon, TRUE);

  editor->priv->tool_label = gtk_label_new ("");
  gimp_label_set_attributes (GTK_LABEL (editor->priv->tool_label),
                             PANGO_ATTR_STYLE, PANGO_STYLE_ITALIC, -1);
  gtk_box_pack_start (GTK_BOX (hbox), editor->priv->tool_label,
                      FALSE, FALSE, 16);
  gtk_widget_set_visible (editor->priv->tool_label, TRUE);

  button = gimp_prop_icon_picker_new (GIMP_VIEWABLE (editor->priv->tool_preset_model),
                                      context->gimp, TRUE);
  gtk_widget_set_tooltip_text (button, "Create a new icon");
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_widget_set_visible (button, TRUE);

  return hbox;
}

static void
setup_action_buttons (GimpToolPresetEditor *editor,
                      GimpDataEditor       *data_editor,
                      GimpContext          *context)
{
  GtkWidget *button;

  button = editor->priv->save_button = gimp_editor_add_action_button (
                                         GIMP_EDITOR (editor),
                                         "tool-preset-editor",
                                         "tool-preset-editor-save", NULL);
  gtk_widget_set_sensitive (button, FALSE);
  gtk_widget_set_visible (button, FALSE);

  g_signal_connect (context, "tool-changed",
                    G_CALLBACK (on_tool_changed),
                    editor->priv->save_button);

  button = editor->priv->save_as_button = gimp_editor_add_action_button (
                                            GIMP_EDITOR (editor),
                                            "tool-preset-editor",
                                            "tool-preset-editor-save-as", NULL);
  gtk_widget_set_sensitive (button, FALSE);
  gtk_widget_set_visible (button, FALSE);

  button = editor->priv->save_resources_button = gimp_editor_add_action_button (
                                                   GIMP_EDITOR (editor),
                                                   "tool-preset-editor",
                                                   "tool-preset-editor-save-resources", NULL);
  gtk_widget_set_sensitive (button, FALSE);

  g_signal_connect (context, "tool-changed",
                    G_CALLBACK (on_tool_changed),
                    editor->priv->save_resources_button);

  button = editor->priv->save_as_resources_button = gimp_editor_add_action_button (
                                                      GIMP_EDITOR (editor),
                                                      "tool-preset-editor",
                                                     "tool-preset-editor-save-resources-as", NULL);
  gtk_widget_set_sensitive (button, FALSE);

  button = editor->priv->save_all_button = gimp_editor_add_action_button (
                                            GIMP_EDITOR (editor),
                                            "tool-preset-editor",
                                            "tool-preset-editor-save-all", NULL);
  gtk_widget_set_sensitive (button, FALSE);
}

static void
setup_style_update_signals (GimpToolPresetEditor *editor,
                            GimpContext          *context)
{
  g_signal_connect_object (context->gimp->config,
                           "notify::override-theme-icon-size",
                           G_CALLBACK (gimp_tool_preset_editor_style_updated),
                           editor, G_CONNECT_AFTER | G_CONNECT_SWAPPED);

  g_signal_connect_object (context->gimp->config,
                           "notify::custom-icon-size",
                           G_CALLBACK (gimp_tool_preset_editor_style_updated),
                           editor, G_CONNECT_AFTER | G_CONNECT_SWAPPED);
}

static void
gimp_tool_preset_editor_constructed (GObject *object)
{
  GimpToolPresetEditor *editor      = GIMP_TOOL_PRESET_EDITOR (object);
  GimpDataEditor       *data_editor = GIMP_DATA_EDITOR (editor);
  GimpContext          *context     = data_editor->context;

  G_OBJECT_CLASS (parent_class)->constructed (object);

  setup_tool_preset_model (editor, context);
  create_tool_preset_icon_picker (editor, data_editor, context);

  preset_application_filter (data_editor, editor->priv->tool_preset_model);
  preset_tool_options (data_editor, context);
  preset_resources (data_editor, context);

  setup_action_buttons (editor, data_editor, context);
  setup_style_update_signals (editor, context);
  connect_signals (context, editor);

  if (data_editor->data)
    gimp_tool_preset_editor_sync_data (editor);
}

static void
gimp_tool_preset_editor_finalize (GObject *object)
{
  GimpToolPresetEditor *editor = GIMP_TOOL_PRESET_EDITOR (object);

  g_clear_object (&editor->priv->tool_preset_model);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gimp_tool_preset_editor_style_updated (GtkWidget *widget)
{
  GimpToolPresetEditor *editor    = GIMP_TOOL_PRESET_EDITOR (widget);
  gint                  icon_size = 16;
  GtkIconSize           button_icon_size;

  GTK_WIDGET_CLASS (parent_class)->style_updated (widget);

  gtk_widget_style_get (widget,
                        "button-icon-size", &button_icon_size,
                        NULL);
  gtk_icon_size_lookup (button_icon_size, &icon_size, NULL);
  gtk_image_set_pixel_size (GTK_IMAGE (editor->priv->tool_icon), icon_size);
}


static void
gimp_tool_preset_editor_set_data (GimpDataEditor *editor,
                                  GimpData       *data)
{
  GimpToolPresetEditor *preset_editor = GIMP_TOOL_PRESET_EDITOR (editor);

  if (editor->data)
    g_signal_handlers_disconnect_by_func (editor->data,
                                          gimp_tool_preset_editor_notify_data,
                                          editor);

  GIMP_DATA_EDITOR_CLASS (parent_class)->set_data (editor, data);

  if (editor->data)
    {
      g_signal_connect (editor->data, "notify",
                        G_CALLBACK (gimp_tool_preset_editor_notify_data),
                        editor);

      if (preset_editor->priv->tool_preset_model)
        gimp_tool_preset_editor_sync_data (preset_editor);
    }
}

static void
update_expander_visibility (GtkWidget *expander)
{
  GtkWidget *frame     = NULL;
  GtkWidget *container = NULL;
  GList     *children;

  gboolean any_box_visible = FALSE;

  frame = gtk_bin_get_child (GTK_BIN (expander));
  if (!frame)
    return;

  // Traverse down the widget hierarchy to find the GtkBox
  container = frame;
  while (container && ! GTK_IS_BOX (container))
    {
      if (GTK_IS_BIN(container))
        container = gtk_bin_get_child (GTK_BIN (container));
      else
        container = NULL;
    }

  if (!container)
      return;

  children = gtk_container_get_children (GTK_CONTAINER (container));
  for (GList *iter = children; iter != NULL; iter = iter->next)
    {
      GtkWidget *child = GTK_WIDGET (iter->data);

      // Check if the child is a GtkBox and is visible
      if (GTK_IS_BOX (child) && gtk_widget_get_visible (child))
        {
          any_box_visible = TRUE;
          break; // No need to check further if one box is visible
        }
    }

  g_list_free (children);

  gtk_widget_set_visible (expander, any_box_visible);
}

static void
sync_tool_preset_model (GimpToolPresetEditor *editor)
{
  GimpToolPresetEditorPrivate *priv        = editor->priv;
  GimpDataEditor              *data_editor = GIMP_DATA_EDITOR (editor);

  g_signal_handlers_block_by_func (priv->tool_preset_model,
                                   gimp_tool_preset_editor_notify_model,
                                   editor);

  gimp_config_sync (G_OBJECT (data_editor->data),
                    G_OBJECT (priv->tool_preset_model),
                    GIMP_CONFIG_PARAM_SERIALIZE);

  g_signal_handlers_unblock_by_func (priv->tool_preset_model,
                                     gimp_tool_preset_editor_notify_model,
                                     editor);
}

static void
update_tool_info (GimpToolPresetEditor *editor,
                  GimpToolInfo         *tool_info)
{
  GimpToolPresetEditorPrivate *priv = editor->priv;
  const gchar                 *icon_name;
  gchar                       *label;

  icon_name = gimp_viewable_get_icon_name (GIMP_VIEWABLE (tool_info));
  label     = g_strdup_printf (_("%s Preset"), tool_info->label);

  gtk_image_set_from_icon_name (GTK_IMAGE (priv->tool_icon),
                                icon_name, GTK_ICON_SIZE_MENU);
  gtk_label_set_text (GTK_LABEL (priv->tool_label), label);

  g_free (label);
}

static void
update_toggle_sensitivity (GimpToolPresetEditorPrivate *priv,
                           GimpContextPropMask         serialize_props)
{
  gtk_widget_set_sensitive (priv->foreground_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_FOREGROUND) != 0);
  gtk_widget_set_sensitive (priv->background_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_BACKGROUND) != 0);
  gtk_widget_set_sensitive (priv->opacity_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_OPACITY) != 0);
  gtk_widget_set_sensitive (priv->paint_mode_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_PAINT_MODE) != 0);
  gtk_widget_set_sensitive (priv->brush_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_BRUSH) != 0);
  gtk_widget_set_sensitive (priv->dynamics_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_DYNAMICS) != 0);
  gtk_widget_set_sensitive (priv->mybrush_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_MYBRUSH) != 0);
  gtk_widget_set_sensitive (priv->gradient_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_GRADIENT) != 0);
  gtk_widget_set_sensitive (priv->pattern_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_PATTERN) != 0);
  gtk_widget_set_sensitive (priv->palette_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_PALETTE) != 0);
  gtk_widget_set_sensitive (priv->font_toggle,
                            (serialize_props &
                             GIMP_CONTEXT_PROP_MASK_FONT) != 0);
}

static void
update_editor_sensitivity (GimpToolPresetEditor *editor,
                           GimpDataEditor       *data_editor,
                           GimpToolPreset       *preset)
{
  gtk_widget_set_sensitive (editor->priv->save_button,
                            (preset != NULL &&
                             gimp_data_is_writable (data_editor->data)));
  gtk_widget_set_sensitive (editor->priv->save_as_button,
                            preset != NULL);
  gtk_widget_set_sensitive (editor->priv->save_resources_button,
                            (preset != NULL &&
                             gimp_data_is_writable (data_editor->data)));
  gtk_widget_set_sensitive (editor->priv->save_as_resources_button,
                            preset != NULL);
  gtk_widget_set_sensitive (editor->priv->save_all_button,
                            preset != NULL);
  gtk_widget_set_sensitive (editor->priv->load_expander,
                            preset != NULL);
  gtk_widget_set_sensitive (editor->priv->options_expander,
                            preset != NULL);
  gtk_widget_set_sensitive (editor->priv->link_expander,
                            preset != NULL);

  update_expander_visibility (editor->priv->options_expander);
  update_expander_visibility (editor->priv->link_expander);
}

static void
gimp_tool_preset_editor_sync_data (GimpToolPresetEditor *editor)
{
  GimpToolPresetEditorPrivate *priv        = editor->priv;
  GimpDataEditor              *data_editor = GIMP_DATA_EDITOR (editor);
  GimpContext                 *context     = data_editor->context;
  GimpToolPreset              *preset;
  GimpToolInfo                *tool_info;
  GimpContextPropMask          serialize_props;

  sync_tool_preset_model (editor);

  if (! priv->tool_preset_model->tool_options)
    return;

  tool_info = priv->tool_preset_model->tool_options->tool_info;
  update_tool_info (editor, tool_info);

  preset = GIMP_TOOL_PRESET (data_editor->data);
  gtk_widget_set_sensitive (editor->priv->link_expander, context->tool_preset_name != NULL);

  serialize_props =
    gimp_context_get_serialize_properties (GIMP_CONTEXT (preset->tool_options));

  update_toggle_sensitivity (priv, serialize_props);
  update_editor_sensitivity (editor, data_editor, preset);
}

static void
gimp_tool_preset_editor_notify_model (GimpToolPreset       *options,
                                      const GParamSpec     *pspec,
                                      GimpToolPresetEditor *editor)
{
  GimpDataEditor *data_editor = GIMP_DATA_EDITOR (editor);

  if (data_editor->data)
    {
      g_signal_handlers_block_by_func (data_editor->data,
                                       gimp_tool_preset_editor_notify_data,
                                       editor);

      gimp_config_sync (G_OBJECT (editor->priv->tool_preset_model),
                        G_OBJECT (data_editor->data),
                        GIMP_CONFIG_PARAM_SERIALIZE);

      g_signal_handlers_unblock_by_func (data_editor->data,
                                         gimp_tool_preset_editor_notify_data,
                                         editor);
    }
}

static void
gimp_tool_preset_editor_notify_data (GimpToolPreset       *options,
                                     const GParamSpec     *pspec,
                                     GimpToolPresetEditor *editor)
{
  GimpDataEditor *data_editor = GIMP_DATA_EDITOR (editor);

  g_signal_handlers_block_by_func (editor->priv->tool_preset_model,
                                   gimp_tool_preset_editor_notify_model,
                                   editor);

  gimp_config_sync (G_OBJECT (data_editor->data),
                    G_OBJECT (editor->priv->tool_preset_model),
                    GIMP_CONFIG_PARAM_SERIALIZE);

  g_signal_handlers_unblock_by_func (editor->priv->tool_preset_model,
                                     gimp_tool_preset_editor_notify_model,
                                     editor);
}