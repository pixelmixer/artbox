/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gegl.h>

#include "core-types.h"

#include "gimp.h"
#include "gimpimage.h"
#include "gimppickable.h"
#include "gimppickable-auto-shrink.h"


typedef enum
{
  AUTO_SHRINK_NOTHING = 0,
  AUTO_SHRINK_ALPHA   = 1,
  AUTO_SHRINK_COLOR   = 2,
  AUTO_SHRINK_BLACK   = 3
} AutoShrinkType;


typedef gboolean (* ColorsEqualFunc) (guchar *col1,
                                      guchar *col2);


/*  local function prototypes  */
static AutoShrinkType   gimp_pickable_guess_bgcolor (GimpPickable      *pickable,
                                                     guchar            *color,
                                                     gint               x1,
                                                     gint               x2,
                                                     gint               y1,
                                                     gint               y2);

static gboolean         gimp_pickable_colors_equal  (guchar            *col1,
                                                     guchar            *col2);

static gboolean         gimp_pickable_colors_alpha  (guchar            *col1,
                                                     guchar            *col2);

static gboolean         gimp_pickable_colors_black  (guchar            *col1,
                                                     guchar            *col2);

static gint             shrink_top_boundary         (GeglBuffer        *buffer,
                                                     gint               x1,
                                                     gint               x2,
                                                     gint               y1,
                                                     gint               y2,
                                                     ColorsEqualFunc    colors_equal_func,
                                                     guchar            *bgcolor,
                                                     const Babl       *format,
                                                     guchar            *buf);

static gint             shrink_bottom_boundary      (GeglBuffer        *buffer,
                                                     gint               x1,
                                                     gint               x2,
                                                     gint               y1,
                                                     gint               y2,
                                                     ColorsEqualFunc    colors_equal_func,
                                                     guchar            *bgcolor,
                                                     const Babl       *format,
                                                     guchar            *buf);

static gint             shrink_left_boundary        (GeglBuffer        *buffer,
                                                     gint               x1,
                                                     gint               x2,
                                                     gint               y1,
                                                     gint               y2,
                                                     ColorsEqualFunc    colors_equal_func,
                                                     guchar            *bgcolor,
                                                     const Babl       *format,
                                                     guchar            *buf);

static gint             shrink_right_boundary       (GeglBuffer        *buffer,
                                                     gint               x1,
                                                     gint               x2,
                                                     gint               y1,
                                                     gint               y2,
                                                     ColorsEqualFunc    colors_equal_func,
                                                     guchar            *bgcolor,
                                                     const Babl       *format,
                                                     guchar            *buf);


GimpAutoShrink
gimp_pickable_auto_shrink(GimpPickable *pickable,
                          gint          start_x,
                          gint          start_y,
                          gint          start_width,
                          gint          start_height,
                          gint         *shrunk_x,
                          gint         *shrunk_y,
                          gint         *shrunk_width,
                          gint         *shrunk_height)
{
  GeglBuffer      *buffer;
  ColorsEqualFunc  colors_equal_func     = NULL;
  guchar           bgcolor[MAX_CHANNELS] = { 0, 0, 0, 0 };
  guchar          *buf = NULL;
  gint             x1, y1, x2, y2;
  gint             width, height;
  const Babl      *format;
  GimpAutoShrink   retval = GIMP_AUTO_SHRINK_UNSHRINKABLE;

  /* Parameter checks */
  g_return_val_if_fail(GIMP_IS_PICKABLE(pickable), FALSE);
  g_return_val_if_fail(shrunk_x != NULL, FALSE);
  g_return_val_if_fail(shrunk_y != NULL, FALSE);
  g_return_val_if_fail(shrunk_width != NULL, FALSE);
  g_return_val_if_fail(shrunk_height != NULL, FALSE);

  gimp_set_busy(gimp_pickable_get_image(pickable)->gimp);

  /* Prepare buffer and initial coordinates */
  gimp_pickable_flush(pickable);
  buffer = gimp_pickable_get_buffer(pickable);

  x1 = MAX(start_x, 0);
  y1 = MAX(start_y, 0);
  x2 = MIN(start_x + start_width,  gegl_buffer_get_width(buffer));
  y2 = MIN(start_y + start_height, gegl_buffer_get_height(buffer));

  /* Set default return values */
  *shrunk_x      = x1;
  *shrunk_y      = y1;
  *shrunk_width  = x2 - x1;
  *shrunk_height = y2 - y1;

  format = babl_format("R'G'B'A u8");

  /* Guess background color and set colors_equal_func */
  switch (gimp_pickable_guess_bgcolor(pickable, bgcolor, x1, x2 - 1, y1, y2 - 1))
    {
      case AUTO_SHRINK_ALPHA:
          colors_equal_func = gimp_pickable_colors_alpha;
          break;
      case AUTO_SHRINK_COLOR:
          colors_equal_func = gimp_pickable_colors_equal;
          break;
      case AUTO_SHRINK_BLACK:
          colors_equal_func = gimp_pickable_colors_black;
          break;
      default:
          goto FINISH;
    }

  width  = x2 - x1;
  height = y2 - y1;

  /* Allocate buffer */
  buf = g_malloc(MAX(width, height) * 4);

  /* Shrink top boundary */
  y1 = shrink_top_boundary(buffer, x1, x2, y1, y2, colors_equal_func, bgcolor, format, buf);
  if (y1 == -1)
    {
      if (colors_equal_func == gimp_pickable_colors_alpha)
          retval = GIMP_AUTO_SHRINK_EMPTY;
      else if (colors_equal_func == gimp_pickable_colors_black)
          retval = GIMP_AUTO_SHRINK_BLACK;
      else if (colors_equal_func == gimp_pickable_colors_equal)
          retval = GIMP_AUTO_SHRINK_UNIFORM;
      else
          retval = GIMP_AUTO_SHRINK_EMPTY;
      goto FINISH;
    }

  /* Shrink bottom boundary */
  y2 = shrink_bottom_boundary(buffer, x1, x2, y1, y2, colors_equal_func, bgcolor, format, buf);

  /* Update height */
  height = y2 - y1;

  /* Shrink left boundary */
  x1 = shrink_left_boundary(buffer, x1, x2, y1, y2, colors_equal_func, bgcolor, format, buf);

  /* Shrink right boundary */
  x2 = shrink_right_boundary(buffer, x1, x2, y1, y2, colors_equal_func, bgcolor, format, buf);

  if (x1      != start_x     ||
      y1      != start_y     ||
      x2 - x1 != start_width ||
      y2 - y1 != start_height)
    {
      *shrunk_x      = x1;
      *shrunk_y      = y1;
      *shrunk_width  = x2 - x1;
      *shrunk_height = y2 - y1;

      retval = GIMP_AUTO_SHRINK_SHRINK;
    }

FINISH:
  g_free(buf);
  gimp_unset_busy(gimp_pickable_get_image(pickable)->gimp);

  return retval;
}

static gint
shrink_top_boundary (GeglBuffer      *buffer,
                     gint             x1,
                     gint             x2,
                     gint             y1,
                     gint             y2,
                     ColorsEqualFunc  colors_equal_func,
                     guchar          *bgcolor,
                     const Babl      *format,
                     guchar          *buf)
{
    GeglRectangle rect;
    gint x, y;
    gint width = x2 - x1;
    gboolean abort = FALSE;

    rect.x      = x1;
    rect.width  = width;
    rect.height = 1;

    for (y = y1; y < y2 && !abort; y++)
      {
        rect.y = y;
        gegl_buffer_get(buffer, &rect, 1.0, format, buf,
                        GEGL_AUTO_ROWSTRIDE, GEGL_ABYSS_NONE);
        for (x = 0; x < width && !abort; x++)
            abort = !colors_equal_func(bgcolor, buf + x * 4);
      }

    if (y == y2 && !abort)
        return -1; /* Entire area is uniform background */

    return y - 1; /* New y1 */
}

static gint
shrink_bottom_boundary (GeglBuffer      *buffer,
                        gint             x1,
                        gint             x2,
                        gint             y1,
                        gint             y2,
                        ColorsEqualFunc  colors_equal_func,
                        guchar          *bgcolor,
                        const Babl      *format,
                        guchar          *buf)
{
  GeglRectangle rect;
  gint          x, y;
  gint          width = x2 - x1;
  gboolean      abort = FALSE;

  rect.x      = x1;
  rect.width  = width;
  rect.height = 1;

  for (y = y2; y > y1 && !abort; y--)
    {
      rect.y = y - 1;
      gegl_buffer_get(buffer, &rect, 1.0, format, buf,
                      GEGL_AUTO_ROWSTRIDE, GEGL_ABYSS_NONE);
      for (x = 0; x < width && !abort; x++)
          abort = !colors_equal_func(bgcolor, buf + x * 4);
    }

  return y + 1;
}

static gint
shrink_left_boundary (GeglBuffer      *buffer,
                      gint             x1,
                      gint             x2,
                      gint             y1,
                      gint             y2,
                      ColorsEqualFunc  colors_equal_func,
                      guchar          *bgcolor,
                      const Babl      *format,
                      guchar          *buf)
{
  GeglRectangle rect;
  gint          x, y;
  gint          height = y2 - y1;
  gboolean      abort  = FALSE;

  rect.y      = y1;
  rect.width  = 1;
  rect.height = height;

  for (x = x1; x < x2 && !abort; x++)
    {
      rect.x = x;
      gegl_buffer_get(buffer, &rect, 1.0, format, buf,
                      GEGL_AUTO_ROWSTRIDE, GEGL_ABYSS_NONE);
      for (y = 0; y < height && !abort; y++)
          abort = !colors_equal_func(bgcolor, buf + y * 4);
    }

  return x - 1;
}

static gint
shrink_right_boundary (GeglBuffer      *buffer,
                       gint             x1,
                       gint             x2,
                       gint             y1,
                       gint             y2,
                       ColorsEqualFunc  colors_equal_func,
                       guchar          *bgcolor,
                       const Babl      *format,
                       guchar          *buf)
{
  GeglRectangle rect;
  gint          x, y;
  gint          height = y2 - y1;
  gboolean      abort = FALSE;

  rect.y      = y1;
  rect.width  = 1;
  rect.height = height;

  for (x = x2; x > x1 && !abort; x--)
    {
      rect.x = x - 1;
      gegl_buffer_get (buffer, &rect, 1.0, format, buf,
                      GEGL_AUTO_ROWSTRIDE, GEGL_ABYSS_NONE);
      for (y = 0; y < height && !abort; y++)
          abort = !colors_equal_func (bgcolor, buf + y * 4);
    }

  return x + 1;
}

static AutoShrinkType
gimp_pickable_guess_bgcolor (GimpPickable *pickable,
                             guchar       *color,
                             gint          x1,
                             gint          x2,
                             gint          y1,
                             gint          y2)
{
  const Babl *format = babl_format("R'G'B'A u8");
  guchar      tl[4];
  guchar      tr[4];
  guchar      bl[4];
  guchar      br[4];
  gint        i;

  for (i = 0; i < 4; i++)
      color[i] = 0;

  /* Check if there's transparency to crop. If not, guess the
    * background color to see if at least 2 corners are equal.
    */

  if (!gimp_pickable_get_pixel_at(pickable, x1, y1, format, tl) ||
      !gimp_pickable_get_pixel_at(pickable, x1, y2, format, tr) ||
      !gimp_pickable_get_pixel_at(pickable, x2, y1, format, bl) ||
      !gimp_pickable_get_pixel_at(pickable, x2, y2, format, br))
    {
      return AUTO_SHRINK_NOTHING;
    }

  if ((tl[ALPHA] == 0 && tr[ALPHA] == 0) ||
      (tl[ALPHA] == 0 && bl[ALPHA] == 0) ||
      (tr[ALPHA] == 0 && br[ALPHA] == 0) ||
      (bl[ALPHA] == 0 && br[ALPHA] == 0))
    {
      return AUTO_SHRINK_ALPHA;
    }

  if (gimp_pickable_colors_black(NULL, tl) &&
      gimp_pickable_colors_black(NULL, tr) &&
      gimp_pickable_colors_black(NULL, bl) &&
      gimp_pickable_colors_black(NULL, br))
    {
      memcpy(color, tl, 4);
      return AUTO_SHRINK_BLACK;
    }

  if (gimp_pickable_colors_equal(tl, tr) ||
      gimp_pickable_colors_equal(tl, bl))
    {
      memcpy(color, tl, 4);
        return AUTO_SHRINK_COLOR;
    }

  if (gimp_pickable_colors_equal(br, bl) ||
      gimp_pickable_colors_equal(br, tr))
    {
      memcpy(color, br, 4);
      return AUTO_SHRINK_COLOR;
    }

  return AUTO_SHRINK_NOTHING;
}

/* Color comparison functions */

static gboolean
gimp_pickable_colors_equal (guchar *col1,
                            guchar *col2)
{
  gint b;

  for (b = 0; b < 4; b++)
    {
      if (col1[b] != col2[b])
        {
          return FALSE;
        }
    }
  return TRUE;
}

static gboolean
gimp_pickable_colors_alpha (guchar *dummy,
                            guchar *col)
{
  return (col[ALPHA] == 0);
}

static gboolean
gimp_pickable_colors_black (guchar *dummy,
                                guchar *col)
{
  return (col[RED] == 0 && col[GREEN] == 0 && col[BLUE] == 0 && col[ALPHA] == 255);
}
