/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include <ctype.h>

#include <gegl.h>
#include <gtk/gtk.h>

#include "actions-types.h"

#include "core/gimpcontext.h"
#include "core/gimpdatafactory.h"
#include "core/gimpdynamics.h"

#include "widgets/gimpdynamicseditor.h"
#include "widgets/gimpwidgets-utils.h"

#include "dynamics-editor-commands.h"
#include "data-editor-commands.h"

#include "dialogs/asset-save-as-dialog.h"

#include "gimp-intl.h"

/*  public functions  */

void
dynamics_editor_save_cmd_callback (GimpAction *action,
                                  GVariant   *value,
                                  gpointer    data)
{
  GimpDataEditor    *data_editor = GIMP_DATA_EDITOR (data);

  if (data_editor->data_editable)
    {
      gimp_data_factory_data_save_single (data_editor->data_factory,
                                          data_editor->data, NULL, NULL);
      gimp_blink_dockable (data_editor->context->gimp, "gimp-dynamics-editor", NULL, NULL, NULL);
    }
}

void
dynamics_editor_save_as_cmd_callback (GimpAction *action,
                                      GVariant   *value,
                                      gpointer    data)
{
  GimpDataEditor *data_editor = GIMP_DATA_EDITOR (data);
  GtkWidget      *dialog;

  dialog = resources_save_as_dialog (data, _("Save Dynamics"), "dynamics", TRUE);

  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
    {
      gchar *new_basename = create_and_assign_new_basename (dialog, "dynamics");

      if (is_valid_asset_name (new_basename, FALSE))
        {
          GimpData *new_dynamics;

          new_dynamics = gimp_data_factory_data_duplicate (data_editor->data_factory,
                                                           data_editor->data,
                                                           new_basename);

          gimp_context_set_dynamics (data_editor->context, GIMP_DYNAMICS (new_dynamics));
          g_free (new_basename);
        }
    }

  gtk_widget_destroy (dialog);
}
