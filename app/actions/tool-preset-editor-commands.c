/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <ctype.h>

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpconfig/gimpconfig.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "actions-types.h"

#include "config/gimpguiconfig.h"

#include "core/gimp.h"
#include "core/gimpbrush.h"
#include "core/gimpbrushgenerated.h"
#include "core/gimpbrushpipe.h"
#include "core/gimpcontext.h"
#include "core/gimpdata.h"
#include "core/gimpdatafactory.h"
#include "core/gimpdynamics.h"
#include "core/gimpgradient.h"
#include "core/gimppaintinfo.h"
#include "core/gimppattern.h"
#include "core/gimppalette.h"
#include "core/gimptoolinfo.h"
#include "core/gimptoolpreset.h"

#include "paint/gimppaintoptions.h"

#include "widgets/gimpdataeditor.h"
#include "widgets/gimpwidgets-utils.h"
#include "widgets/gimptoolpreseteditor.h"
#include "widgets/gimpuimanager.h"

#include "tool-preset-editor-commands.h"
#include "data-editor-commands.h"

#include "dialogs/asset-save-as-dialog.h"

#include "tools/gimpmybrushtool.h"

#include "gimp-intl.h"

typedef struct {
  const gchar *path_default;
  const gchar *path_a;
  const gchar *path_b;
  const gchar *path_c;
} SavePaths;

typedef void (*DataSetterFunc) (GimpContext *, GimpData *);

typedef struct {
  GimpData      *(*get_resource_func)(GimpContext *);
  gboolean         should_save;
  GimpDataFactory *factory;
  const gchar     *resource_type;
  SavePaths        save_folders;
  DataSetterFunc   setter_func;
} ResourceInfo;

static const gchar *
get_brush_type (GimpBrush *brush)
{
  if (GIMP_IS_BRUSH_GENERATED (brush))
    return ".vbr";
  else if  (GIMP_IS_BRUSH_PIPE (brush))
    return ".gih";
  else
    return ".gbr";
}

static const gchar *
get_resource_extension (const gchar *resource_type,
                        GimpData    *data)
{
  if (g_strcmp0(resource_type, "brush") == 0)
    return get_brush_type (GIMP_BRUSH (data));
  else if (g_strcmp0(resource_type, "pattern") == 0)
    return ".pat";
  else if (g_strcmp0(resource_type, "dynamics") == 0)
    return ".gdyn";
  else if (g_strcmp0(resource_type, "gradient") == 0)
    return ".ggr";
  else if (g_strcmp0(resource_type, "tool-preset") == 0)
    return ".gtp";
  else if (g_strcmp0(resource_type, "palette") == 0)
    return ".gpl";
  else
    return "";
}

GimpToolPreset *
duplicate_and_save_tool_preset (Gimp           *gimp,
                                GimpContext    *context,
                                GimpToolPreset *preset,
                                const gchar    *name)
{
  GimpData       *new_preset_data = gimp_data_factory_data_new (gimp->tool_preset_factory, context, name);
  GimpToolInfo   *tool_info       = gimp_context_get_tool (context);
  GimpToolPreset *new_preset      = NULL;
  GdkPixbuf      *pixbuf          = NULL;

  if (! preset)
    {
      g_error ("No Preset in duplicate_and_save_tool_preset!\n");
      return NULL;
    }

  new_preset = GIMP_TOOL_PRESET (new_preset_data);

  /* Transfer the save states for resource duplication */
  new_preset->savename_includes_type = preset->savename_includes_type;

  new_preset->resource_save_brush    = preset->resource_save_brush;
  new_preset->resource_save_dynamics = preset->resource_save_dynamics;
  new_preset->resource_save_gradient = preset->resource_save_gradient;
  new_preset->resource_save_pattern  = preset->resource_save_pattern;
  new_preset->resource_save_palette  = preset->resource_save_palette;

  /* Retrieve the icon-pixbuf from the old preset */
  g_object_get (G_OBJECT (preset),
                "icon-pixbuf", &pixbuf,
                NULL);

  /* If the old preset had an icon-pixbuf, set it for the new preset too */
  if (pixbuf != NULL)
    {
      g_object_set (G_OBJECT (new_preset),
                    "icon-pixbuf", pixbuf,
                    NULL);

      g_object_unref (pixbuf);
      pixbuf = NULL;
    }

  gimp_config_sync (G_OBJECT (tool_info->tool_options),
                    G_OBJECT (new_preset->tool_options), 0);

  gimp_data_dirty (GIMP_DATA (new_preset));

  gimp_data_factory_data_save_single (gimp->tool_preset_factory,
                                      GIMP_DATA (new_preset), NULL, NULL);

  gimp_context_set_tool_preset (context, new_preset);
  gimp_blink_dockable (gimp, "gimp-tool-options", NULL, NULL, NULL);
  gimp_context_tool_preset_changed (context);
  gimp_data_clean (GIMP_DATA (new_preset));

  return new_preset;
}

void
tool_preset_editor_save_cmd_callback (GimpAction *action,
                                      GVariant   *value,
                                      gpointer    data)
{
  GimpEditor     *editor      = GIMP_EDITOR (data);
  Gimp           *gimp        = gimp_editor_get_ui_manager (editor)->gimp;
  GimpContext    *context     = gimp_get_user_context (gimp);
  GimpToolPreset *preset      = gimp_context_get_tool_preset (context);
  GimpToolInfo   *tool_info   = gimp_context_get_tool (context);
  GimpToolInfo   *preset_tool;

  if (! preset)
    {
      gimp_message (context->gimp,
                    G_OBJECT (editor), GIMP_MESSAGE_WARNING,
                    _("Can't save '%s' tool options to an "
                      "non existing tool preset."),
                    tool_info->label);
      return;
    }

  preset_tool = gimp_context_get_tool (GIMP_CONTEXT (preset->tool_options));

  if (tool_info != preset_tool)
    {
      gimp_message (context->gimp,
                    G_OBJECT (editor), GIMP_MESSAGE_WARNING,
                    _("Can't save '%s' tool options to an "
                      "existing '%s' tool preset."),
                    tool_info->label,
                    preset_tool->label);
      return;
    }

  if (tool_info && preset)
    {
      /* sync is a way to force the tool options into a save state */
      gimp_config_sync (G_OBJECT (tool_info->tool_options),
                        G_OBJECT (preset->tool_options), 0);

      gimp_data_factory_data_save_single (context->gimp->tool_preset_factory,
                                          GIMP_DATA (preset), NULL, NULL);

      gimp_blink_dockable (context->gimp, "gimp-tool-options", NULL, NULL, NULL);
      gimp_context_tool_preset_changed (context);
    }
}

/* Tool Preset Save As... */
void
tool_preset_editor_save_as_cmd_callback (GimpAction *action,
                                         GVariant   *value,
                                         gpointer    data)
{
  GimpEditor     *editor    = GIMP_EDITOR (data);
  Gimp           *gimp      = gimp_editor_get_ui_manager (editor)->gimp;
  GimpContext    *context   = gimp_get_user_context (gimp);
  GimpToolPreset *preset    = gimp_context_get_tool_preset (context);
  GimpToolInfo   *tool_info = gimp_context_get_tool (context);

  /* It's possible to start GIMP with no Tool Preset active...*/
  if (tool_info && preset)
    {
      GtkWidget   *dialog;

      dialog = resources_save_as_dialog (data, _("Save Tool Preset"), "tool-preset", TRUE);

      if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
        {
          gchar *new_basename = create_and_assign_new_basename (dialog, "tool-preset");

          if (is_valid_asset_name (new_basename, FALSE))
            {
              duplicate_and_save_tool_preset (gimp, context, preset, new_basename);
              gimp_context_tool_preset_changed (context);

              g_free (new_basename);
            }
        }

      gtk_widget_destroy (dialog);
    }
}

/* Linked Resources Save */
void
tool_preset_editor_save_resources_cmd_callback (GimpAction *action,
                                                GVariant   *value,
                                                gpointer    data)
{
  GimpEditor     *editor    = GIMP_EDITOR (data);
  Gimp           *gimp      = gimp_editor_get_ui_manager (editor)->gimp;
  GimpContext    *context   = gimp_get_user_context (gimp);
  GimpToolPreset *preset    = gimp_context_get_tool_preset (context);
  GimpToolInfo   *tool_info = gimp_context_get_tool (context);

  if (tool_info && preset)
    {
      GimpDynamics   *dynamics   = gimp_context_get_dynamics (context);
      GimpData       *dyn_data   = GIMP_DATA (dynamics);
      GimpBrush      *brush      = gimp_context_get_brush (context);
      GimpData       *brush_data = GIMP_DATA (brush);
      GimpGradient   *gradient   = gimp_context_get_gradient (context);
      GimpData       *grad_data  = GIMP_DATA (gradient);
      GimpPalette    *palette    = gimp_context_get_palette (context);
      GimpData       *pal_data   = GIMP_DATA (palette);

      gimp_config_sync (G_OBJECT (tool_info->tool_options),
                        G_OBJECT (preset->tool_options), 0);
      gimp_data_factory_data_save_single (context->gimp->tool_preset_factory,
                                          GIMP_DATA (preset), NULL, NULL);

      if (dynamics && preset->resource_save_dynamics)
        gimp_data_factory_data_save_single (gimp->dynamics_factory, dyn_data, NULL, NULL);

      if (brush && preset->resource_save_brush)
        gimp_data_factory_data_save_single (gimp->brush_factory, brush_data, NULL, NULL);

      if (gradient && preset->resource_save_gradient)
        gimp_data_factory_data_save_single (gimp->gradient_factory, grad_data, NULL, NULL);

      if (palette && preset->resource_save_palette)
        gimp_data_factory_data_save_single (gimp->palette_factory, pal_data, NULL, NULL);

      gimp_context_tool_preset_changed (context);
      gimp_blink_dockable (context->gimp, "gimp-tool-preset-editor", NULL, NULL, NULL);
    }
}

static void
show_error_dialog (GtkWidget   *parent,
                   const gchar *message)
{
  GtkWidget *error_dialog = gtk_message_dialog_new (GTK_WINDOW (parent),
                                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                                    GTK_MESSAGE_ERROR,
                                                    GTK_BUTTONS_CLOSE,
                                                    "%s", message);
  gtk_dialog_run (GTK_DIALOG (error_dialog));
  gtk_widget_destroy (error_dialog);
}

static GimpData *
duplicate_and_save_resource (Gimp            *gimp,
                             GimpDataFactory *factory,
                             GimpData        *data,
                             GtkWidget       *dialog,
                             const gchar     *filename,
                             const gchar     *type_name,
                             GimpContext     *context,
                             SavePaths        save_folders)
{
  GimpGuiConfig  *gui_config = GIMP_GUI_CONFIG (gimp->config);
  GimpToolPreset *preset     = context->tool_preset;
  GimpData       *new_data   = NULL;
  gchar          *new_filename;

  new_filename = build_resource_name (type_name, filename, preset->savename_includes_type);

  if (save_folders.path_a && gui_config->package_use_save_path_a)
    {
      new_data = artbox_data_factory_data_save_external (factory, data, new_filename,
                                              NULL, save_folders.path_a);
    }

  if (save_folders.path_b && gui_config->package_use_save_path_b)
    {
      new_data = artbox_data_factory_data_save_external (factory, data, new_filename,
                                              NULL, save_folders.path_b);
    }

  if (save_folders.path_c && gui_config->package_use_save_path_c)
    {
      new_data = artbox_data_factory_data_save_external (factory, data, new_filename,
                                              NULL, save_folders.path_c);
    }

  if (save_folders.path_default && gui_config->package_use_save_path_default)
    {
      new_data = artbox_data_factory_data_save_external (factory, data, new_filename,
                                              NULL, save_folders.path_default);
    }

  g_free (new_filename);
  return new_data;
}

static void
apply_new_resource (GimpContext   *context,
                    GimpData      *new_resource,
                    DataSetterFunc setter_func)
{
  /* Brush duplication and setting active, resets Paintbrush tool options... */
  if (GIMP_IS_BRUSH (new_resource))
    {
      GimpPaintOptions *paint_options = context->paint_info->paint_options;
      gdouble brush_size              = paint_options->brush_size;
      gdouble brush_angle             = paint_options->brush_angle;
      gdouble brush_aspect_ratio      = paint_options->brush_aspect_ratio;
      gdouble brush_spacing           = paint_options->brush_spacing;
      gdouble brush_hardness          = paint_options->brush_hardness;

      /* Set the new brush */
      gimp_context_set_brush (context, GIMP_BRUSH (new_resource));

      /* Restore saved brush properties */
      paint_options->brush_size = brush_size;
      paint_options->brush_angle = brush_angle;
      paint_options->brush_aspect_ratio = brush_aspect_ratio;
      paint_options->brush_spacing = brush_spacing;
      paint_options->brush_hardness = brush_hardness;
    }
  else
    {
      /* Use the provided setter function for non-brush resources */
      setter_func (context, new_resource);

      if (GIMP_IS_TOOL_PRESET (new_resource))
        gimp_context_tool_preset_changed (context);
    }
}

static void
save_resource_to_paths_and_set_active (Gimp            *gimp,
                                       GimpContext     *context,
                                       GtkWidget       *dialog,
                                       GimpData        *resource,
                                       GimpDataFactory *factory,
                                       const gchar     *type_name,
                                       SavePaths        save_folders,
                                       DataSetterFunc   setter_func,
                                       const gchar     *filename)
{
  if (resource)
    {
      GimpData *new_resource;

      new_resource = duplicate_and_save_resource (gimp,
                                                  factory,
                                                  resource,
                                                  dialog,
                                                  filename,
                                                  type_name,
                                                  context,
                                                  save_folders);

      /* Pass the setter function to apply_new_resource */
      apply_new_resource (context, new_resource, setter_func);
    }
}

static guint
initialize_resource_info (Gimp           *gimp,
                          GimpContext    *context,
                          GimpToolPreset *preset,
                          GimpGuiConfig  *gui_config,
                          ResourceInfo  **resources_out)
{
  /* Allocate resources array dynamically */
  ResourceInfo *resources = g_new0 (ResourceInfo, 6);

  resources[0] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_brush,
    preset->resource_save_brush,
    gimp->brush_factory,
    "brush",
    { g_build_filename (gui_config->package_save_path_default, "brushes", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "brushes", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "brushes", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "brushes", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_brush
  };

  resources[1] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_dynamics,
    preset->resource_save_dynamics,
    gimp->dynamics_factory,
    "dynamics",
    { g_build_filename (gui_config->package_save_path_default, "dynamics", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "dynamics", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "dynamics", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "dynamics", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_dynamics
  };

  resources[2] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_gradient,
    preset->resource_save_gradient,
    gimp->gradient_factory,
    "gradient",
    { g_build_filename (gui_config->package_save_path_default, "gradients", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "gradients", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "gradients", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "gradients", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_gradient
  };

  resources[3] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_pattern,
    preset->resource_save_pattern,
    gimp->pattern_factory,
    "pattern",
    { g_build_filename (gui_config->package_save_path_default, "patterns", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "patterns", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "patterns", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "patterns", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_pattern
  };

  resources[4] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_palette,
    preset->resource_save_palette,
    gimp->palette_factory,
    "palette",
    { g_build_filename (gui_config->package_save_path_default, "palettes", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "palettes", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "palettes", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "palettes", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_palette
  };

  resources[5] = (ResourceInfo) {
    (GimpData *(*)(GimpContext *)) gimp_context_get_tool_preset,
    TRUE,
    gimp->tool_preset_factory,
    "tool-preset",
    { g_build_filename (gui_config->package_save_path_default, "tool-presets", gui_config->package_tag_path_default, NULL),
      g_build_filename (gui_config->package_save_path_a, "tool-presets", gui_config->package_tag_path_a, NULL),
      g_build_filename (gui_config->package_save_path_b, "tool-presets", gui_config->package_tag_path_b, NULL),
      g_build_filename (gui_config->package_save_path_c, "tool-presets", gui_config->package_tag_path_c, NULL) },
    (DataSetterFunc) gimp_context_set_tool_preset
  };

  *resources_out = resources;
  return 6; /* Number of resources */
}

static gchar *
build_filename_for_resource (const gchar *basename,
                             const gchar *resource_type,
                             GimpData *(*get_resource_func)(GimpContext *),
                             GimpContext *context)
{
  const gchar *extension = get_resource_extension (resource_type, get_resource_func (context));

  if (extension && *extension != '\0')
    return g_strdup_printf ("%s%s", basename, extension);
  else
     return g_strdup (basename);
}

static void
g_free_save_folders_paths (SavePaths *save_folders)
{
  g_free ((gchar *)save_folders->path_default);
  g_free ((gchar *)save_folders->path_a);
  g_free ((gchar *)save_folders->path_b);
  g_free ((gchar *)save_folders->path_c);

  save_folders->path_default = NULL;
  save_folders->path_a = NULL;
  save_folders->path_b = NULL;
  save_folders->path_c = NULL;
}

static void
save_dialog_resources (Gimp           *gimp,
                       GimpContext    *context,
                       GimpToolPreset *preset,
                       GtkWidget      *dialog)
{
  GimpGuiConfig *gui_config = GIMP_GUI_CONFIG (gimp->config);
  GtkWidget     *name_entry = g_object_get_data (G_OBJECT (dialog), "name-entry");
  const gchar   *basename   = gtk_entry_get_text (GTK_ENTRY (name_entry));
  ResourceInfo  *resources  = NULL;
  gchar         *filename   = NULL;
  guint          num_resources;

  num_resources = initialize_resource_info (gimp, context, preset, gui_config, &resources);
  for (guint i = 0; i < num_resources; i++)
    {
      if (!resources[i].should_save)
        continue;

      filename = build_filename_for_resource (basename,
                                              resources[i].resource_type,
                                              resources[i].get_resource_func,
                                              context);

      /* Save the resource to its paths */
      save_resource_to_paths_and_set_active (
        gimp,
        context,
        dialog,
        resources[i].get_resource_func (context), /* Resource instance */
        resources[i].factory,                     /* Factory */
        resources[i].resource_type,               /* Resource type */
        resources[i].save_folders,                /* Save paths */
        resources[i].setter_func,                 /* Setter function */
        filename);                                /* Filename with extension */

      g_free (filename);
      g_free_save_folders_paths (&resources[i].save_folders);
    }

  g_free(resources);
}

static void
debug_print_path_match (const gchar *load_path,
                        gint         save_path_index,
                        gboolean     debug_enabled)
{
  if (!debug_enabled)
    {
      return;
    }

  g_print ("Active load path '%s' matches active save path '%s'.\n",
           load_path,
           (save_path_index == 0) ? "default" :
           (save_path_index == 1) ? "A" :
           (save_path_index == 2) ? "B" : "C");
}

static gboolean
save_paths_contain_load_path (const gchar  *load_path,
                              gchar       **save_paths,
                              gboolean    *use_save_paths,
                              gint         n)
{
  for (gint j = 0; j < n; j++)
    {
      if (use_save_paths[j] && g_strcmp0 (load_path, save_paths[j]) == 0)
        {
          gboolean debug_path_match = FALSE;

          debug_print_path_match (load_path, j, debug_path_match);

          return TRUE;
        }
    }

  return FALSE;
}

static gboolean
check_active_load_save_path_match (GimpGuiConfig *gui_config)
{
  gchar *load_paths[] =
    {
      gui_config->package_load_path_user,
      gui_config->package_load_path_install,
      gui_config->package_load_path_a,
      gui_config->package_load_path_b,
      gui_config->package_load_path_c
    };

  gboolean use_load_paths[] =
    {
      gui_config->package_use_load_path_user,
      gui_config->package_use_load_path_install,
      gui_config->package_use_load_path_a,
      gui_config->package_use_load_path_b,
      gui_config->package_use_load_path_c
    };

  gchar *save_paths[] =
    {
      gui_config->package_save_path_default,
      gui_config->package_save_path_a,
      gui_config->package_save_path_b,
      gui_config->package_save_path_c
    };

  gboolean use_save_paths[] =
    {
      gui_config->package_use_save_path_default,
      gui_config->package_use_save_path_a,
      gui_config->package_use_save_path_b,
      gui_config->package_use_save_path_c
    };

  /* Check each load path */
  for (gint i = 0; i < 5; i++)
    {
      /* Skip if the load path is not in use or not set */
      if (!use_load_paths[i] || !load_paths[i])
        {
          continue;
        }

      /* See if this load path appears in the active save paths */
      if (save_paths_contain_load_path (load_paths[i],
                                        save_paths,
                                        use_save_paths,
                                        4))
        {
          return TRUE;
        }
    }

  return FALSE;
}

/* Linked Resources Save as...*/
void
tool_preset_editor_save_resources_as_cmd_callback (GimpAction *action,
                                                   GVariant   *value,
                                                   gpointer    data)
{
  GimpEditor     *editor    = GIMP_EDITOR (data);
  Gimp           *gimp      = gimp_editor_get_ui_manager (editor)->gimp;
  GimpContext    *context   = gimp_get_user_context (gimp);
  GimpToolInfo   *tool_info = gimp_context_get_tool (context);
  GimpToolPreset *preset    = gimp_context_get_tool_preset (context);
  GtkWidget      *dialog;
  GtkWidget      *name_entry;

  if (! tool_info || ! preset)
    return;

  dialog = resources_save_as_dialog (data, _("Save Tool Preset and Resources As"),
                                     "preset", FALSE);

  if (gtk_dialog_run (GTK_DIALOG (dialog)) != GTK_RESPONSE_OK)
    {
      gtk_widget_destroy (dialog);
      return;
    }

  name_entry = g_object_get_data (G_OBJECT (dialog), "name-entry");
  if (! is_valid_asset_name (gtk_entry_get_text (GTK_ENTRY (name_entry)), FALSE))
    {
      show_error_dialog (dialog, "Invalid name provided.");
      gtk_widget_destroy (dialog);
      return;
    }

  /* Do the saving of the GUI specified resources to the provided locations */
  save_dialog_resources (gimp, context, preset, dialog);

  /* Reload all the resources if the saved resource is on the loading search path */
  if (check_active_load_save_path_match (GIMP_GUI_CONFIG (gimp->config)))
    {
      gimp_data_factory_data_refresh (gimp->tool_preset_factory, context);
      gimp_data_factory_data_refresh (gimp->brush_factory, context);
      gimp_data_factory_data_refresh (gimp->dynamics_factory, context);
      gimp_data_factory_data_refresh (gimp->gradient_factory, context);
      gimp_data_factory_data_refresh (gimp->pattern_factory, context);
      gimp_data_factory_data_refresh (gimp->palette_factory, context);
    }
  else
    {
      /* Select the original preset */
      gimp_context_set_tool_preset (context, preset);
      gimp_context_tool_preset_changed (context);
    }

  gimp_blink_dockable (gimp, "gimp-tool-preset-editor", NULL, NULL, NULL);
  gtk_widget_destroy (dialog);
}

/* You forgot what you changed and think it's best to save over everything... */
void
tool_preset_editor_save_all_cmd_callback (GimpAction *action,
                                          GVariant   *value,
                                          gpointer    data)
{
  GimpEditor     *editor    = GIMP_EDITOR (data);
  Gimp           *gimp      = gimp_editor_get_ui_manager (editor)->gimp;
  GimpContext    *context   = gimp_get_user_context (gimp);
  GimpToolPreset *preset    = gimp_context_get_tool_preset (context);
  GimpToolInfo   *tool_info = gimp_context_get_tool (context);

  if (tool_info && preset)
    {
      GtkReliefStyle        default_relief;

      gtk_widget_style_get (GTK_WIDGET (editor),
                            "button-relief", &default_relief,
                            NULL);

      gimp_config_sync (G_OBJECT (tool_info->tool_options),
                        G_OBJECT (preset->tool_options), 0);

      gimp_data_factory_data_save (gimp->tool_preset_factory);
      gimp_blink_dockable (context->gimp, "gimp-tool-preset-editor", NULL, NULL, NULL);
      gimp_data_factory_data_save (gimp->dynamics_factory);
      gimp_data_factory_data_save (gimp->brush_factory);
      gimp_data_factory_data_save (gimp->gradient_factory);
      gimp_data_factory_data_save (gimp->palette_factory);
    }
}

void
tool_preset_editor_restore_cmd_callback (GimpAction *action,
                                         GVariant   *value,
                                         gpointer    data)
{
  GimpDataEditor *editor  = GIMP_DATA_EDITOR (data);
  GimpContext    *context = editor->context;
  GimpToolPreset *preset;

  preset = GIMP_TOOL_PRESET (gimp_data_editor_get_data (editor));

  if (preset)
    gimp_context_tool_preset_changed (gimp_get_user_context (context->gimp));
}

void
tool_preset_editor_reload_cmd_callback (GimpAction *action,
                                         GVariant   *value,
                                         gpointer    data)
{
  GimpDataEditor *editor  = GIMP_DATA_EDITOR (data);
  GimpContext    *context = editor->context;
  GimpToolPreset *preset;

  preset = GIMP_TOOL_PRESET (gimp_data_editor_get_data (editor));

  if (preset)
    gimp_context_tool_preset_changed (gimp_get_user_context (context->gimp));
}
