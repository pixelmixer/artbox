/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include <ctype.h>

#include <gegl.h>
#include <gtk/gtk.h>

#include "actions-types.h"

#include "core/gimpcontext.h"
#include "core/gimpdatafactory.h"
#include "core/gimppalette.h"

#include "widgets/gimppaletteeditor.h"
#include "widgets/gimppaletteview.h"
#include "widgets/gimpwidgets-utils.h"

#include "palette-editor-commands.h"
#include "data-editor-commands.h"

#include "dialogs/asset-save-as-dialog.h"

#include "gimp-intl.h"

/*  public functions  */

void
palette_editor_save_cmd_callback (GimpAction *action,
                                  GVariant   *value,
                                  gpointer    data)
{
  GimpDataEditor    *data_editor = GIMP_DATA_EDITOR (data);

  if (data_editor->data_editable)
    {
      gimp_data_factory_data_save_single (data_editor->data_factory,
                                          data_editor->data, NULL, NULL);
      gimp_blink_dockable (data_editor->context->gimp, "gimp-palette-editor", NULL, NULL, NULL);
    }
}

void
palette_editor_save_as_cmd_callback (GimpAction *action,
                                     GVariant   *value,
                                     gpointer    data)
{
  GimpDataEditor *data_editor = GIMP_DATA_EDITOR (data);
  GtkWidget      *dialog;

  dialog = resources_save_as_dialog (data, _("Save Palette"), "palette", TRUE);

  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
    {
      gchar *new_basename = create_and_assign_new_basename (dialog, "palette");

      if (is_valid_asset_name (new_basename, FALSE))
        {
          GimpData *new_palette;

          new_palette = gimp_data_factory_data_duplicate (data_editor->data_factory,
                                                          data_editor->data,
                                                          new_basename);

          gimp_context_set_palette (data_editor->context, GIMP_PALETTE (new_palette));
          g_free (new_basename);
        }
    }

  gtk_widget_destroy (dialog);
}

void
palette_editor_edit_color_cmd_callback (GimpAction *action,
                                        GVariant   *value,
                                        gpointer    data)
{
  GimpPaletteEditor *editor = GIMP_PALETTE_EDITOR (data);

  gimp_palette_editor_edit_color (editor);
}

void
palette_editor_new_color_cmd_callback (GimpAction *action,
                                       GVariant   *value,
                                       gpointer    data)
{
  GimpPaletteEditor *editor      = GIMP_PALETTE_EDITOR (data);
  GimpDataEditor    *data_editor = GIMP_DATA_EDITOR (data);
  gboolean           background  = (gboolean) g_variant_get_int32 (value);

  if (data_editor->data_editable)
    {
      GimpPalette      *palette = GIMP_PALETTE (data_editor->data);
      GimpPaletteEntry *entry;
      GeglColor        *color;

      if (background)
        color = gimp_context_get_background (data_editor->context);
      else
        color = gimp_context_get_foreground (data_editor->context);

      entry = gimp_palette_add_entry (palette, -1, NULL, color);
      gimp_palette_view_select_entry (GIMP_PALETTE_VIEW (editor->view), entry);
    }
}

void
palette_editor_delete_color_cmd_callback (GimpAction *action,
                                          GVariant   *value,
                                          gpointer    data)
{
  GimpPaletteEditor *editor      = GIMP_PALETTE_EDITOR (data);
  GimpDataEditor    *data_editor = GIMP_DATA_EDITOR (data);

  if (data_editor->data_editable && editor->color)
    {
      GimpPalette *palette = GIMP_PALETTE (data_editor->data);

      gimp_palette_delete_entry (palette, editor->color);
    }
}

void
palette_editor_zoom_cmd_callback (GimpAction *action,
                                  GVariant   *value,
                                  gpointer    data)
{
  GimpPaletteEditor *editor    = GIMP_PALETTE_EDITOR (data);
  GimpZoomType       zoom_type = (GimpZoomType) g_variant_get_int32 (value);

  gimp_palette_editor_zoom (editor, zoom_type);
}
