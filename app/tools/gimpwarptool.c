/* GIMP - The GNU Image Manipulation Program
 *
 * gimpwarptool.c
 * Copyright (C) 2011 Michael Muré <batolettre@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gegl-plugin.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "libgimpbase/gimpbase.h"
#include "libgimpmath/gimpmath.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "tools-types.h"

#include "config/gimpdisplayconfig.h"
#include "config/gimpguiconfig.h"

#include "gegl/gimp-gegl-apply-operation.h"
#include "gegl/gimp-gegl-loops.h"
#include "gegl/gimp-gegl-utils.h"

#include "core/gimp.h"
#include "core/gimpchannel.h"
#include "core/gimpcontainer.h"
#include "core/gimpdrawable-filters.h"
#include "core/gimpdrawablefilter.h"
#include "core/gimp-edit.h"
#include "core/gimpimage.h"
#include "core/gimpimage-undo.h"
#include "core/gimpgrouplayer.h"
#include "core/gimplayer.h"
#include "core/gimplayer-new.h"
#include "core/gimplayermask.h"
#include "core/gimppickable.h"
#include "core/gimppickable-auto-shrink.h"
#include "core/gimpprogress.h"
#include "core/gimpprojection.h"
#include "core/gimpsubprogress.h"
#include "core/gimptoolinfo.h"

#include "widgets/gimphelp-ids.h"
#include "widgets/gimpwidgets-utils.h"

#include "display/gimpdisplay.h"
#include "display/gimpdisplayshell.h"
#include "display/gimpdisplayshell-expose.h"
#include "display/gimpdisplayshell-render.h"

#include "gimpwarptool.h"
#include "gimpwarpoptions.h"
#include "gimptoolcontrol.h"
#include "tool_manager.h"
#include "gimptools-utils.h"

#include "gimp-intl.h"

#define STROKE_TIMER_MAX_FPS 20
#define PREVIEW_SAMPLER      GEGL_SAMPLER_NEAREST
#define GUI_SCALE 100
#define PRINT_GROUP_TREE FALSE
#define PRINT_DEBUG_TABLES FALSE
#define PRINT_DISPLAY_CHANGE FALSE
#define PRINT_WARP_PROGRESS FALSE
#define PRINT_WARP_COUNTER FALSE
#define USE_FREEZE_AND_THAW FALSE

typedef struct
{
  GList         *items;        /* List of items to warp              */
  GeglNode      *offset_node;  /* Shift the GEGL warp by an offset   */
  gint           warp_counter; /* Counter for the warp progress      */
  gint           warp_total;   /* Total items for the warp progress  */
  GeglRectangle  effect_bbox;  /* The warp effect, bounding box      */
  GeglRectangle  layer_bbox;   /* The warp group layer, bounding box */
} WarpGroup;

typedef struct
{
  GimpItem     *item;
  GeglRectangle left_margin;
  GeglRectangle right_margin;
  GeglRectangle item_bbox;
  GeglRectangle layer_expanded_bbox;
} ChildResizeData;

typedef struct
{
  GimpItem *parent;
  GList    *children;
} ParentGroup;

typedef enum
{
  CONTENT_WITH_OFFSET,
  CONTENT_NO_OFFSET
} GetContentMode;

static void            gimp_warp_tool_constructed               (GObject               *object);

static void            gimp_warp_tool_control                   (GimpTool              *tool,
                                                                 GimpToolAction         action,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_button_press              (GimpTool              *tool,
                                                                 const GimpCoords      *coords,
                                                                 guint32                time,
                                                                 GdkModifierType        state,
                                                                 GimpButtonPressType    press_type,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_button_release            (GimpTool              *tool,
                                                                 const GimpCoords      *coords,
                                                                 guint32                time,
                                                                 GdkModifierType        state,
                                                                 GimpButtonReleaseType  release_type,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_during_motion             (GimpTool              *tool,
                                                                 const GimpCoords      *coords,
                                                                 guint32                time,
                                                                 GdkModifierType        state,
                                                                 GimpDisplay           *display);
static gboolean        gimp_warp_tool_key_press                 (GimpTool              *tool,
                                                                 GdkEventKey           *kevent,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_oper_update               (GimpTool              *tool,
                                                                 const GimpCoords      *coords,
                                                                 GdkModifierType        state,
                                                                 gboolean               proximity,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_cursor_update             (GimpTool              *tool,
                                                                 const GimpCoords      *coords,
                                                                 GdkModifierType        state,
                                                                 GimpDisplay           *display);
const gchar          * gimp_warp_tool_can_undo                  (GimpTool              *tool,
                                                                 GimpDisplay           *display);
const gchar          * gimp_warp_tool_can_redo                  (GimpTool              *tool,
                                                                 GimpDisplay           *display);
static gboolean        gimp_warp_tool_undo                      (GimpTool              *tool,
                                                                 GimpDisplay           *display);
static gboolean        gimp_warp_tool_redo                      (GimpTool              *tool,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_options_notify            (GimpTool              *tool,
                                                                 GimpToolOptions       *options,
                                                                 const GParamSpec      *pspec);

static void            gimp_warp_tool_draw                      (GimpDrawTool          *draw_tool);

static void            gimp_warp_tool_cursor_notify             (GimpDisplayConfig     *config,
                                                                 GParamSpec            *pspec,
                                                                 GimpWarpTool          *wt);

static gboolean        gimp_warp_tool_can_stroke                (GimpWarpTool          *wt,
                                                                 GimpDisplay           *display,
                                                                 gboolean               show_message);

static gboolean        gimp_warp_tool_start                     (GimpWarpTool          *wt,
                                                                 GimpDisplay           *display);
static void            gimp_warp_tool_halt                      (GimpWarpTool          *wt);
static void            gimp_warp_tool_commit                    (GimpWarpTool          *wt);

static void            gimp_warp_tool_start_stroke_timer        (GimpWarpTool          *wt);
static void            gimp_warp_tool_stop_stroke_timer         (GimpWarpTool          *wt);
static gboolean        gimp_warp_tool_stroke_timer              (GimpWarpTool          *wt);

static void            gimp_warp_tool_create_graph              (GimpWarpTool          *wt);
static void            gimp_warp_tool_create_filter             (GimpWarpTool          *wt,
                                                                 GimpDrawable          *drawable);
static void            gimp_warp_tool_set_sampler               (GimpWarpTool          *wt,
                                                                 gboolean               commit);
static GeglRectangle
                       gimp_warp_tool_get_stroke_bounds         (GeglNode              *node);
static GeglRectangle   gimp_warp_tool_get_node_bounds           (GeglNode              *node);
static void            gimp_warp_tool_clear_node_bounds         (GeglNode              *node);
static GeglRectangle   gimp_warp_tool_get_invalidated_by_change (GimpWarpTool          *wt,
                                                                 const GeglRectangle   *area);
static void            gimp_warp_tool_update_bounds             (GimpWarpTool          *wt);
static void            gimp_warp_tool_update_area               (GimpWarpTool          *wt,
                                                                 const GeglRectangle   *area,
                                                                 gboolean               synchronous);
static void            gimp_warp_tool_update_stroke             (GimpWarpTool          *wt,
                                                                 GeglNode              *node);
static void            gimp_warp_tool_stroke_append             (GimpWarpTool          *wt,
                                                                 gchar                  type,
                                                                 gdouble                x,
                                                                 gdouble                y);
static void            gimp_warp_tool_filter_flush              (GimpDrawableFilter    *filter,
                                                                 GimpTool              *tool);
static void            gimp_warp_tool_add_op                    (GimpWarpTool          *wt,
                                                                 GeglNode              *op);
static void            gimp_warp_tool_remove_op                 (GimpWarpTool          *wt,
                                                                 GeglNode              *op);
static void            gimp_warp_tool_free_op                   (GeglNode              *op);

static void            gimp_warp_tool_animate                   (GimpWarpTool          *wt);

static void            gimp_warp_tool_group_commit              (GimpWarpTool          *wt);
static GList         * expand_layer_group                       (GimpWarpTool          *wt,
                                                                 GimpItem              *group);
static gboolean        warp_is_active                           (GimpWarpTool          *wt);
static void            should_begin_group_warp                  (GimpWarpTool          *wt,
                                                                 GimpDrawable          *drawable,
                                                                 GimpImage             *image);
static void            initialize_warp_tool                     (GimpWarpTool          *wt,
                                                                 GimpDisplay           *display,
                                                                 GimpDrawable          *drawable);
static void            create_warp_data_buffer                  (GimpWarpTool          *wt,
                                                                 GimpDrawable          *drawable);
static void            warp_tool_display_changed                (GimpContext           *context,
                                                                 GimpDisplay           *display,
                                                                 GimpWarpTool          *wt);

/* Type Definition */
G_DEFINE_TYPE (GimpWarpTool, gimp_warp_tool, GIMP_TYPE_DRAW_TOOL)

#define parent_class gimp_warp_tool_parent_class

void
gimp_warp_tool_register (GimpToolRegisterCallback  callback,
                         gpointer                  data)
{
  (* callback) (GIMP_TYPE_WARP_TOOL,
                GIMP_TYPE_WARP_OPTIONS,
                gimp_warp_options_gui,
                0,
                "gimp-warp-tool",
                _("Warp Transform"),
                _("Warp Transform: Deform with different tools"),
                N_("_Warp Transform"), "W",
                NULL, GIMP_HELP_TOOL_WARP,
                GIMP_ICON_TOOL_WARP,
                data);
}

static void
gimp_warp_tool_class_init (GimpWarpToolClass *klass)
{
  GObjectClass      *object_class    = G_OBJECT_CLASS (klass);
  GimpToolClass     *tool_class      = GIMP_TOOL_CLASS (klass);
  GimpDrawToolClass *draw_tool_class = GIMP_DRAW_TOOL_CLASS (klass);

  object_class->constructed  = gimp_warp_tool_constructed;

  tool_class->control        = gimp_warp_tool_control;
  tool_class->button_press   = gimp_warp_tool_button_press;
  tool_class->button_release = gimp_warp_tool_button_release;
  tool_class->motion         = gimp_warp_tool_during_motion;
  tool_class->key_press      = gimp_warp_tool_key_press;
  tool_class->oper_update    = gimp_warp_tool_oper_update;
  tool_class->cursor_update  = gimp_warp_tool_cursor_update;
  tool_class->can_undo       = gimp_warp_tool_can_undo;
  tool_class->can_redo       = gimp_warp_tool_can_redo;
  tool_class->undo           = gimp_warp_tool_undo;
  tool_class->redo           = gimp_warp_tool_redo;
  tool_class->options_notify = gimp_warp_tool_options_notify;

  draw_tool_class->draw      = gimp_warp_tool_draw;
}

static void
gimp_warp_tool_init (GimpWarpTool *self)
{
  GimpTool *tool = GIMP_TOOL (self);

  gimp_tool_control_set_motion_mode     (tool->control, GIMP_MOTION_MODE_EXACT);
  gimp_tool_control_set_scroll_lock     (tool->control, TRUE);
  gimp_tool_control_set_preserve        (tool->control, FALSE);
  gimp_tool_control_set_dirty_mask      (tool->control,
                                         GIMP_DIRTY_IMAGE           |
                                         GIMP_DIRTY_DRAWABLE        |
                                         GIMP_DIRTY_SELECTION       |
                                         GIMP_DIRTY_ACTIVE_DRAWABLE);
  gimp_tool_control_set_dirty_action    (tool->control,
                                         GIMP_TOOL_ACTION_COMMIT);
  gimp_tool_control_set_wants_click     (tool->control, TRUE);
  gimp_tool_control_set_precision       (tool->control,
                                         GIMP_CURSOR_PRECISION_SUBPIXEL);
  gimp_tool_control_set_tool_cursor     (tool->control,
                                         GIMP_TOOL_CURSOR_WARP);

  gimp_tool_control_set_action_pixel_size (tool->control,
                                           "tools-warp-effect-pixel-size-set");
  gimp_tool_control_set_action_size       (tool->control,
                                           "tools-warp-effect-size-set");
  gimp_tool_control_set_action_hardness   (tool->control,
                                           "tools-warp-effect-hardness-set");

  self->show_cursor           = TRUE;
  self->draw_brush            = TRUE;
  self->snap_brush            = FALSE;
  self->is_a_layer_group_warp = FALSE;
  self->tmp_expand_layers     = NULL;
  self->layer_group           = NULL;
  self->container             = NULL;
}

static void
gimp_warp_tool_constructed (GObject *object)
{
  GimpWarpTool      *wt   = GIMP_WARP_TOOL (object);
  GimpTool          *tool = GIMP_TOOL (object);
  GimpDisplayConfig *display_config;
  GimpContext       *user_context;
  GimpDisplay       *active_display;

  G_OBJECT_CLASS (parent_class)->constructed (object);

  display_config = GIMP_DISPLAY_CONFIG (tool->tool_info->gimp->config);

  user_context   = GIMP_CONTEXT (tool->tool_info->gimp->user_context);
  active_display = gimp_context_get_display (user_context);

  wt->show_cursor = display_config->show_paint_tool_cursor;
  wt->draw_brush  = display_config->show_brush_outline;
  wt->snap_brush  = display_config->snap_brush_outline;

  g_signal_connect_object (display_config, "notify::show-paint-tool-cursor",
                           G_CALLBACK (gimp_warp_tool_cursor_notify),
                           wt, 0);
  g_signal_connect_object (display_config, "notify::show-brush-outline",
                           G_CALLBACK (gimp_warp_tool_cursor_notify),
                           wt, 0);
  g_signal_connect_object (display_config, "notify::snap-brush-outline",
                           G_CALLBACK (gimp_warp_tool_cursor_notify),
                           wt, 0);
  g_signal_connect_object (user_context, "display-changed",
                           G_CALLBACK (warp_tool_display_changed),
                           wt, 0);

  warp_tool_display_changed (user_context, active_display, wt);
}

static void
warp_tool_display_changed (GimpContext  *context,
                           GimpDisplay  *display,
                           GimpWarpTool *wt)
{
  g_return_if_fail (GIMP_IS_TOOL (GIMP_TOOL (wt)));

  if (gimp_warp_tool_can_undo (GIMP_TOOL (wt), display))
    {
      gimp_tool_control (GIMP_TOOL (wt), GIMP_TOOL_ACTION_COMMIT, display);
    }
}

static void
gimp_warp_tool_control (GimpTool       *tool,
                        GimpToolAction  action,
                        GimpDisplay    *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);

  switch (action)
    {
    case GIMP_TOOL_ACTION_PAUSE:
      break;

    case GIMP_TOOL_ACTION_RESUME:
      break;

    case GIMP_TOOL_ACTION_HALT:
      gimp_warp_tool_halt (wt);
      break;

    case GIMP_TOOL_ACTION_COMMIT:
      gimp_warp_tool_commit (wt);
      break;

    default:
      break;
    }

  GIMP_TOOL_CLASS (parent_class)->control (tool, action, display);
}

static gboolean
warp_is_active (GimpWarpTool *wt)
{
  return (wt->filter != NULL);
}

static gboolean
gimp_warp_tool_validate_and_start (GimpTool     *tool,
                                   GimpWarpTool *wt,
                                   GimpDisplay  *display)
{
  /* Ensure that the tool is operating on exactly one drawable */
  if (g_list_length (tool->drawables) != 1)
    {
      return FALSE;
    }

  /* Halt the tool if the display has changed, or return FALSE if warp cannot stroke */
  if (tool->display && display != tool->display)
    {
      gimp_tool_control (tool, GIMP_TOOL_ACTION_HALT, tool->display);
    }
  else if (! gimp_warp_tool_can_stroke (wt, display, TRUE))
    {
      return FALSE;
    }

  /* Start a new warp if none exists */
  if (! tool->display && ! gimp_warp_tool_start (wt, display))
    {
      return FALSE;
    }

  return TRUE;
}

static void
gimp_warp_tool_initialize_stroke (GimpWarpTool     *wt,
                                  const GimpCoords *coords,
                                  GimpWarpOptions  *options)
{
  GeglNode *new_op;

  wt->current_stroke = gegl_path_new ();
  wt->last_pos.x = coords->x;
  wt->last_pos.y = coords->y;
  wt->total_dist = 0.0;

  new_op = gegl_node_new_child (NULL,
                                "operation", "gegl:warp",
                                "behavior",  options->behavior,
                                "size",      options->effect_size,
                                "hardness",  options->effect_hardness / 100.0,
                                "strength",  options->effect_strength,
                                "spacing",   10.0, /* ignored */
                                "stroke",    wt->current_stroke,
                                NULL);

  gimp_warp_tool_add_op (wt, new_op);
  g_object_unref (new_op);
}

static void
gimp_warp_tool_add_stroke (GimpTool *tool, GimpWarpTool *wt)
{
  gint off_x, off_y;

  gimp_item_get_offset (GIMP_ITEM (tool->drawables->data), &off_x, &off_y);

  gimp_warp_tool_stroke_append (wt,
                                'M', wt->last_pos.x - off_x,
                                     wt->last_pos.y - off_y);
}

static void
gimp_warp_tool_handle_undo (GimpTool     *tool,
                            GimpWarpTool *wt,
                            GimpDisplay  *display)
{
  gimp_warp_tool_undo (tool, display);

  /* The just undone stroke has no business on the redo stack */
  gimp_warp_tool_free_op (wt->redo_stack->data);
  wt->redo_stack = g_list_remove_link (wt->redo_stack, wt->redo_stack);
}

static void
gimp_warp_tool_clear_redo_stack (GimpWarpTool *wt)
{
  if (wt->redo_stack)
    {
      /* The redo stack becomes invalid by actually doing a stroke */
      g_list_free_full (wt->redo_stack,
                        (GDestroyNotify) gimp_warp_tool_free_op);
      wt->redo_stack = NULL;
    }
}

static void
gimp_warp_tool_button_press (GimpTool            *tool,
                             const GimpCoords    *coords,
                             guint32              time,
                             GdkModifierType      state,
                             GimpButtonPressType  press_type,
                             GimpDisplay         *display)
{
  GimpWarpTool    *wt      = GIMP_WARP_TOOL (tool);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  if (! gimp_warp_tool_validate_and_start (tool, wt, display))
    return;

  gimp_warp_tool_initialize_stroke (wt, coords, options);

  gimp_warp_tool_add_stroke (tool, wt);

  gimp_warp_tool_start_stroke_timer (wt);

  gimp_tool_control_activate (tool->control);
}

static void
gimp_warp_tool_pause_and_update_stroke (GimpWarpTool *wt,
                                        GimpTool     *tool)
{
  gimp_draw_tool_pause (GIMP_DRAW_TOOL (wt));
  gimp_tool_control_halt (tool->control);
  gimp_warp_tool_stop_stroke_timer (wt);
}

static void
gimp_warp_tool_handle_release (GimpTool              *tool,
                               GimpWarpTool          *wt,
                               GimpButtonReleaseType release_type,
                               GimpDisplay           *display)
{
  if (release_type == GIMP_BUTTON_RELEASE_CANCEL)
    {
      gimp_warp_tool_handle_undo (tool, wt, display);
    }
  else
    {
      gimp_warp_tool_clear_redo_stack (wt);

      /* Push status to inform about commit transform */
      gimp_tool_push_status (tool, tool->display,
                             _("Press ENTER to commit the transform"));
    }
}

static void
gimp_warp_tool_resume_operations (GimpTool     *tool,
                                  GimpWarpTool *wt)
{
  /* Resume drawing and update undo actions / menu items */
  gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
  gimp_image_flush (gimp_display_get_image (GIMP_TOOL (wt)->display));
}

void
gimp_warp_tool_button_release (GimpTool              *tool,
                               const GimpCoords      *coords,
                               guint32                time,
                               GdkModifierType        state,
                               GimpButtonReleaseType  release_type,
                               GimpDisplay           *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);

  gimp_warp_tool_pause_and_update_stroke (wt, tool);

#ifdef WARP_DEBUG
  g_print ("%s\n", gegl_path_to_string (wt->current_stroke));
#endif

  /* Clear the current stroke object */
  g_clear_object (&wt->current_stroke);

  gimp_warp_tool_handle_release (tool, wt, release_type, display);

  gimp_warp_tool_resume_operations (tool, wt);
}

static gdouble
cursor_delta_and_distance (GimpWarpTool     *wt,
                           GimpVector2      *delta,
                           GimpVector2      *last_pos,
                           const GimpCoords *coords)
{
  wt->cursor_pos.x = coords->x;
  wt->cursor_pos.y = coords->y;

  /* Calculate the difference between the cursor and the last position */
  gimp_vector2_sub (delta, &wt->cursor_pos, last_pos);

  /* Return the length of the delta vector (i.e., the distance moved) */
  return gimp_vector2_length (delta);
}

static void
set_warp_effect_location (GimpWarpTool *wt,
                          GimpVector2  *delta,
                          gdouble       diff,
                          gdouble       cursor_movement,
                          GimpVector2  *last_pos)
{
  /* Update position based on the step difference */
  gimp_vector2_mul (delta, diff / cursor_movement);
  gimp_vector2_add (last_pos, last_pos, delta);

  /* Recalculate the delta after position update */
  gimp_vector2_sub (delta, &wt->cursor_pos, last_pos);
}

static void
append_stroke_segment_and_update_preview (GimpTool     *tool,
                                          GimpWarpTool *wt,
                                          gboolean     *stroke_updated)
{
  gint off_x, off_y;

  if (! *stroke_updated)
    {
      *stroke_updated = TRUE;
      gimp_draw_tool_pause (GIMP_DRAW_TOOL (tool));
    }

  /* Get drawable offset */
  gimp_item_get_offset (GIMP_ITEM (tool->drawables->data), &off_x, &off_y);

  /* Append a line segment to the stroke and update the real-time preview */
  gimp_warp_tool_stroke_append (wt, 'L', wt->last_pos.x - off_x,
                                         wt->last_pos.y - off_y);
}

static gboolean
check_stroke_motion (gdouble total_dist, gdouble remaining_dist, gdouble step)
{
  return (total_dist + remaining_dist >= step);
}

static void
initialize_motion (GimpWarpTool     *wt,
                   const GimpCoords *coords,
                   GimpVector2      *delta,
                   GimpVector2      *last_pos,
                   gdouble          *step,
                   gdouble          *cursor_movement,
                   gdouble          *remaining_dist)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  *step = options->effect_size * (options->stroke_spacing / GUI_SCALE);
  *cursor_movement = cursor_delta_and_distance (wt, delta, last_pos, coords);
  *remaining_dist = *cursor_movement;
}

static void
update_warp_stroke (GimpTool     *tool,
                    GimpWarpTool *wt,
                    GimpVector2  *delta,
                    GimpVector2  *last_pos,
                    gdouble      *remaining_dist,
                    gdouble       step,
                    gboolean     *stroke_updated)
{
  gboolean should_update_warp_stroke = check_stroke_motion (wt->total_dist, *remaining_dist, step);
  GimpWarpOptions           *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  while (should_update_warp_stroke)
    {
      gdouble diff = step - wt->total_dist;

      set_warp_effect_location (wt, delta, diff, *remaining_dist, last_pos);

      *remaining_dist -= diff;
      wt->last_pos = *last_pos;
      wt->total_dist = 0.0;

      /* Optional updating of the warp stroke during motion */
      if (options->stroke_during_motion)
        append_stroke_segment_and_update_preview (tool, wt, stroke_updated);

      should_update_warp_stroke = check_stroke_motion (wt->total_dist, *remaining_dist, step);
    }
}

static void
finalize_motion (GimpWarpTool *wt,
                 gdouble       remaining_dist,
                 gboolean      stroke_updated,
                 GimpTool     *tool)
{
  /* Accumulate leftover distance for future steps */
  wt->total_dist += remaining_dist;

  if (stroke_updated)
    {
      gimp_warp_tool_start_stroke_timer (wt);
      gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
    }
}

/**
 * Pauses or resumes the draw tool to prevent interference with the global snap
 * behavior of the brush tool. If snapping is disabled (`snap_brush` is FALSE),
 * this function pauses draw updates while processing cursor movements and resumes
 * them afterward. Stepped updates are similar to brush snapping.
 */
static void
warp_tool_controls_display_refresh (GimpWarpTool *wt,
                                    gboolean   pause)
{
  if (! wt->snap_brush)
    {
      if (pause)
        gimp_draw_tool_pause (GIMP_DRAW_TOOL (wt));
      else
        gimp_draw_tool_resume (GIMP_DRAW_TOOL (wt));
    }
}

/**
 * gimp_warp_tool_during_motion:
 * @tool: the active GimpTool instance (Warp Tool).
 * @coords: the current coordinates of the cursor.
 * @time: the timestamp of the motion event.
 * @state: the current state of the modifier keys (e.g., Shift, Ctrl).
 * @display: the active GimpDisplay instance.
 *
 * This function is called during motion events to update the warp path based on
 * the user's cursor movement. It calculates the distance moved by the cursor
 * and constructs stroke segments that define the warp effect. The warp path is
 * constructed incrementally as the cursor moves, appending new segments when
 * the movement exceeds a predefined threshold (determined by brush size and
 * spacing). If real-time stroke updates are enabled, the warp effect path is
 * dynamically updated during motion.
 *
 * When real-time updates are disabled, the stroke path is updated by a separate
 * stroke timer mechanism that appends the cursor position at timed intervals.
 */
static void
gimp_warp_tool_during_motion (GimpTool         *tool,
                              const GimpCoords *coords,
                              guint32           time,
                              GdkModifierType   state,
                              GimpDisplay      *display)
{
  GimpWarpTool    *wt             = GIMP_WARP_TOOL (tool);
  GimpVector2      last_pos       = wt->cursor_pos;
  GimpVector2      delta;
  gdouble          step, cursor_movement, remaining_dist;
  gboolean         stroke_updated = FALSE;

  warp_tool_controls_display_refresh (wt, TRUE);

  initialize_motion (wt, coords, &delta, &last_pos, &step, &cursor_movement, &remaining_dist);

  update_warp_stroke (tool, wt, &delta, &last_pos, &remaining_dist, step, &stroke_updated);

  finalize_motion (wt, remaining_dist, stroke_updated, tool);

  warp_tool_controls_display_refresh (wt, FALSE);
}

static gboolean
gimp_warp_tool_key_press (GimpTool    *tool,
                          GdkEventKey *kevent,
                          GimpDisplay *display)
{
  switch (kevent->keyval)
    {
    case GDK_KEY_BackSpace:
      return TRUE;

    case GDK_KEY_Return:
    case GDK_KEY_KP_Enter:
    case GDK_KEY_ISO_Enter:
      gimp_tool_control (tool, GIMP_TOOL_ACTION_COMMIT, display);
      return TRUE;

    case GDK_KEY_Escape:
      gimp_tool_control (tool, GIMP_TOOL_ACTION_HALT, display);
      return TRUE;

    default:
      break;
    }

  return FALSE;
}

/**
 * gimp_warp_tool_oper_update:
 *
 * Pauses or resumes the warp tool based on the cursor's proximity to the image
 * on the canvas. If the cursor is near the image and the tool's display matches
 * the current display, the cursor position is updated, and the tool resumes drawing.
 *
 * If the cursor moves away from the image, the tool is stopped to prevent updates.
 */
static void
gimp_warp_tool_oper_update (GimpTool         *tool,
                            const GimpCoords *coords,
                            GdkModifierType   state,
                            gboolean          proximity,
                            GimpDisplay      *display)
{
  GimpWarpTool *wt        = GIMP_WARP_TOOL (tool);
  GimpDrawTool *draw_tool = GIMP_DRAW_TOOL (tool);

  if (proximity)
    {
      gimp_draw_tool_pause (draw_tool);

      wt->cursor_pos.x = coords->x;
      wt->cursor_pos.y = coords->y;
      wt->last_pos = wt->cursor_pos;

      if (! gimp_draw_tool_is_active (draw_tool))
        gimp_draw_tool_start (draw_tool, display);

      gimp_draw_tool_resume (draw_tool);
    }
  else if (gimp_draw_tool_is_active (draw_tool))
    {
      gimp_draw_tool_stop (draw_tool);
    }
}

static void
gimp_warp_tool_cursor_update (GimpTool         *tool,
                              const GimpCoords *coords,
                              GdkModifierType   state,
                              GimpDisplay      *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);

  /* If the tool can't stroke or the cursor shouldn't be shown, set the cursor to NONE */
  if (! gimp_warp_tool_can_stroke (wt, display, FALSE) || ! wt->show_cursor)
    {
      gimp_tool_set_cursor (tool, display,
                            GIMP_CURSOR_NONE,
                            GIMP_TOOL_CURSOR_NONE,
                            GIMP_CURSOR_MODIFIER_NONE);
      return;
    }

  /* Call parent class's cursor update */
  GIMP_TOOL_CLASS (parent_class)->cursor_update (tool, coords, state, display);
}

const gchar *
gimp_warp_tool_can_undo (GimpTool    *tool,
                         GimpDisplay *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);
  GeglNode     *to_delete;
  const gchar  *type;

  if (! wt->render_node)
    return NULL;

  to_delete = gegl_node_get_producer (wt->render_node, "aux", NULL);
  type = gegl_node_get_operation (to_delete);

  if (strcmp (type, "gegl:warp"))
    return NULL;

  return _("Warp Tool Stroke");
}

const gchar *
gimp_warp_tool_can_redo (GimpTool    *tool,
                         GimpDisplay *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);

  if (! wt->render_node || ! wt->redo_stack)
    return NULL;

  return _("Warp Tool Stroke");
}

static void
gimp_check_warp_undo_status (GimpTool    *tool,
                             GimpDisplay *display)
{
  GimpWarpTool    *wt      = GIMP_WARP_TOOL (tool);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  /* If there are no more undo operations available, notify the user */
  if (! gimp_warp_tool_can_undo (tool, display))
    {
      gimp_tools_show_tool_options (display->gimp);
      gimp_widget_blink_cancel (options->behavior_combo);
      gimp_tool_replace_status (tool, display, "%s", _("That was the last Warp undo..."));
    }
}

/**
 * In this function, we retrieve the producer node that is connected to the render
 * node's "aux" input. The "aux" input or "pad" allows the render node to use
 * additional data (such as transformations) to modify its output.
 */
static gboolean
gimp_warp_tool_undo (GimpTool    *tool,
                     GimpDisplay *display)
{
  GimpWarpTool    *wt = GIMP_WARP_TOOL (tool);
  GeglNode        *to_delete;
  GeglNode        *prev_node;

  to_delete = gegl_node_get_producer (wt->render_node, "aux", NULL);

  wt->redo_stack = g_list_prepend (wt->redo_stack, to_delete);

  /* we connect render_node to the previous node, but keep the current node
   * in the graph, connected to the previous node as well, so that it doesn't
   * get invalidated and maintains its cache.  this way, redoing it doesn't
   * require reprocessing.
   */
  prev_node = gegl_node_get_producer (to_delete, "input", NULL);

  gegl_node_connect (prev_node,       "output",
                     wt->render_node, "aux");

  gimp_warp_tool_update_bounds (wt);
  gimp_warp_tool_update_stroke (wt, to_delete);

  /* Check the status of the warp undo to determine if it's the last one */
  gimp_check_warp_undo_status (tool, display);

  return TRUE;
}

static gboolean
gimp_warp_tool_redo (GimpTool    *tool,
                     GimpDisplay *display)
{
  GimpWarpTool *wt = GIMP_WARP_TOOL (tool);
  GeglNode     *to_add;

  to_add = wt->redo_stack->data;

  gegl_node_connect (to_add,          "output",
                     wt->render_node, "aux");

  wt->redo_stack = g_list_remove_link (wt->redo_stack, wt->redo_stack);

  gimp_warp_tool_update_bounds (wt);
  gimp_warp_tool_update_stroke (wt, to_add);

  return TRUE;
}

static void
gimp_warp_tool_options_notify (GimpTool         *tool,
                               GimpToolOptions  *options,
                               const GParamSpec *pspec)
{
  GimpWarpTool    *wt         = GIMP_WARP_TOOL (tool);
  GimpWarpOptions *wt_options = GIMP_WARP_OPTIONS (options);

  GIMP_TOOL_CLASS (parent_class)->options_notify (tool, options, pspec);

  if (! strcmp (pspec->name, "effect-size"))
    {
      gimp_draw_tool_pause (GIMP_DRAW_TOOL (tool));
      gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
    }
  else if (! strcmp (pspec->name, "interpolation"))
    {
      gimp_warp_tool_set_sampler (wt, /* commit = */ FALSE);
    }
  else if (! strcmp (pspec->name, "abyss-policy"))
    {
      if (wt->render_node)
        {
          gegl_node_set (wt->render_node,
                         "abyss-policy", wt_options->abyss_policy,
                         NULL);

          gimp_warp_tool_update_stroke (wt, NULL);
        }
    }
  else if (! strcmp (pspec->name, "high-quality-preview"))
    {
      gimp_warp_tool_set_sampler (wt, /* commit = */ FALSE);
    }
}

static void
gimp_warp_tool_draw (GimpDrawTool *draw_tool)
{
  GimpWarpTool    *wt      = GIMP_WARP_TOOL (draw_tool);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  gdouble          x, y;

  if (wt->snap_brush)
    {
      x = wt->last_pos.x;
      y = wt->last_pos.y;
    }
  else
    {
      x = wt->cursor_pos.x;
      y = wt->cursor_pos.y;
    }

  if (wt->draw_brush)
    {
      gimp_draw_tool_add_arc (draw_tool,
                              FALSE,
                              x - options->effect_size * 0.5,
                              y - options->effect_size * 0.5,
                              options->effect_size,
                              options->effect_size,
                              0.0, 2.0 * G_PI);
    }
  else if (! wt->show_cursor)
    {
      /*  don't leave the user without any indication and draw
       *  a fallback crosshair
       */
      gimp_draw_tool_add_handle (draw_tool,
                                 GIMP_HANDLE_CROSSHAIR,
                                 x, y,
                                 GIMP_TOOL_HANDLE_SIZE_CROSSHAIR,
                                 GIMP_TOOL_HANDLE_SIZE_CROSSHAIR,
                                 GIMP_HANDLE_ANCHOR_CENTER);
    }
}

static void
gimp_warp_tool_cursor_notify (GimpDisplayConfig *config,
                              GParamSpec        *pspec,
                              GimpWarpTool      *wt)
{
  gimp_draw_tool_pause (GIMP_DRAW_TOOL (wt));

  wt->show_cursor = config->show_paint_tool_cursor;
  wt->draw_brush  = config->show_brush_outline;
  wt->snap_brush  = config->snap_brush_outline;

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (wt));
}

static void
gimp_warp_tool_reset_group_layers (GimpWarpTool *wt)
{
  wt->layer_group = NULL;
  wt->is_a_layer_group_warp = FALSE;
  wt->tmp_expand_layers = NULL;
}

static gboolean
requires_mask_edit_switch (GimpDrawable *drawable,
                           GimpDrawable *warp_drawable)
{
  return GIMP_IS_CHANNEL (drawable) &&
         gimp_layer_get_mask (GIMP_LAYER (warp_drawable));
}

static gboolean
warp_is_on_same_display (GimpTool      *tool,
                         GimpDrawable  *drawable,
                         GimpDisplay   *display)
{
  return (tool->drawables && drawable) &&
         (tool->display && display == tool->display);
}

static gboolean
mid_warp_layer_switch_detected (GimpTool     *tool,
                                GimpDrawable *drawable)
{
  GimpDrawable *warp_drawable = tool->drawables ? tool->drawables->data : NULL;

  return warp_drawable &&
         (warp_drawable != drawable) &&
         gimp_item_is_group_layer (GIMP_ITEM (warp_drawable));
}

static gboolean
gimp_warp_tool_can_stroke (GimpWarpTool *wt,
                           GimpDisplay  *display,
                           gboolean      show_message)
{
  GimpTool        *tool          = GIMP_TOOL (wt);
  GimpWarpOptions *options       = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GimpGuiConfig   *config        = GIMP_GUI_CONFIG (display->gimp->config);
  GimpImage       *image         = gimp_display_get_image (display);
  GimpItem        *locked_item   = NULL;
  GimpDrawable    *drawable      = gimp_image_get_single_selected_drawable (image);

  if (! drawable)
    {
      if (show_message)
        {
          gimp_tool_message_literal(tool, display,
                                    _("Select one layer or group to warp."));
        }

      return FALSE;
    }

  if (gimp_item_is_content_locked (GIMP_ITEM (drawable), &locked_item))
    {
      if (show_message)
        {
          gimp_tool_message_literal (tool, display,
                                     _("The selected item's pixels are locked."));

          gimp_tools_blink_lock_box (display->gimp, locked_item);
        }

      return FALSE;
    }

  if (! gimp_item_is_visible (GIMP_ITEM (drawable)) &&
      ! config->edit_non_visible)
    {
      if (show_message)
        {
          gimp_tool_message_literal (tool, display,
                                     _("The selected item is not visible."));
        }

      return FALSE;
    }

  if (! options->stroke_during_motion &&
      ! options->stroke_periodically)
    {
      if (show_message)
        {
          gimp_tool_message_literal (tool, display,
                                     _("No stroke events selected."));

          gimp_tools_show_tool_options (display->gimp);
          gimp_widget_blink (options->stroke_frame);
        }

      return FALSE;
    }

    if (! warp_is_on_same_display (tool, drawable, display))
      {
        if (PRINT_DISPLAY_CHANGE)
          {
            g_print ("Active Display : %p\n", display);
            g_print ("Tool's Display : %p\n", tool->display);
            g_print ("Draw   Display : %p\n", GIMP_DRAW_TOOL(wt)->display);
          }
        return TRUE;
      }

    if (warp_is_on_same_display (tool, drawable, display) &&
        mid_warp_layer_switch_detected (tool, drawable))
      {
        GimpDrawable *warp_drawable = tool->drawables->data;

        gimp_tool_message_literal (tool, display,
                                  _("Press ENTER to commit the group warp, before selecting another layer!"));

        gimp_image_set_single_selected_layer (image, GIMP_LAYER (warp_drawable));

        if (requires_mask_edit_switch (drawable, warp_drawable))
          {
            gimp_layer_set_edit_mask (GIMP_LAYER (warp_drawable), FALSE);
          }

        /* Set focus back to the display to enable 'Enter' for commit */
        gimp_display_grab_focus (display);

        return FALSE;
      }

  return TRUE;
}

static gboolean
is_group_warp_in_progress (GimpWarpTool *wt)
{
  return wt->tmp_expand_layers != NULL;
}

static void
gimp_warp_validate_mode (GimpWarpTool *wt,
                         GimpDisplay  *display)
{
  GimpTool        *tool    = GIMP_TOOL (wt);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  /* If the Warp has no undo, and can't Erase or Smooth, switch to Move */
  if (! warp_is_active (wt) || ! gimp_tool_can_undo (tool, display))
    if (options->behavior == GIMP_WARP_BEHAVIOR_ERASE ||
        options->behavior == GIMP_WARP_BEHAVIOR_SMOOTH)
      {
        gimp_widget_blink (options->behavior_combo);
        gimp_int_combo_box_set_active (GIMP_INT_COMBO_BOX (options->behavior_combo),
                                       GIMP_WARP_BEHAVIOR_MOVE);
      }
}

static void
initialize_warp_tool (GimpWarpTool *wt,
                      GimpDisplay  *display,
                      GimpDrawable *drawable)
{
  GimpTool *tool  = GIMP_TOOL (wt);

  tool->display = display;
  g_list_free (tool->drawables);
  tool->drawables = g_list_append (NULL, drawable);
  gimp_warp_validate_mode (wt, display);
}

static void
create_warp_data_buffer (GimpWarpTool *wt,
                         GimpDrawable *drawable)
{
  const Babl    *format;
  GeglRectangle  bbox;

  /* Create the coords buffer, with the size of the selection */
  format = babl_format_n (babl_type ("float"), 2);

  gimp_item_mask_intersect (GIMP_ITEM (drawable), &bbox.x, &bbox.y,
                            &bbox.width, &bbox.height);

#ifdef WARP_DEBUG
  g_printerr ("Initialize coordinate buffer (%d,%d) at %d,%d\n",
              bbox.width, bbox.height, bbox.x, bbox.y);
#endif

  wt->coords_buffer = gegl_buffer_new (&bbox, format);
}

static void
should_begin_group_warp (GimpWarpTool *wt,
                         GimpDrawable *drawable,
                         GimpImage    *image)
{
  /* Check to see if a group warp needs initializing */
  if (! is_group_warp_in_progress (wt) && gimp_item_is_group_layer (GIMP_ITEM (drawable)))
    {
      wt->layer_group = GIMP_ITEM (drawable);
      wt->is_a_layer_group_warp = TRUE;
      wt->tmp_expand_layers = expand_layer_group (wt, GIMP_ITEM (drawable));
    }
}

static gboolean
gimp_warp_tool_start (GimpWarpTool *wt,
                      GimpDisplay  *display)
{
  GimpWarpOptions *options   = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GimpImage       *image     = gimp_display_get_image (display);
  GimpDrawable    *drawable  = gimp_image_get_single_selected_drawable (image);
  GimpContainer   *container = gimp_image_get_layers (image);

  if (! gimp_warp_tool_can_stroke (wt, display, TRUE))
    return FALSE;

  initialize_warp_tool (wt, display, drawable);
  should_begin_group_warp (wt, drawable, image);
  create_warp_data_buffer (wt, drawable);
  gimp_warp_tool_create_filter (wt, drawable);

  if (! gimp_draw_tool_is_active (GIMP_DRAW_TOOL (wt)))
    gimp_draw_tool_start (GIMP_DRAW_TOOL (wt), display);

  if (options->animate_button)
    {
      g_signal_connect_swapped (options->animate_button, "clicked",
                                G_CALLBACK (gimp_warp_tool_animate),
                                wt);

      gtk_widget_set_sensitive (options->animate_button, TRUE);
    }

  if (container && USE_FREEZE_AND_THAW)
    {
      gimp_container_freeze (container);
      wt->container = container;
    }

  return TRUE;
}

static void
remove_tmp_expand_layers (GimpWarpTool *wt)
{
  GimpImage *image = gimp_display_get_image (GIMP_TOOL (wt)->display);

  for (GList *iter = wt->tmp_expand_layers; iter != NULL; iter = iter->next)
    {
      GimpLayer *layer = GIMP_LAYER (iter->data);

      if (GIMP_IS_LAYER (layer))
        {
          gimp_image_remove_layer (image, layer, FALSE, NULL);
        }
    }

  g_list_free (wt->tmp_expand_layers);
  wt->tmp_expand_layers = NULL;
}

static void
gimp_warp_tool_halt (GimpWarpTool *wt)
{
  GimpTool        *tool    = GIMP_TOOL (wt);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  g_clear_object (&wt->coords_buffer);
  g_clear_object (&wt->graph);
  wt->render_node = NULL;

  if (warp_is_active (wt) && GIMP_IS_DRAWABLE_FILTER (wt->filter))
    {
      gimp_tool_message_literal (tool, tool->display, _("Warp cancelled"));
      gimp_drawable_filter_abort (wt->filter);
      g_clear_object (&wt->filter);

      if (wt->is_a_layer_group_warp)
        {
          remove_tmp_expand_layers (wt);
          gimp_warp_tool_reset_group_layers (wt);
        }

      gimp_image_flush (gimp_display_get_image (tool->display));
    }

  if (wt->redo_stack)
    {
      g_list_free (wt->redo_stack);
      wt->redo_stack = NULL;
    }

  if (GIMP_IS_CONTAINER (wt->container) && USE_FREEZE_AND_THAW)
    {
      gimp_container_thaw (wt->container);
    }

  tool->display   = NULL;
  g_list_free (tool->drawables);
  tool->drawables = NULL;

  if (gimp_draw_tool_is_active (GIMP_DRAW_TOOL (wt)))
    {
      gimp_draw_tool_stop (GIMP_DRAW_TOOL (wt));
    }

  if (options->animate_button)
    {
      gtk_widget_set_sensitive (options->animate_button, FALSE);

      g_signal_handlers_disconnect_by_func (options->animate_button,
                                            gimp_warp_tool_animate,
                                            wt);
    }
}

static void
gimp_warp_set_start_mode_as_move (GimpWarpOptions *options)
{
  /* don't start the tool after committing in an non-functioning state */
  if (options->behavior == GIMP_WARP_BEHAVIOR_ERASE ||
      options->behavior == GIMP_WARP_BEHAVIOR_SMOOTH)
    {
      gimp_int_combo_box_set_active (GIMP_INT_COMBO_BOX (options->behavior_combo),
                                     GIMP_WARP_BEHAVIOR_MOVE);
    }
}

static void
gimp_warp_tool_commit (GimpWarpTool *wt)
{
  GimpTool        *tool    = GIMP_TOOL (wt);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  /* Is there a warp to commit ? */
  if (tool->display && gimp_tool_can_undo (tool, tool->display))
    {
      gimp_tool_control_push_preserve (tool->control, TRUE);
      gimp_warp_tool_set_sampler (wt, /* commit = */ TRUE);

      if (wt->is_a_layer_group_warp)
        {
          gimp_warp_tool_group_commit (wt);
          gimp_warp_tool_reset_group_layers (wt);
        }
      else if (GIMP_IS_DRAWABLE_FILTER (wt->filter))
        {
          gimp_drawable_filter_commit (wt->filter, FALSE,
                                       GIMP_PROGRESS (tool), FALSE);
          g_clear_object (&wt->filter);
        }

      gimp_tool_control_pop_preserve (tool->control);
      gimp_image_flush (gimp_display_get_image (tool->display));
      gimp_warp_set_start_mode_as_move (options);
    }
}

static gboolean
warping_is_timed (GimpWarpOptions *options)
{
  return options->stroke_periodically &&
         options->stroke_periodically_rate > 0.0 &&
         !(options->behavior == GIMP_WARP_BEHAVIOR_MOVE && options->stroke_during_motion);
}

static void
gimp_warp_tool_start_stroke_timer (GimpWarpTool *wt)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  gimp_warp_tool_stop_stroke_timer (wt);

  if (warping_is_timed (options))
    {
      gdouble fps = STROKE_TIMER_MAX_FPS * options->stroke_periodically_rate / GUI_SCALE;

      /* Milliseconds per user defined frame */
      wt->stroke_timer = g_timeout_add (1000.0 / fps,
                                        (GSourceFunc) gimp_warp_tool_stroke_timer,
                                        wt);
    }
}

static void
gimp_warp_tool_stop_stroke_timer (GimpWarpTool *wt)
{
  if (wt->stroke_timer)
    g_source_remove (wt->stroke_timer);

  wt->stroke_timer = 0;
}

static gboolean
gimp_warp_tool_stroke_timer (GimpWarpTool *wt)
{
  GimpTool *tool = GIMP_TOOL (wt);
  gint      off_x, off_y;

  g_return_val_if_fail (g_list_length (tool->drawables) == 1, FALSE);

  gimp_item_get_offset (GIMP_ITEM (tool->drawables->data), &off_x, &off_y);

  gimp_warp_tool_stroke_append (wt,
                                'L', wt->last_pos.x - off_x,
                                     wt->last_pos.y - off_y);

  return TRUE;
}

static void
gimp_warp_tool_create_graph (GimpWarpTool *wt)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GeglNode        *graph;           /* Wrapper to be returned */
  GeglNode        *input, *output;  /* Proxy nodes */
  GeglNode        *coords, *render; /* Render nodes */

  /* render_node is not supposed to be recreated */
  g_return_if_fail (wt->graph == NULL);

  graph = gegl_node_new ();

  input  = gegl_node_get_input_proxy  (graph, "input");
  output = gegl_node_get_output_proxy (graph, "output");

  coords = gegl_node_new_child (graph,
                                "operation", "gegl:buffer-source",
                                "buffer",    wt->coords_buffer,
                                NULL);

  render = gegl_node_new_child (graph,
                                "operation",    "gegl:map-relative",
                                "abyss-policy", options->abyss_policy,
                                NULL);

  gegl_node_link_many (input, render, output, NULL);
  gegl_node_connect (coords, "output",
                     render, "aux");

  wt->graph       = graph;
  wt->render_node = render;
}

static void
gimp_warp_tool_create_filter (GimpWarpTool *wt,
                              GimpDrawable *drawable)
{
  if (! wt->graph)
    gimp_warp_tool_create_graph (wt);

  /* FALSE is for preview quality, overriden by high_quality_preview */
  gimp_warp_tool_set_sampler (wt, FALSE);

  wt->filter = gimp_drawable_filter_new (drawable,
                                         _("Warp transform"),
                                         wt->graph,
                                         GIMP_ICON_TOOL_WARP);

  gimp_drawable_filter_set_region (wt->filter, GIMP_FILTER_REGION_DRAWABLE);

  g_signal_connect (wt->filter, "flush",
                    G_CALLBACK (gimp_warp_tool_filter_flush),
                    wt);
}

static void
gimp_warp_tool_set_sampler (GimpWarpTool *wt,
                            gboolean      commit)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GeglSamplerType  sampler;
  GeglSamplerType  old_sampler;

  if (! wt->render_node)
    return;

  if (commit || options->high_quality_preview)
    sampler = (GeglSamplerType) options->interpolation;
  else
    sampler = PREVIEW_SAMPLER;

  gegl_node_get (wt->render_node,
                 "sampler-type", &old_sampler,
                 NULL);

  if (sampler != old_sampler)
    {
      gegl_node_set (wt->render_node,
                     "sampler-type", sampler,
                     NULL);

      gimp_warp_tool_update_bounds (wt);
      gimp_warp_tool_update_stroke (wt, NULL);
    }
}

static GeglRectangle
gimp_warp_tool_get_stroke_bounds (GeglNode *node)
{
  GeglRectangle  bbox = {0, 0, 0, 0};
  GeglPath      *stroke;
  gdouble        size;

  gegl_node_get (node,
                 "stroke", &stroke,
                 "size",   &size,
                 NULL);

  if (stroke)
    {
      gdouble        min_x;
      gdouble        max_x;
      gdouble        min_y;
      gdouble        max_y;

      gegl_path_get_bounds (stroke, &min_x, &max_x, &min_y, &max_y);
      g_object_unref (stroke);

      bbox.x      = floor (min_x - size * 0.5);
      bbox.y      = floor (min_y - size * 0.5);
      bbox.width  = ceil (max_x + size * 0.5) - bbox.x;
      bbox.height = ceil (max_y + size * 0.5) - bbox.y;
    }

  return bbox;
}

static GeglRectangle
gimp_warp_tool_get_node_bounds (GeglNode *node)
{
  GeglRectangle *bounds;

  if (! node || strcmp (gegl_node_get_operation (node), "gegl:warp"))
    return *GEGL_RECTANGLE (0, 0, 0, 0);

  bounds = g_object_get_data (G_OBJECT (node), "gimp-warp-tool-bounds");

  if (! bounds)
    {
      GeglNode      *input_node;
      GeglRectangle  input_bounds;
      GeglRectangle  stroke_bounds;

      input_node   = gegl_node_get_producer (node, "input", NULL);
      input_bounds = gimp_warp_tool_get_node_bounds (input_node);

      stroke_bounds = gimp_warp_tool_get_stroke_bounds (node);

      gegl_rectangle_bounding_box (&input_bounds,
                                   &input_bounds, &stroke_bounds);

      bounds = gegl_rectangle_dup (&input_bounds);

      g_object_set_data_full (G_OBJECT (node), "gimp-warp-tool-bounds",
                              bounds, g_free);
    }

  return *bounds;
}

static void
gimp_warp_tool_clear_node_bounds (GeglNode *node)
{
  if (node && ! strcmp (gegl_node_get_operation (node), "gegl:warp"))
    g_object_set_data (G_OBJECT (node), "gimp-warp-tool-bounds", NULL);
}

static GeglRectangle
gimp_warp_tool_get_invalidated_by_change (GimpWarpTool        *wt,
                                          const GeglRectangle *area)
{
  GeglRectangle result = *area;

  if (! warp_is_active (wt))
    return result;

  if (wt->render_node)
    {
      GeglOperation *operation = gegl_node_get_gegl_operation (wt->render_node);

      result = gegl_operation_get_invalidated_by_change (operation,
                                                         "aux", area);
    }

  return result;
}

static void
gimp_warp_tool_update_bounds (GimpWarpTool *wt)
{
  GeglRectangle bounds = {0, 0, 0, 0};

  if (! warp_is_active (wt))
    return;

  if (wt->render_node)
    {
      GeglNode *node = gegl_node_get_producer (wt->render_node, "aux", NULL);

      bounds = gimp_warp_tool_get_node_bounds (node);

      bounds = gimp_warp_tool_get_invalidated_by_change (wt, &bounds);
    }

  gimp_drawable_filter_set_crop (wt->filter, &bounds, FALSE);
}

static void
gimp_warp_tool_update_area (GimpWarpTool        *wt,
                            const GeglRectangle *area,
                            gboolean             synchronous)
{
  GeglRectangle  rect;
  GimpContainer *filters;

  if (! warp_is_active (wt))
    return;

  rect = gimp_warp_tool_get_invalidated_by_change (wt, area);

  /* Move this operation below any non-destructive filters that
   * may be active, so that it's directly affect the raw pixels. */
  filters =
    gimp_drawable_get_filters (gimp_drawable_filter_get_drawable (wt->filter));

  if (gimp_container_have (filters, GIMP_OBJECT (wt->filter)))
  {
    gint end_index = gimp_container_get_n_children (filters) - 1;
    gint index     = gimp_container_get_child_index (filters,
                                                     GIMP_OBJECT (wt->filter));

    if (end_index > 0 && index != end_index)
      gimp_container_reorder (filters, GIMP_OBJECT (wt->filter),
                              end_index);
  }

  if (synchronous)
    {
      GimpTool  *tool  = GIMP_TOOL (wt);
      GimpImage *image = gimp_display_get_image (tool->display);

      g_signal_handlers_block_by_func (wt->filter,
                                       gimp_warp_tool_filter_flush,
                                       wt);

      gimp_drawable_filter_apply (wt->filter, &rect);

      gimp_projection_flush_now (gimp_image_get_projection (image), TRUE);
      gimp_display_flush_now (tool->display);

      g_signal_handlers_unblock_by_func (wt->filter,
                                         gimp_warp_tool_filter_flush,
                                         wt);
    }
  else
    {
      gimp_drawable_filter_apply (wt->filter, &rect);
    }
}

static void
gimp_warp_tool_update_stroke (GimpWarpTool *wt,
                              GeglNode     *node)
{
  GeglRectangle bounds = {0, 0, 0, 0};

  if (! warp_is_active (wt))
    return;

  if (node)
    {
      bounds = gimp_warp_tool_get_stroke_bounds (node);
    }
  else if (wt->render_node)
    {
      node = gegl_node_get_producer (wt->render_node, "aux", NULL);
      bounds = gimp_warp_tool_get_node_bounds (node);
    }

  if (! gegl_rectangle_is_empty (&bounds))
    {
#ifdef WARP_DEBUG
      /* Debug output to print the updated stroke area coordinates and dimensions */
      g_printerr ("update stroke: (%d,%d), %dx%d\n",
                  bounds.x, bounds.y,
                  bounds.width, bounds.height);
#endif

      gimp_warp_tool_update_area (wt, &bounds, FALSE);
    }
}

static void
gimp_warp_tool_stroke_append (GimpWarpTool *wt,
                              gchar         type,
                              gdouble       x,
                              gdouble       y)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GeglRectangle    area;

  if (! warp_is_active (wt))
    return;

  gegl_path_append (wt->current_stroke, type, x, y);

  area.x      = floor (x - options->effect_size * 0.5);
  area.y      = floor (y - options->effect_size * 0.5);
  area.width  = ceil  (x + options->effect_size * 0.5) - area.x;
  area.height = ceil  (y + options->effect_size * 0.5) - area.y;

#ifdef WARP_DEBUG
  g_printerr ("update rect: (%d,%d), %dx%d\n",
              area.x, area.y,
              area.width, area.height);
#endif

  if (wt->render_node)
    {
      GeglNode *node = gegl_node_get_producer (wt->render_node, "aux", NULL);

      gimp_warp_tool_clear_node_bounds (node);

      gimp_warp_tool_update_bounds (wt);
    }

  gimp_warp_tool_update_area (wt, &area, options->real_time_preview);
}

static void
gimp_warp_tool_filter_flush (GimpDrawableFilter *filter,
                             GimpTool           *tool)
{
  GimpImage *image = gimp_display_get_image (tool->display);

  gimp_projection_flush (gimp_image_get_projection (image));
}

static void
gimp_warp_tool_add_op (GimpWarpTool *wt,
                       GeglNode     *op)
{
  GeglNode *last_op;

  g_return_if_fail (GEGL_IS_NODE (wt->render_node));

  gegl_node_add_child (wt->graph, op);

  last_op = gegl_node_get_producer (wt->render_node, "aux", NULL);

  gegl_node_disconnect (wt->render_node, "aux");
  gegl_node_link (last_op, op);
  gegl_node_connect (op,              "output",
                     wt->render_node, "aux");
}

static void
gimp_warp_tool_remove_op (GimpWarpTool *wt,
                          GeglNode     *op)
{
  GeglNode *previous;

  g_return_if_fail (GEGL_IS_NODE (wt->render_node));

  previous = gegl_node_get_producer (op, "input", NULL);

  gegl_node_disconnect (op,              "input");
  gegl_node_connect (previous,        "output",
                     wt->render_node, "aux");

  gegl_node_remove_child (wt->graph, op);
}

static void
gimp_warp_tool_free_op (GeglNode *op)
{
  GeglNode *parent;

  parent = gegl_node_get_parent (op);

  gimp_assert (parent != NULL);

  gegl_node_remove_child (parent, op);
}

static void
gimp_warp_tool_free_image_map (GimpWarpTool *wt)
{
  if (warp_is_active (wt) && GIMP_IS_DRAWABLE_FILTER (wt->filter))
    {
      gimp_drawable_filter_abort (wt->filter);
      g_clear_object (&wt->filter);
    }
}

static void
gimp_warp_tool_animate (GimpWarpTool *wt)
{
  GimpTool        *tool    = GIMP_TOOL (wt);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GimpImage       *orig_image;
  GimpImage       *image;
  GimpLayer       *layer;
  GimpLayer       *first_layer;
  GeglNode        *scale_node;
  GimpProgress    *progress;
  GtkWidget       *widget;
  gint             i;

  g_return_if_fail (g_list_length (tool->drawables) == 1);

  if (! gimp_warp_tool_can_undo (tool, tool->display))
    {
      gimp_tool_message_literal (tool, tool->display,
                                 _("Please add some warp strokes first."));
      return;
    }

  /*  get rid of the image map so we can use wt->graph  */
  gimp_warp_tool_free_image_map (wt);

  gimp_warp_tool_set_sampler (wt, /* commit = */ TRUE);

  gimp_progress_start (GIMP_PROGRESS (tool), FALSE,
                       _("Rendering Frame %d"), 1);

  orig_image = gimp_item_get_image (GIMP_ITEM (tool->drawables->data));

  image = gimp_create_image (orig_image->gimp,
                             gimp_item_get_width  (GIMP_ITEM (tool->drawables->data)),
                             gimp_item_get_height (GIMP_ITEM (tool->drawables->data)),
                             gimp_drawable_get_base_type (tool->drawables->data),
                             gimp_drawable_get_precision (tool->drawables->data),
                             TRUE);

  /*  the first frame is always the unwarped image  */
  layer = GIMP_LAYER (gimp_item_convert (GIMP_ITEM (tool->drawables->data), image,
                                         GIMP_TYPE_LAYER));
  gimp_object_take_name (GIMP_OBJECT (layer),
                         g_strdup_printf (_("Frame %d"), 1));

  gimp_item_set_offset (GIMP_ITEM (layer), 0, 0);
  gimp_item_set_visible (GIMP_ITEM (layer), TRUE, FALSE);
  gimp_layer_set_mode (layer, gimp_image_get_default_new_layer_mode (image),
                       FALSE);
  gimp_layer_set_opacity (layer, GIMP_OPACITY_OPAQUE, FALSE);
  gimp_image_add_layer (image, layer, NULL, 0, FALSE);

  first_layer = layer;

  scale_node = gegl_node_new_child (NULL,
                                    "operation",    "gimp:scalar-multiply",
                                    "n-components", 2,
                                    NULL);
  gimp_warp_tool_add_op (wt, scale_node);

  progress = gimp_sub_progress_new (GIMP_PROGRESS (tool));

  for (i = 1; i < options->n_animation_frames; i++)
    {
      gimp_progress_set_text (GIMP_PROGRESS (tool),
                              _("Rendering Frame %d"), i + 1);

      gimp_sub_progress_set_step (GIMP_SUB_PROGRESS (progress),
                                  i, options->n_animation_frames);

      layer = GIMP_LAYER (gimp_item_duplicate (GIMP_ITEM (first_layer),
                                               GIMP_TYPE_LAYER));
      gimp_object_take_name (GIMP_OBJECT (layer),
                             g_strdup_printf (_("Frame %d"), i + 1));

      gegl_node_set (scale_node,
                     "factor", (gdouble) i /
                               (gdouble) (options->n_animation_frames - 1),
                     NULL);

      gimp_gegl_apply_operation (gimp_drawable_get_buffer (GIMP_DRAWABLE (first_layer)),
                                 progress,
                                 _("Frame"),
                                 wt->graph,
                                 gimp_drawable_get_buffer (GIMP_DRAWABLE (layer)),
                                 NULL, FALSE);

      gimp_image_add_layer (image, layer, NULL, 0, FALSE);
    }

  g_object_unref (progress);

  gimp_warp_tool_remove_op (wt, scale_node);

  gimp_progress_end (GIMP_PROGRESS (tool));

  /*  recreate the image map  */
  gimp_warp_tool_create_filter (wt, tool->drawables->data);
  gimp_warp_tool_update_stroke (wt, NULL);

  widget = GTK_WIDGET (gimp_display_get_shell (tool->display));
  gimp_create_display (orig_image->gimp, image, gimp_unit_pixel (), 1.0,
                       G_OBJECT (gimp_widget_get_monitor (widget)));
  g_object_unref (image);
}

static void
update_group_warp_progress (GimpWarpTool *wt,
                            GimpProgress *progress,
                            const gchar  *item_name,
                            WarpGroup    *warp_group)
{
  gimp_progress_set_text (GIMP_PROGRESS (GIMP_TOOL (wt)),
                          _("Warping %s"), item_name);

  gimp_sub_progress_set_step (GIMP_SUB_PROGRESS (progress),
                              warp_group->warp_counter, warp_group->warp_total);
}

static void
print_group_tree (GimpItem *item,
                  gint      level,
                  gboolean  print_tree)
{
  if (print_tree)
    {
      for (gint i = 0; i < level; i++)
        {
          g_print ("  ");
        }

      g_print ("%s\n", gimp_object_get_name (item));

      if (gimp_item_is_group_layer (item))
        {
          GList *children;

          children = gimp_group_layer_get_all_child_items (GIMP_GROUP_LAYER (item), FALSE);

          for (GList *iter = children; iter; iter = iter->next)
            {
              print_group_tree (GIMP_ITEM (iter->data), level + 1, print_tree);
            }

          g_list_free (children);
        }
    }
}

static gboolean
item_get_content_bbox (GimpItem      *item,
                       GeglRectangle *content_bbox,
                       GetContentMode offset_mode)
{
  GeglRectangle  item_bbox;
  GimpAutoShrink pixels;

  /* Retrieve the bounding box of the item */
  gimp_item_get_bounding_box (item, &item_bbox);

  /* Attempt to find the content area; uniform color blocks may show as EMPTY */
  pixels = gimp_pickable_auto_shrink (GIMP_PICKABLE (item),
                                      0, 0,
                                      item_bbox.width, item_bbox.height,
                                      &content_bbox->x, &content_bbox->y,
                                      &content_bbox->width, &content_bbox->height);

  switch (pixels)
    {
    case GIMP_AUTO_SHRINK_EMPTY:
    case GIMP_AUTO_SHRINK_BLACK:
      return FALSE;
    case GIMP_AUTO_SHRINK_SHRINK:
      break;
    case GIMP_AUTO_SHRINK_UNIFORM:
      break;
    case GIMP_AUTO_SHRINK_UNSHRINKABLE:
    default:
      content_bbox->x      = item_bbox.x;
      content_bbox->y      = item_bbox.y;
      content_bbox->width  = item_bbox.width;
      content_bbox->height = item_bbox.height;
      break;
    }

  /* Adjust the content offset to include the layer offset, if requested */
  if (offset_mode == CONTENT_WITH_OFFSET)
    {
      content_bbox->x += item_bbox.x;
      content_bbox->y += item_bbox.y;
    }

  return TRUE;
}

static WarpGroup *
warp_group_initialize (GimpWarpTool *wt,
                       GList        *items)
{
  WarpGroup *warp_group = g_new0 (WarpGroup, 1);
  GeglNode  *node       = gegl_node_get_producer (wt->render_node, "aux", NULL);

  /* Get the layer group offsets and dimensions */
  gimp_item_get_bounding_box (wt->layer_group, &warp_group->layer_bbox);

  /* The area the warp effect has touched */
  warp_group->effect_bbox = gimp_warp_tool_get_node_bounds (node);

  /* The warp effect offset is relative to the warp layer offset */
  warp_group->effect_bbox.x += warp_group->layer_bbox.x;
  warp_group->effect_bbox.y += warp_group->layer_bbox.y;

  /* A node to offset the warp to position it correctly on an offset layer */
  warp_group->offset_node = gegl_node_new_child (NULL, "operation", "gegl:translate", NULL);
  gimp_warp_tool_add_op (wt, warp_group->offset_node);

  warp_group->items        = items;
  warp_group->warp_counter = 0;

  return warp_group;
}

static void
resize_layer_pre_warp (ChildResizeData *child_data,
                       GimpContext     *context,
                       WarpGroup       *warp_group)
{
  GeglRectangle content_bbox;
  gint          max_x, max_y;
  gint          warp_right_edge, layer_right_edge;
  gint          warp_bottom_edge, layer_bottom_edge;

  /* Get the content bounding box of the item */

  if (! item_get_content_bbox (child_data->item, &content_bbox, CONTENT_NO_OFFSET))
    {
      g_print ("Prewarp resize %s is has no content.\n", gimp_object_get_name (child_data->item));
    }

  /* Store the margins around the content for restoration */
  child_data->left_margin.width   = content_bbox.x;
  child_data->left_margin.height  = content_bbox.y;
  child_data->right_margin.width  = child_data->item_bbox.width  - (content_bbox.x + content_bbox.width);
  child_data->right_margin.height = child_data->item_bbox.height - (content_bbox.y + content_bbox.height);

  /* Determine the minimum x and y coordinates for the expanded area */
  child_data->layer_expanded_bbox.x = MIN (warp_group->effect_bbox.x, child_data->item_bbox.x);
  child_data->layer_expanded_bbox.y = MIN (warp_group->effect_bbox.y, child_data->item_bbox.y);

  /* Calculate the farthest right edge of the expanded area */
  warp_right_edge  = warp_group->effect_bbox.x + warp_group->effect_bbox.width;
  layer_right_edge = child_data->item_bbox.x + child_data->item_bbox.width;
  max_x            = MAX (warp_right_edge, layer_right_edge);

  /* Calculate the farthest bottom edge of the expanded area */
  warp_bottom_edge  = warp_group->effect_bbox.y + warp_group->effect_bbox.height;
  layer_bottom_edge = child_data->item_bbox.y + child_data->item_bbox.height;
  max_y             = MAX (warp_bottom_edge, layer_bottom_edge);

  /* Calculate the width and height of the expanded area */
  child_data->layer_expanded_bbox.width  = max_x - child_data->layer_expanded_bbox.x;
  child_data->layer_expanded_bbox.height = max_y - child_data->layer_expanded_bbox.y;

  /* Resize the layer to provide room for warping */
  gimp_item_resize (GIMP_ITEM (child_data->item), context, GIMP_FILL_TRANSPARENT,
                    child_data->layer_expanded_bbox.width,
                    child_data->layer_expanded_bbox.height,
                    child_data->item_bbox.x - child_data->layer_expanded_bbox.x,
                    child_data->item_bbox.y - child_data->layer_expanded_bbox.y);

  /* Update the item bounding box with the new offsets after resizing */
  gimp_item_get_offset (child_data->item, &child_data->item_bbox.x, &child_data->item_bbox.y);
}

static void
resize_layer_post_warp (GimpItem      *item,
                        GimpContext   *context,
                        GeglRectangle *left_margin,
                        GeglRectangle *right_margin)
{
  GeglRectangle layer, warp_content_bbox;
  gint new_width, new_height, offset_x, offset_y;

  /* Get current dimensions of the layer */
  layer.width  = gimp_item_get_width (item);
  layer.height = gimp_item_get_height (item);

  /* Find the warped layer's new content area, non-uniform pixels */

  if (! item_get_content_bbox (item, &warp_content_bbox, CONTENT_NO_OFFSET))
    {
      g_print ("Post-warp-resize: %s has no content.\n", gimp_object_get_name (item));
    }
  /* Calculate the new width and height based on warp content and margins */
  new_width  = warp_content_bbox.width  + right_margin->width  + left_margin->width;
  new_height = warp_content_bbox.height + right_margin->height + left_margin->height;

  /* Calculate the x and y offsets based on the original margins */
  offset_x = left_margin->width - warp_content_bbox.x;
  offset_y = left_margin->height - warp_content_bbox.y;

  /* Resize the layer to match its original margins */
  gimp_item_resize (GIMP_ITEM (item), context, GIMP_FILL_TRANSPARENT,
                    new_width,
                    new_height,
                    offset_x,
                    offset_y);
}

static void
apply_warp_to_item (GimpWarpTool  *wt,
                    WarpGroup     *warp_group,
                    GimpItem      *item,
                    GimpProgress  *progress,
                    GeglRectangle *layer_bbox)
{
  warp_group->warp_counter += 1;

  /* Offset the warp according to its relative position to the group layer */
  gegl_node_set (warp_group->offset_node,
                 "x", (gdouble) (-1 * (layer_bbox->x - warp_group->layer_bbox.x)),
                 "y", (gdouble) (-1 * (layer_bbox->y - warp_group->layer_bbox.y)),
                 NULL);

  gimp_gegl_apply_operation (gimp_drawable_get_buffer (GIMP_DRAWABLE (item)),
                             progress,
                             _("Warp Layer Group"),
                             wt->graph,
                             gimp_drawable_get_buffer (GIMP_DRAWABLE (item)),
                             NULL,
                             FALSE);
  if (PRINT_WARP_COUNTER)
    g_print ("Warp Item: %-42s  -> warp number: %d\n", gimp_object_get_name (item),
            warp_group->warp_counter);
}

static gchar *
truncate_name (const gchar *name,
               gint         max_length)
{
  if (strlen(name) > max_length)
    {
      gchar *truncated = g_malloc(max_length + 1);
      g_strlcpy(truncated, name, max_length - 2);
      strcat(truncated, "...");
      return truncated;
    }
  else
    {
      return g_strdup(name);
    }
}

static void
add_item_print_summary (GList    **summary,
                        GimpItem  *item,
                        gboolean   warped)
{
  const gint  max_length = 24;
  gboolean    is_group   = gimp_item_is_group_layer (item);
  gboolean    has_mask   = (gimp_layer_get_mask (GIMP_LAYER (item)) != NULL);
  gchar      *trunc_name = truncate_name (gimp_object_get_name(item), max_length);

  *summary = g_list_append (*summary,
                            g_strdup_printf ("%-30s %-7s %-6s %-5s\n",
                                              trunc_name,
                                              is_group ? "Group" : "Basic",
                                              has_mask ? "Yes" : "No",
                                              warped ? "Yes" : "No"));
  g_free (trunc_name);
}

static void
print_warp_summary_table (GList    *summary,
                          gboolean  print_table)
{
  if (print_table)
    {
      g_print ("\n");
      g_print ("%-30s %-7s %-6s %-5s\n",
               "Item Name", "Type", "Mask", "Warped");
      g_print ("%-30s %-7s %-6s %-5s\n",
               "----------------------------", "-----", "----", "------");

      for (GList *iter = summary; iter; iter = iter->next)
        {
          g_print ("%s", (gchar *) iter->data);
          g_free (iter->data);
        }
    }

  g_list_free (summary);
}

static void
debug_print_warp_item (GimpItem *item)
{
  if (PRINT_WARP_PROGRESS)
    g_print ("Warping : %s\n", gimp_object_get_name (item));
}

static void
warp_child_item (GimpWarpTool    *wt,
                 GimpLayerMask   *mask,
                 GimpContext     *context,
                 GimpProgress    *progress,
                 WarpGroup       *warp_group,
                 ChildResizeData *child_data)
{
  update_group_warp_progress (wt, progress, gimp_object_get_name (child_data->item), warp_group);
  gimp_item_get_bounding_box (child_data->item, &child_data->item_bbox);

  resize_layer_pre_warp (child_data, context, warp_group);
  debug_print_warp_item (child_data->item);
  apply_warp_to_item (wt, warp_group, child_data->item, progress, &child_data->item_bbox);

  if (mask)
    {
      debug_print_warp_item (GIMP_ITEM (mask));
      apply_warp_to_item (wt, warp_group, GIMP_ITEM (mask), progress, &child_data->item_bbox);
      gimp_drawable_update (GIMP_DRAWABLE (mask), 0, 0, -1, -1);
    }
}

static void
warp_group_mask (GimpWarpTool  *wt,
                 GimpLayerMask *mask,
                 GimpProgress  *progress,
                 WarpGroup     *warp_group)
{
  GeglRectangle  item_bbox;

  gimp_item_get_bounding_box (GIMP_ITEM (mask), &item_bbox);
  debug_print_warp_item (GIMP_ITEM (mask));
  apply_warp_to_item (wt, warp_group, GIMP_ITEM (mask), progress, &item_bbox);
  gimp_drawable_update (GIMP_DRAWABLE (mask), 0, 0, -1, -1);
}

static void
resize_and_cleanup_children (GList       **child_data_list,
                             GimpContext  *context)
{
  if (*child_data_list)
    {
      for (GList *child_iter = *child_data_list; child_iter != NULL; child_iter = child_iter->next)
        {
          ChildResizeData *child_data = (ChildResizeData *) child_iter->data;

          resize_layer_post_warp (child_data->item, context,
                                  &child_data->left_margin, &child_data->right_margin);
        }

      g_list_free_full (*child_data_list, g_free);
      *child_data_list = NULL;
    }
}

static void
warp_items (GimpWarpTool *wt,
            WarpGroup    *warp_group,
            GimpProgress *progress)
{
  GimpTool        *tool            = GIMP_TOOL (wt);
  GimpImage       *image           = gimp_display_get_image (tool->display);
  GimpContext     *context         = gimp_get_user_context (image->gimp);
  GList           *summary         = NULL;
  GList           *child_data_list = NULL;
  GimpWarpOptions *options         = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  for (GList *iter = warp_group->items; iter; iter = iter->next)
    {
      GimpItem *item      = iter->data;
      gboolean  warped    = FALSE;
      GimpLayerMask *mask = gimp_layer_get_mask (GIMP_LAYER (item));

      if (gimp_item_is_basic_layer (item))
        {
          ChildResizeData *child_data = g_new0 (ChildResizeData, 1);

          child_data->item = item;
          warp_child_item (wt, mask, context, progress, warp_group, child_data);
          warped = TRUE;
          child_data_list = g_list_append(child_data_list, child_data);
        }
      else if (gimp_item_is_group_layer (item) && mask)
        {
          if (options->group_warp_group_masks)
            {
              warp_group_mask (wt, mask, progress, warp_group);
              warped = TRUE;
            }

          if (options->group_opaque_group_masks)
            {
              gimp_channel_all (GIMP_CHANNEL (mask), FALSE);
            }
        }

      /* The parent of this batch of child items in the list */
      if (gimp_item_is_group_layer (item))
        {
          resize_and_cleanup_children (&child_data_list, context);
        }

      gimp_projection_flush_now (gimp_image_get_projection (image), TRUE);
      gimp_display_flush_now (tool->display);
      add_item_print_summary (&summary, item, warped);
    }

  print_warp_summary_table (summary, PRINT_DEBUG_TABLES);
}

static void
count_warp_items (GimpWarpTool *wt,
                  WarpGroup    *warp_group)
{
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);

  for (GList *iter = warp_group->items; iter; iter = iter->next)
    {
      GimpItem      *item = iter->data;
      GimpLayerMask *mask = gimp_layer_get_mask (GIMP_LAYER (item));

      /* Increment by 2 if item is a basic layer with a mask */
      if (gimp_item_is_basic_layer (item) && mask)
        warp_group->warp_total += 2;
      else if (gimp_item_is_basic_layer (item))
        warp_group->warp_total ++;

      /* Increment for group layers with mask if group_warp_group_masks is set */
      if (gimp_item_is_group_layer (item) && mask && options->group_warp_group_masks)
        warp_group->warp_total ++;

      if (PRINT_WARP_COUNTER)
        g_print ("Count Item: %-42s -> warp index : %d\n", gimp_object_get_name (item),
                  warp_group->warp_total);
    }
}

static void
get_warped_area_bbox (GimpWarpTool  *wt,
                      GeglRectangle *warp_area)
{
  GeglNode      *node = gegl_node_get_producer (wt->render_node, "aux", NULL);
  GeglRectangle  warp_layer_bbox;

  if (! wt->tmp_expand_layers)
    {
      g_warning ("No expansion layers available.");
      return;
    }

  /* Get the offset of the expanded warp area */
  gimp_item_get_bounding_box (GIMP_ITEM (wt->tmp_expand_layers->data), &warp_layer_bbox);

  /* Get the bounds of the warp node and adjust the position according to offset */
  *warp_area = gimp_warp_tool_get_node_bounds (node);
  warp_area->x += warp_layer_bbox.x;
  warp_area->y += warp_layer_bbox.y;
}

/* Checks basic layers for validity, sub group layers need special checks */
static gboolean
is_valid_warp_item (GimpWarpTool *wt,
                    GimpItem     *item)
{
  GeglRectangle content_bbox, warp_area_bbox;

  if (g_list_find (wt->tmp_expand_layers, item))
    {
      /* g_print ("Item : %s is in tmp_expand_layers.\n", gimp_object_get_name (item)); */
      return FALSE;
    }

  if (gimp_item_is_content_locked (item, NULL))
    {
      /* g_print ("Item : %s is content-locked.\n", gimp_object_get_name (item)); */
      return FALSE;
    }

  if (! item_get_content_bbox (item, &content_bbox, CONTENT_WITH_OFFSET))
    {
      /* g_print ("Item : %s has no content.\n", gimp_object_get_name (item)); */
      return FALSE;
    }

  get_warped_area_bbox (wt, &warp_area_bbox);

  if (! gegl_rectangle_intersect (NULL, &warp_area_bbox, &content_bbox))
    {
     /*  g_print ("Item : %s is outside the warped area.\n", gimp_object_get_name (item)); */
      return FALSE;
    }

  if (!gegl_rectangle_intersect (NULL, &warp_area_bbox, &content_bbox))
    {
      /* g_print ("Item %s is outside the warped area.\n", gimp_object_get_name (item)); */
      return FALSE;
    }

    /* g_print ("Item : %s is valid for warping.\n", gimp_object_get_name (item)); */
    return TRUE;
}

static gint
find_max_depth (GList *items)
{
  gint max_depth = 0;

  for (GList *iter = items; iter; iter = iter->next)
    {
      GimpItem *item = iter->data;
      gint item_depth = gimp_item_get_depth (item);

      if (item_depth > max_depth)
        {
          max_depth = item_depth;
        }
    }

  return max_depth;
}

static GList *
get_valid_warp_item_list_from_group (GimpWarpTool *wt)
{
  GimpGroupLayer *group = GIMP_GROUP_LAYER (wt->layer_group);
  GList *items          = gimp_group_layer_get_all_child_items (group, TRUE);
  GList *valid_items    = NULL;

  for (GList *iter = items; iter; iter = iter->next)
    {
      if (is_valid_warp_item (wt, iter->data))
        {
          valid_items = g_list_append (valid_items, iter->data);
        }
    }

  /* Add root item if valid */
  if (is_valid_warp_item (wt, wt->layer_group))
    {
      valid_items = g_list_append (valid_items, wt->layer_group);
    }

  g_list_free (items);

  return valid_items;
}

static GList *
group_items_by_parent (GList *items,
                       gint   level)
{
  GList *parent_group_list = NULL;

  for (GList *iter = items; iter; iter = iter->next)
    {
      GimpItem *item = iter->data;

      if (gimp_item_get_depth (item) == level)
        {
          GimpItem *parent = gimp_item_get_parent (item);
          ParentGroup *parent_group = NULL;

          /* Search for existing ParentGroup struct for this parent */
          for (GList *group_iter = parent_group_list; group_iter; group_iter = group_iter->next)
            {
              ParentGroup *existing_group = group_iter->data;
              if (existing_group->parent == parent)
                {
                  parent_group = existing_group;
                  break;
                }
            }

          /* If not found, create a new ParentGroup struct */
          if (parent_group == NULL)
            {
              parent_group = g_new0 (ParentGroup, 1);
              parent_group->parent = parent;
              parent_group->children = NULL;
              parent_group_list = g_list_prepend (parent_group_list, parent_group);
            }

          /* Add the child to the list */
          parent_group->children = g_list_prepend (parent_group->children, item);
        }
    }

  return parent_group_list;
}

static void
append_item_to_warp_list (GList   **warp_list,
                          GimpItem *item)
{
  /* Only add the item if it’s not already in the warp list */
  if (g_list_find (*warp_list, item) == NULL)
    {
      *warp_list = g_list_append (*warp_list, item);
    }
}

/* Children then Parent, from the deepest layer stack levels to the root */
static void
build_ordered_warp_item_list (GimpWarpTool  *wt,
                              GList        **warp_list)
{
  GList    *items     = get_valid_warp_item_list_from_group (wt);
  gint      max_depth = find_max_depth (items);

  /* Process each layer stack depth level, starting from the deepest */
  for (gint depth = max_depth; depth >= 0; depth--)
    {
      GList *parent_group_list = group_items_by_parent (items, depth);

      for (GList *group_iter = parent_group_list; group_iter; group_iter = group_iter->next)
        {
          ParentGroup *parent_group = group_iter->data;
          GimpItem    *parent       = parent_group->parent;
          GList       *children     = parent_group->children;

          for (GList *child_iter = children; child_iter; child_iter = child_iter->next)
            {
              GimpItem *child = child_iter->data;
              append_item_to_warp_list (warp_list, child);
            }

          if (parent != NULL)
            {
              append_item_to_warp_list (warp_list, parent);
            }

          g_list_free (children);
          g_free (parent_group);
        }

      g_list_free (parent_group_list);
    }
}


static gboolean
create_warp_item_list (GimpWarpTool *wt,
                       GList        **warp_list)
{
  print_group_tree (wt->layer_group, 0, PRINT_GROUP_TREE);
  gimp_warp_tool_free_image_map (wt);
  gimp_warp_tool_set_sampler (wt, TRUE);
  build_ordered_warp_item_list (wt, warp_list);

  if (! *warp_list)
    {
      GimpTool *tool = GIMP_TOOL (wt);

      gimp_tool_message_literal (tool, tool->display,
                                 _("No layers warped"));

      remove_tmp_expand_layers (wt);
      g_list_free (*warp_list);
      *warp_list = NULL;
      return FALSE;
    }

  return TRUE;
}

static void
handle_early_exit (GimpWarpTool *wt,
                   GimpImage    *image)
{
  gimp_image_undo_group_end (image);
  remove_tmp_expand_layers (wt);
}

static void
start_warp_progress_indicator (GimpTool      *tool,
                               GimpProgress **progress)
{
  gimp_progress_start (GIMP_PROGRESS (tool), FALSE, _("Warping Group Layers %d"), 1);
  *progress = gimp_sub_progress_new (GIMP_PROGRESS (tool));
}

static void
finalize_warp_progress_indicator (GimpTool     *tool,
                                  GimpProgress *progress)
{
  g_object_unref (progress);
  gimp_progress_end (GIMP_PROGRESS (tool));
}

static void
cleanup_after_warp (GimpWarpTool *wt,
                    WarpGroup    *warp_group)
{
  gimp_warp_tool_remove_op (wt, warp_group->offset_node);
  g_list_free (warp_group->items);
  warp_group->items = NULL;
  g_free (warp_group);
  remove_tmp_expand_layers (wt);
}

static void
gimp_warp_tool_group_commit (GimpWarpTool *wt)
{
  GimpTool      *tool       = GIMP_TOOL (wt);
  GimpImage     *image      = gimp_display_get_image (tool->display);
  GList         *warp_list  = NULL;
  WarpGroup     *warp_group;
  GimpProgress  *progress;

  gimp_image_undo_group_start (image, GIMP_UNDO_GROUP_DRAWABLE_MOD, _("Group Warp Transform"));

  gimp_set_busy (image->gimp);
  if (! create_warp_item_list (wt, &warp_list))
    {
      handle_early_exit (wt, image);
      return;
    }

  warp_group = warp_group_initialize (wt, warp_list);
  count_warp_items (wt, warp_group);
  start_warp_progress_indicator (tool, &progress);
  warp_items (wt, warp_group, progress);
  finalize_warp_progress_indicator (tool, progress);
  cleanup_after_warp (wt, warp_group);
  gimp_unset_busy (image->gimp);

  gimp_image_undo_group_end (image);
}

static GimpLayer *
create_expansion_layer (GimpImage *image,
                        GimpItem  *group,
                        gint       offset_x,
                        gint       offset_y,
                        const gchar *name)
{
  GimpLayer *layer;

  layer = gimp_layer_new (image,
                          1, 1,
                          gimp_image_get_layer_format (image, TRUE),
                          name,
                          0.0,
                          GIMP_LAYER_MODE_NORMAL);

  gimp_item_set_offset (GIMP_ITEM (layer), offset_x, offset_y);
  gimp_image_add_layer (image, layer, GIMP_LAYER (group), 0, FALSE);

  return layer;
}

static GList *
expand_layer_group (GimpWarpTool *wt,
                    GimpItem     *group)
{
  GimpImage       *image   = gimp_display_get_image (GIMP_TOOL (wt)->display);
  GimpWarpOptions *options = GIMP_WARP_TOOL_GET_OPTIONS (wt);
  GeglRectangle    group_bbox;
  gint             top_left_offset_x, top_left_offset_y;
  gint             bottom_right_offset_x, bottom_right_offset_y;
  gint             margin = options->group_margin;
  GList           *expanded_layers = NULL;
  GimpDrawable    *drawable;

  gimp_item_get_bounding_box (group, &group_bbox);

  /* Calculate offsets for the top-left and bottom-right expansion corners */
  top_left_offset_x     = group_bbox.x - (margin / 2);
  top_left_offset_y     = group_bbox.y - (margin/ 2);
  bottom_right_offset_x = group_bbox.x + group_bbox.width - 1 + (margin/ 2);
  bottom_right_offset_y = group_bbox.y + group_bbox.height - 1 + (margin / 2);

  /* Create top-left layer and add to expanded_layers */
  drawable = GIMP_DRAWABLE (create_expansion_layer (image,
                                                    group,
                                                    top_left_offset_x,
                                                    top_left_offset_y,
                                                    "warp_top_left_expander"));
  expanded_layers = g_list_append (expanded_layers, drawable);


  /* Create bottom-right layer and add to expanded_layers */
  drawable = GIMP_DRAWABLE (create_expansion_layer (image,
                                                    group,
                                                    bottom_right_offset_x,
                                                    bottom_right_offset_y,
                                                    "warp_bottom_right_expander"));
  expanded_layers = g_list_append (expanded_layers, drawable);

  /* Reset the active layer selection to the warp layer group */
  gimp_image_set_single_selected_layer (image, GIMP_LAYER (wt->layer_group));

  return expanded_layers;
}


