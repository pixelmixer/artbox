/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995-1999 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpwidgets/gimpwidgets.h"

#include "tools-types.h"

#include "config/gimpguiconfig.h"

#include "core/gimp.h"
#include "core/gimptoolinfo.h"

#include "paint/gimppaintoptions.h"

#include "widgets/gimplayermodebox.h"
#include "widgets/gimppropwidgets.h"
#include "widgets/gimpviewablebox.h"
#include "widgets/gimpwidgets-constructors.h"
#include "widgets/gimpwidgets-utils.h"

#include "gimpairbrushtool.h"
#include "gimpclonetool.h"
#include "gimpconvolvetool.h"
#include "gimpdodgeburntool.h"
#include "gimperasertool.h"
#include "gimphealtool.h"
#include "gimpinktool.h"
#include "gimpmybrushtool.h"
#include "gimppaintoptions-gui.h"
#include "gimppenciltool.h"
#include "gimpperspectiveclonetool.h"
#include "gimpsmudgetool.h"
#include "gimptooloptions-gui.h"

#include "gimp-intl.h"

typedef struct {
  GtkWidget   *image;
  const gchar *icon_name_active;
  const gchar *icon_name_inactive;
  const gchar *help_text_on;
  const gchar *help_text_off;
  const gchar *link_prop_name;
} ResourceToggleData;

typedef struct
{
  GObject      *config;
  const gchar  *prop_name;
  const gchar  *link_prop_name;
  const gchar  *reset_tooltip;
  gdouble       step_increment;
  gdouble       page_increment;
  gint          digits;
  gdouble       scale_min;
  gdouble       scale_max;
  gdouble       factor;
  gdouble       gamma;
  gboolean      constrain_drag;
  GCallback     reset_slider_callback;
  GCallback     reset_button_callback;
  GtkSizeGroup *link_group;
  gboolean      show_brush_linked;
  gboolean      show_brush_reset;
} BrushSliderOptions;


static gboolean gimp_paint_options_slider_reset_size     (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_ratio    (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_angle    (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_spacing  (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_hardness (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_force    (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);
static gboolean gimp_paint_options_slider_reset_jitter   (GtkWidget      *widget,
                                                          GdkEventButton *event,
                                                          gpointer        user_data);

static void gimp_paint_options_button_reset_size     (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_ratio    (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_angle    (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_spacing  (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_hardness (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_force    (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);
static void gimp_paint_options_button_reset_jitter   (GtkWidget        *button,
                                                      GimpPaintOptions *paint_options);

static GtkWidget *stroke_effects_gui          (GimpPaintOptions *paint_options,
                                               GType             tool_type,
                                               GtkWidget        *outer_vbox);
static GtkWidget *fade_colour_options_gui     (GimpPaintOptions *paint_options,
                                               GType             tool_type);
static GtkWidget *smoothing_options_gui       (GimpPaintOptions *paint_options,
                                               GType             tool_type);
static GtkWidget *expand_options_gui          (GimpPaintOptions *paint_options,
                                               GType             tool_type);

static GtkWidget *gimp_paint_options_gui_scale_with_buttons (BrushSliderOptions *slider_options);


/*  public functions  */

GtkWidget *
gimp_paint_options_gui_get_paint_mode_box (GtkWidget *options_gui)
{
  return g_object_get_data (G_OBJECT (options_gui),
                            "gimp-paint-options-gui-paint-mode-box");
}


/*  private functions  */

static GtkWidget *
create_expanded_frame (GtkWidget   *parent_vbox,
                       const gchar *expander_title,
                       gboolean     expanded,
                       gint         padding,
                       GtkWidget  **out_expander)
{
  GtkWidget *expander = gtk_expander_new (expander_title);
  GtkWidget *frame    = gimp_frame_new (NULL);
  GtkWidget *inner_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);

  gtk_container_add (GTK_CONTAINER (frame), inner_vbox);
  gtk_container_add (GTK_CONTAINER (expander), frame);
  gtk_box_pack_start (GTK_BOX (parent_vbox), expander, FALSE, FALSE, padding);

  gtk_expander_set_expanded (GTK_EXPANDER (expander), expanded);
  gtk_widget_set_visible (expander, TRUE);
  gtk_widget_set_visible (frame, TRUE);
  gtk_widget_set_visible (inner_vbox, TRUE);

  /* Optionally set out_expander to point to the expander widget */
  if (out_expander)
    {
      *out_expander = expander;
    }

  return inner_vbox;
}

static void
add_brush_option_sliders (GtkWidget     *outer_vbox,
                          GObject       *config,
                          GimpGuiConfig *gui_config,
                          GtkSizeGroup  *link_group,
                          GType          tool_type)
{
  GtkWidget *hbox;
  GtkWidget *inner_vbox;

  static BrushSliderOptions slider_options;

  inner_vbox = create_expanded_frame (outer_vbox, _("Brush Options"), TRUE, 2, NULL);

  /* Generic options */
  slider_options.config             = config,
  slider_options.link_group         = link_group,
  slider_options.show_brush_linked  = gui_config->show_brush_update_buttons,
  slider_options.show_brush_reset   = gui_config->show_brush_reset_buttons,
  slider_options.constrain_drag     = TRUE,
  slider_options.factor             = 1.0,
  slider_options.gamma              = 1.0,

  /* Brush Size Slider */
  slider_options.prop_name             = "brush-size";
  slider_options.link_prop_name        = "brush-link-size";
  slider_options.reset_tooltip         = _("Reset size to the brush defined size");
  slider_options.step_increment        = 1.0;
  slider_options.page_increment        = 10.0;
  slider_options.digits                = 2;
  slider_options.scale_min             = 1.0;
  slider_options.scale_max             = 1000.0;
  slider_options.factor                = 1.0;
  slider_options.gamma                 = 1.7;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_size);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_size);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  /* Brush Aspect Ratio Slider */
  slider_options.prop_name             = "brush-aspect-ratio";
  slider_options.link_prop_name        = "brush-link-aspect-ratio";
  slider_options.reset_tooltip         = _("Reset aspect ratio to the brush defined aspect ratio");
  slider_options.step_increment        = 0.01;
  slider_options.page_increment        = 0.1;
  slider_options.digits                = 2;
  slider_options.scale_min             = -1.0;
  slider_options.scale_max             = 1.0;
  slider_options.gamma                 = 1.0;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_ratio);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_ratio);
  slider_options.constrain_drag        = FALSE;

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  /* Brush Angle Slider */
  slider_options.prop_name             = "brush-angle";
  slider_options.link_prop_name        = "brush-link-angle";
  slider_options.reset_tooltip         = _("Reset angle to the brush defined angle");
  slider_options.step_increment        = 0.1;
  slider_options.page_increment        = 1.0;
  slider_options.digits                = 2;
  slider_options.scale_min             = -180.0;
  slider_options.scale_max             = 180.0;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_angle);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_angle);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  /* Brush Spacing Slider */
  slider_options.prop_name             = "brush-spacing";
  slider_options.link_prop_name        = "brush-link-spacing";
  slider_options.reset_tooltip         = _("Reset spacing to the brush defined spacing");
  slider_options.step_increment        = 0.1;
  slider_options.page_increment        = 1.0;
  slider_options.digits                = 1;
  slider_options.scale_min             = 1.0;
  slider_options.scale_max             = 200.0;
  slider_options.factor                = 100.0;
  slider_options.gamma                 = 1.7;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_spacing);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_spacing);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  /* Brush Hardness Slider */
  slider_options.prop_name             = "brush-hardness";
  slider_options.link_prop_name        = "brush-link-hardness";
  slider_options.reset_tooltip         = _("Reset hardness to the brush defined hardness");
  slider_options.step_increment        = 0.1;
  slider_options.page_increment        = 1.0;
  slider_options.digits                = 1;
  slider_options.scale_min             = 0.0;
  slider_options.scale_max             = 100.0;
  slider_options.factor                = 100.0;
  slider_options.gamma                 = 1.0;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_hardness);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_hardness);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  /* Brush Force Slider */
  slider_options.prop_name             = "brush-force";
  slider_options.link_prop_name        = NULL;
  slider_options.reset_tooltip         = _("Reset force to the default value");
  slider_options.step_increment        = 0.1;
  slider_options.page_increment        = 1.0;
  slider_options.digits                = 1;
  slider_options.scale_min             = 0.0;
  slider_options.scale_max             = 100.0;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_force);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_force);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);

  /* Hide when the tool is the pencil, as there is no meaning to Force */
  if (tool_type != GIMP_TYPE_PENCIL_TOOL)
    gtk_widget_show(hbox);

  /* Brush Jitter Slider */
  slider_options.prop_name             = "brush-jitter";
  slider_options.link_prop_name        = NULL;
  slider_options.reset_tooltip         = _("Reset jitter to the default value");
  slider_options.step_increment        = 1.0;
  slider_options.page_increment        = 10.0;
  slider_options.scale_min             = 0.0;
  slider_options.scale_max             = 1024.0;
  slider_options.reset_slider_callback = G_CALLBACK(gimp_paint_options_slider_reset_jitter);
  slider_options.reset_button_callback = G_CALLBACK(gimp_paint_options_button_reset_jitter);

  hbox = gimp_paint_options_gui_scale_with_buttons(&slider_options);
  gtk_box_pack_start(GTK_BOX(inner_vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);
}

static void
add_checkbox_to_vbox (GtkWidget   *vbox,
                      GObject     *config,
                      const gchar *property)
{
  GtkWidget *button = gimp_prop_check_button_new (config, property, NULL);
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
}

static void
add_additional_checkbox_options (GtkWidget        *outer_vbox,
                                 GObject          *config,
                                 GimpPaintOptions *options,
                                 GType             tool_type)
{
  GtkWidget *inner_vbox;

  inner_vbox = create_expanded_frame (outer_vbox, _("Additional Options"), FALSE, 2, NULL);

  /* Checkboxes based on tool type */
  if (tool_type == GIMP_TYPE_PENCIL_TOOL     ||
      tool_type == GIMP_TYPE_AIRBRUSH_TOOL   ||
      tool_type == GIMP_TYPE_PAINTBRUSH_TOOL ||
      tool_type == GIMP_TYPE_ERASER_TOOL     ||
      tool_type == GIMP_TYPE_SMUDGE_TOOL     ||
      tool_type == GIMP_TYPE_DODGE_BURN_TOOL)
    {
      add_checkbox_to_vbox(inner_vbox, config, "brush-pick-layer");
    }

  if (g_type_is_a (tool_type, GIMP_TYPE_BRUSH_TOOL))
    {
      add_checkbox_to_vbox (inner_vbox, config, "brush-lock-to-view");
      add_checkbox_to_vbox (inner_vbox, config, "brush-simple-boundary");
    }

  if (tool_type == GIMP_TYPE_ERASER_TOOL            ||
      tool_type == GIMP_TYPE_CLONE_TOOL             ||
      tool_type == GIMP_TYPE_HEAL_TOOL              ||
      tool_type == GIMP_TYPE_PERSPECTIVE_CLONE_TOOL ||
      tool_type == GIMP_TYPE_CONVOLVE_TOOL          ||
      tool_type == GIMP_TYPE_DODGE_BURN_TOOL        ||
      tool_type == GIMP_TYPE_SMUDGE_TOOL)
    {
      add_checkbox_to_vbox (inner_vbox, config, "hard");
    }

  if (tool_type == GIMP_TYPE_PAINTBRUSH_TOOL ||
      tool_type == GIMP_TYPE_PENCIL_TOOL     ||
      tool_type == GIMP_TYPE_AIRBRUSH_TOOL   ||
      tool_type == GIMP_TYPE_CLONE_TOOL      ||
      tool_type == GIMP_TYPE_HEAL_TOOL       ||
      tool_type == GIMP_TYPE_CONVOLVE_TOOL   ||
      tool_type == GIMP_TYPE_SMUDGE_TOOL)
    {
      GtkWidget *expand_frame = expand_options_gui(options, tool_type);
      gtk_box_pack_start (GTK_BOX(inner_vbox), expand_frame, FALSE, FALSE, 0);
      gtk_widget_show (expand_frame);
    }
}

GtkWidget *
gimp_paint_options_gui (GimpToolOptions *tool_options)
{
  Gimp             *gimp       = tool_options->tool_info->gimp;
  GimpGuiConfig    *gui_config = GIMP_GUI_CONFIG (gimp->config);
  GObject          *config     = G_OBJECT (tool_options);
  GimpPaintOptions *options    = GIMP_PAINT_OPTIONS (tool_options);
  GtkWidget        *vbox       = gimp_tool_options_gui (tool_options);
  GType             tool_type  = tool_options->tool_info->tool_type;
  GtkWidget        *menu, *scale;

  /* Brush button */
  if (g_type_is_a (tool_type, GIMP_TYPE_BRUSH_TOOL))
    {
      GtkWidget *button = gimp_prop_brush_box_new (NULL, GIMP_CONTEXT (tool_options),
                                                   _("Brush"), 2, "brush-view-type",
                                                   "brush-view-size", "gimp-brush-editor",
                                                   _("Edit this brush"));
      gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
    }

  /* Paint mode menu */
  menu = gimp_prop_layer_mode_box_new (config, "paint-mode",
                                       GIMP_LAYER_MODE_CONTEXT_PAINT);
  gimp_layer_mode_box_set_label (GIMP_LAYER_MODE_BOX (menu), _("Mode"));
  gimp_layer_mode_box_set_ellipsize (GIMP_LAYER_MODE_BOX (menu), PANGO_ELLIPSIZE_END);
  gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);

  g_object_set_data (G_OBJECT (vbox), "gimp-paint-options-gui-paint-mode-box", menu);

  /* Disable menu for certain tools */
  if (tool_type == GIMP_TYPE_ERASER_TOOL     ||
      tool_type == GIMP_TYPE_CONVOLVE_TOOL   ||
      tool_type == GIMP_TYPE_DODGE_BURN_TOOL ||
      tool_type == GIMP_TYPE_HEAL_TOOL       ||
      tool_type == GIMP_TYPE_MYBRUSH_TOOL    ||
      tool_type == GIMP_TYPE_SMUDGE_TOOL)
    {
      gtk_widget_set_sensitive (menu, FALSE);
    }

  /* Opacity scale */
  scale = gimp_prop_spin_scale_new (config, "opacity", 0.01, 0.1, 0);
  gimp_spin_scale_set_constrain_drag (GIMP_SPIN_SCALE (scale), TRUE);
  gimp_prop_widget_set_factor (scale, 100.0, 1.0, 10.0, 1);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  /* Brush-specific options */
  if (g_type_is_a (tool_type, GIMP_TYPE_BRUSH_TOOL))
    {
      GtkSizeGroup *link_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

      add_brush_option_sliders (vbox,
                                config,
                                gui_config,
                                link_group,
                                tool_type);

      g_object_unref (link_group);

      stroke_effects_gui (options, tool_type, vbox);
    }

  /* Additional checkbox options */
  if (g_type_is_a (tool_type, GIMP_TYPE_PAINT_TOOL) ||
      g_type_is_a (tool_type, GIMP_TYPE_BRUSH_TOOL))
    {
      add_additional_checkbox_options (vbox, config, options, tool_type);
    }

  return vbox;
}

static GtkWidget *
stroke_effects_gui (GimpPaintOptions *paint_options,
                      GType           tool_type,
                      GtkWidget      *outer_vbox)
{
  GObject   *config = G_OBJECT (paint_options);
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *dynamics_button;
  GtkWidget *multiply_button;
  GtkWidget *select_dynamics;
  GtkWidget *inner_vbox;
  GtkWidget *expander;
  gboolean   expanded;

  expander = gtk_expander_new (_("Stroke Effects"));
  g_object_get (config, "stroke-effects-expander", &expanded, NULL);

  inner_vbox = create_expanded_frame (outer_vbox, _("Stroke Effects"), expanded, 2, &expander);

  g_object_bind_property (config, "stroke-effects-expander",
                          expander, "expanded",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  /*  the "Build-Up" toggle renamed from "Incremental"  */
  if (tool_type == GIMP_TYPE_PENCIL_TOOL     ||
      tool_type == GIMP_TYPE_PAINTBRUSH_TOOL ||
      tool_type == GIMP_TYPE_ERASER_TOOL     ||
      tool_type == GIMP_TYPE_DODGE_BURN_TOOL)
    {
      button = gimp_prop_enum_check_button_new (config, "application-mode",
                                                NULL,
                                                GIMP_PAINT_CONSTANT,
                                                GIMP_PAINT_INCREMENTAL);
      gtk_box_pack_start (GTK_BOX (inner_vbox), button, FALSE, FALSE, 0);
    }

  /*  The "Smoothing" options  */
  if (g_type_is_a (tool_type, GIMP_TYPE_PAINT_TOOL))
    {
      frame = smoothing_options_gui (paint_options, tool_type);
      gtk_box_pack_start (GTK_BOX (inner_vbox), frame, FALSE, FALSE, 0);
      gtk_widget_set_visible (frame, TRUE);
    }

  dynamics_button = gimp_prop_check_button_new (config, "dynamics-enabled", NULL);
  gtk_box_pack_start (GTK_BOX (inner_vbox), dynamics_button, FALSE, FALSE, 0);

  select_dynamics = gimp_prop_dynamics_box_new (NULL,
                                                GIMP_CONTEXT (config),
                                                _("Dynamics"), 2,
                                                "dynamics-view-type",
                                                "dynamics-view-size",
                                                "gimp-dynamics-editor",
                                                _("Edit this dynamics"));
  gtk_box_pack_start (GTK_BOX (inner_vbox), select_dynamics, FALSE, FALSE, 0);

  multiply_button = gimp_prop_check_button_new (config, "brush-pressure-multiply", NULL);
  gtk_box_pack_start (GTK_BOX (inner_vbox), multiply_button, FALSE, FALSE, 0);

  /* the fade and colour option, hidden when dynamics is not active */
  frame = fade_colour_options_gui (paint_options, tool_type);
  gtk_box_pack_start (GTK_BOX (inner_vbox), frame, FALSE, FALSE, 0);
  gtk_widget_set_visible (frame, TRUE);

  g_object_bind_property (dynamics_button, "active",
                          frame, "visible",
                          G_BINDING_SYNC_CREATE);

  g_object_bind_property (dynamics_button, "active",
                          select_dynamics, "visible",
                          G_BINDING_SYNC_CREATE);

  g_object_bind_property (dynamics_button, "active",
                          multiply_button, "visible",
                          G_BINDING_SYNC_CREATE);

  return frame;
}

static GtkWidget *
create_fade_scale_unit_menu (GObject *config)
{
  GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
  GtkWidget *scale = gimp_prop_spin_scale_new(config, "fade-length", 1.0, 50.0, 0);
  GtkWidget *menu = gimp_prop_unit_combo_box_new(config, "fade-unit");

  gimp_spin_scale_set_scale_limits(GIMP_SPIN_SCALE(scale), 1.0, 1000.0);
  gtk_box_pack_start(GTK_BOX(hbox), scale, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), menu, FALSE, FALSE, 0);

  return hbox;
}

static GtkWidget *
fade_colour_options_gui (GimpPaintOptions *paint_options,
                         GType             tool_type)
{
  GObject   *config = G_OBJECT(paint_options);
  GtkWidget *expander;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *fade_scale_menu;
  GtkWidget *combo;
  GtkWidget *label;
  GtkWidget *gradient_box;

  expander = gtk_expander_new(_("Fade and Colour"));
  gtk_widget_set_visible(expander, TRUE);

  frame = gimp_frame_new(NULL);
  gtk_container_add(GTK_CONTAINER(expander), frame);
  gtk_widget_set_visible(frame, TRUE);

  vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
  gtk_container_add(GTK_CONTAINER(frame), vbox);
  gtk_widget_set_visible(vbox, TRUE);

  /* Fade scale and unit menu */
  fade_scale_menu = create_fade_scale_unit_menu(config);
  gtk_box_pack_start(GTK_BOX(vbox), fade_scale_menu, FALSE, FALSE, 0);
  gtk_widget_set_visible(fade_scale_menu, TRUE);

  /* Repeat type combo */
  combo = gimp_prop_enum_combo_box_new(config, "fade-repeat", 0, 0);
  gimp_int_combo_box_set_label(GIMP_INT_COMBO_BOX(combo), _("Repeat"));
  g_object_set(combo, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), combo, TRUE, TRUE, 0);

  /* Fade reverse and multiply checkboxes */
  add_checkbox_to_vbox(vbox, config, "fade-reverse");
  add_checkbox_to_vbox(vbox, config, "fade-multiply");

  /* Gradient color options if applicable */
  if (g_type_is_a(tool_type, GIMP_TYPE_PAINTBRUSH_TOOL) || tool_type == GIMP_TYPE_SMUDGE_TOOL)
    {
      label = gtk_label_new_with_mnemonic(_("Gradient Colour Options"));
      gimp_label_set_attributes(GTK_LABEL(label), PANGO_ATTR_WEIGHT, PANGO_WEIGHT_NORMAL, -1);
      gtk_label_set_xalign(GTK_LABEL(label), 0.0);
      gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
      gtk_widget_set_visible(label, TRUE);

      gradient_box = gimp_prop_gradient_box_new(NULL, GIMP_CONTEXT(config),
                                                _("Gradient"), 2, "gradient-view-type",
                                                "gradient-view-size", "gradient-reverse",
                                                "gradient-blend-color-space",
                                                "gimp-gradient-editor",
                                                _("Edit this gradient"));
      gtk_box_pack_start(GTK_BOX(vbox), gradient_box, FALSE, FALSE, 0);

      /* Blend color space combo */
      combo = gimp_prop_enum_combo_box_new(config, "gradient-blend-color-space", 0, 0);
      gimp_int_combo_box_set_label(GIMP_INT_COMBO_BOX(combo), _("Blend Color Space"));
      g_object_set(combo, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
      gtk_box_pack_start(GTK_BOX(vbox), combo, TRUE, TRUE, 0);
    }

  return expander;
}

static GtkWidget *
smoothing_options_gui (GimpPaintOptions *paint_options,
                       GType             tool_type)
{
  GObject   *config = G_OBJECT (paint_options);
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *scale;

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);

  frame = gimp_prop_expanding_frame_new (config, "use-smoothing", NULL,
                                         vbox, NULL);

  scale = gimp_prop_spin_scale_new (config, "sample-depth", 1, 16, 1);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  scale = gimp_prop_spin_scale_new (config, "position-smoothing", 1, 10, 1);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  scale = gimp_prop_spin_scale_new (config, "pressure-smoothing", 1, 10, 1);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  scale = gimp_prop_spin_scale_new (config, "direction-smoothing", 1, 10, 1);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  return frame;
}

static gboolean
is_control_left_click (GimpBrush      *brush,
                       GdkEventButton *event)
{
  if (brush && event->type == GDK_BUTTON_PRESS &&
      event->button == GDK_BUTTON_PRIMARY &&
      event->state & GDK_CONTROL_MASK)
    {
      return TRUE;
    }

  return FALSE;
}

static gboolean
paint_options_slider_reset_property (GtkWidget        *widget,
                                     GdkEventButton   *event,
                                     GimpPaintOptions *paint_options,
                                     void (*reset_func)(GimpPaintOptions *, GimpBrush *))
{
  GimpBrush *brush = gimp_context_get_brush (GIMP_CONTEXT (paint_options));

  if (is_control_left_click (brush, event) && brush)
    {
      reset_func(paint_options, brush);
      return TRUE;
    }

  return FALSE;
}

static gboolean
gimp_paint_options_slider_reset_size (GtkWidget      *widget,
                                      GdkEventButton *event,
                                      gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_size);
}

static gboolean
gimp_paint_options_slider_reset_ratio (GtkWidget      *widget,
                                       GdkEventButton *event,
                                       gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_aspect_ratio);
}

static gboolean
gimp_paint_options_slider_reset_angle (GtkWidget      *widget,
                                       GdkEventButton *event,
                                       gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_angle);
}

static gboolean
gimp_paint_options_slider_reset_spacing (GtkWidget      *widget,
                                         GdkEventButton *event,
                                         gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_spacing);
}

static gboolean
gimp_paint_options_slider_reset_hardness (GtkWidget      *widget,
                                          GdkEventButton *event,
                                          gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_hardness);
}

static gboolean
gimp_paint_options_slider_reset_jitter (GtkWidget      *widget,
                                        GdkEventButton *event,
                                        gpointer        user_data)
{
  return paint_options_slider_reset_property (widget, event,
                                              (GimpPaintOptions *) user_data,
                                              gimp_paint_options_set_default_brush_jitter);
}

static gboolean
gimp_paint_options_slider_reset_force (GtkWidget      *widget,
                                       GdkEventButton *event,
                                       gpointer        user_data)
{
  GimpPaintOptions *paint_options = (GimpPaintOptions *) user_data;
  GimpBrush        *brush         = gimp_context_get_brush (GIMP_CONTEXT (paint_options));

if (is_control_left_click (brush, event))
    {
      g_object_set (paint_options,
                    "brush-force", 0.5,
                    NULL);
      return TRUE;
    }

  return FALSE;
}

static void
paint_options_set_brush_default (GimpPaintOptions *paint_options,
                                 void             (*set_func)(GimpPaintOptions *, GimpBrush *))
{
  GimpBrush *brush = gimp_context_get_brush (GIMP_CONTEXT (paint_options));

  if (brush)
    set_func (paint_options, brush);
}

static void
gimp_paint_options_button_reset_size (GtkWidget *button,
                                      GimpPaintOptions *paint_options)
{
  paint_options_set_brush_default (paint_options,
                                   gimp_paint_options_set_default_brush_size);
}

static void
gimp_paint_options_button_reset_ratio (GtkWidget        *button,
                                       GimpPaintOptions *paint_options)
{
  paint_options_set_brush_default (paint_options,
                                   gimp_paint_options_set_default_brush_aspect_ratio);
}

static void
gimp_paint_options_button_reset_angle (GtkWidget        *button,
                                       GimpPaintOptions *paint_options)
{
  paint_options_set_brush_default (paint_options,
                                   gimp_paint_options_set_default_brush_angle);
}

static void
gimp_paint_options_button_reset_spacing (GtkWidget        *button,
                                         GimpPaintOptions *paint_options)
{
  paint_options_set_brush_default (paint_options,
                                   gimp_paint_options_set_default_brush_spacing);
}

static void
gimp_paint_options_button_reset_hardness (GtkWidget        *button,
                                          GimpPaintOptions *paint_options)
{
  paint_options_set_brush_default (paint_options,
                                   gimp_paint_options_set_default_brush_hardness);
}

static void
gimp_paint_options_button_reset_force (GtkWidget        *button,
                                       GimpPaintOptions *paint_options)
{
  g_object_set (paint_options,
                "brush-force", 0.5,
                NULL);
}

static void
gimp_paint_options_button_reset_jitter (GtkWidget        *button,
                                        GimpPaintOptions *paint_options)
{
  g_object_set (paint_options,
                "brush-jitter", 0.0,
                NULL);
}

static GtkWidget *
expand_options_gui (GimpPaintOptions *paint_options,
                    GType             tool_type)
{
  GObject   *config = G_OBJECT (paint_options);
  GtkWidget *frame;
  GtkWidget *scale;
  GtkWidget *combo_box;
  GtkWidget *vbox;

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);

  scale = gimp_prop_spin_scale_new (config, "expand-amount",
                                    1, 10, 2);
  gimp_spin_scale_set_constrain_drag (GIMP_SPIN_SCALE (scale), TRUE);

  gimp_spin_scale_set_scale_limits (GIMP_SPIN_SCALE (scale), 1.0, 1000.0);
  gimp_spin_scale_set_gamma (GIMP_SPIN_SCALE (scale), 1.0);
  gtk_box_pack_start (GTK_BOX (vbox), scale, FALSE, FALSE, 0);

  combo_box = gimp_prop_enum_combo_box_new (config, "expand-fill-type", 0, 0);
  gtk_box_pack_start (GTK_BOX (vbox), combo_box, FALSE, FALSE, 0);

  frame = gimp_prop_enum_radio_frame_new (config, "expand-mask-fill-type",
                                          _("Fill Layer Mask With"), 0, 1);
  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

  frame = gimp_prop_expanding_frame_new (config, "expand-use", NULL,
                                         vbox, NULL);

  return frame;
}

static GtkWidget *
create_scale_widget (GObject     *config,
                     const gchar *prop_name,
                     gdouble      step_increment,
                     gdouble      page_increment,
                     gint         digits,
                     gdouble      scale_min,
                     gdouble      scale_max,
                     gdouble      factor,
                     gdouble      gamma,
                     gboolean     constrain_drag)
{
  GimpToolOptions *tool_options = GIMP_TOOL_OPTIONS (config);
  GimpGuiConfig   *gui_config   = GIMP_GUI_CONFIG (tool_options->tool_info->gimp->config);
  GtkWidget       *scale;

  scale = gimp_prop_spin_scale_new (config, prop_name,
                                    step_increment, page_increment, digits);

  gimp_spin_scale_set_constrain_drag (GIMP_SPIN_SCALE (scale), constrain_drag);
  gimp_prop_widget_set_factor (scale, factor,
                               step_increment, page_increment, digits);
  gimp_spin_scale_set_scale_limits (GIMP_SPIN_SCALE (scale),
                                    scale_min, scale_max);
  gimp_spin_scale_set_gamma (GIMP_SPIN_SCALE (scale), gamma);
  gtk_widget_show (scale);

  if (! gui_config->show_tool_tips)
    {
      gtk_widget_set_tooltip_text (scale, NULL);
    }

  return scale;
}

static void
gimp_toggle_link_resource_button_update_icon (GtkToggleButton    *toggle_button,
                                              ResourceToggleData *toggle_data)
{
  gdouble      opacity;
  const gchar *help_text;
  const gchar *icon_name;

  if (gtk_toggle_button_get_active (toggle_button))
    {
      opacity   = 1.0;
      help_text = toggle_data->help_text_on;
      icon_name = toggle_data->icon_name_active;
    }
  else
    {
      opacity   = 0.45;
      help_text = toggle_data->help_text_off;
      icon_name = toggle_data->icon_name_inactive;
    }

  gtk_widget_set_opacity (toggle_data->image, opacity);
  gtk_image_set_from_icon_name (GTK_IMAGE (toggle_data->image),
                                icon_name,
                                GTK_ICON_SIZE_MENU);
  gimp_help_set_help_data (GTK_WIDGET (toggle_button), help_text, NULL);
}

static GtkWidget *
create_reset_button (GCallback    reset_button_callback,
                     GObject     *config,
                     const gchar *reset_tooltip)
{
  GtkWidget *button;

  button = gimp_icon_button_new (GIMP_ICON_RESET, NULL);
  gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
  gtk_image_set_from_icon_name (GTK_IMAGE (gtk_bin_get_child (GTK_BIN (button))),
                                GIMP_ICON_RESET, GTK_ICON_SIZE_MENU);
  g_signal_connect (button, "clicked", reset_button_callback, config);
  gimp_help_set_help_data (button, reset_tooltip, NULL);
  gtk_widget_show (button);

  return button;
}

static GtkWidget *
create_blank_toggle (ResourceToggleData *toggle_data)
{
  GtkWidget *resource_toggle = gtk_toggle_button_new ();

  gtk_button_set_relief (GTK_BUTTON (resource_toggle), GTK_RELIEF_NONE);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (resource_toggle), FALSE);
  gtk_widget_set_sensitive (resource_toggle, FALSE);
  gtk_widget_show (resource_toggle);

  // Manage memory for toggle_data
  g_object_set_data_full (G_OBJECT (resource_toggle), "toggle-data", toggle_data, g_free);

  return resource_toggle;
}

static GtkWidget *
create_resource_toggle (GObject            *config,
                        ResourceToggleData *toggle_data)
{
  GtkWidget *resource_toggle = gtk_toggle_button_new ();

  gtk_button_set_relief (GTK_BUTTON (resource_toggle), GTK_RELIEF_NONE);
  toggle_data->image = gtk_image_new_from_icon_name (toggle_data->icon_name_active,
                                                     GTK_ICON_SIZE_MENU);

  gtk_container_add (GTK_CONTAINER (resource_toggle), toggle_data->image);
  gtk_widget_show (toggle_data->image);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (resource_toggle), TRUE);

  // Connect the toggled signal with the appropriate update function
  g_object_bind_property (config, toggle_data->link_prop_name,
                          resource_toggle, "active",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  g_signal_connect (resource_toggle, "toggled",
                    G_CALLBACK (gimp_toggle_link_resource_button_update_icon),
                    toggle_data);

  gimp_toggle_link_resource_button_update_icon (GTK_TOGGLE_BUTTON (resource_toggle), toggle_data);

  gtk_widget_show (resource_toggle);

  // Manage memory for toggle_data
  g_object_set_data_full (G_OBJECT (resource_toggle), "toggle-data", toggle_data, g_free);

  return resource_toggle;
}

static GtkWidget *
create_link_resource_toggle (GObject     *config,
                             const gchar *link_prop_name)
{
  ResourceToggleData *toggle_data = g_new0 (ResourceToggleData, 1);

  if (link_prop_name)
    {
      toggle_data->icon_name_active   = GIMP_ICON_CHAIN_HORIZONTAL;
      toggle_data->icon_name_inactive = GIMP_ICON_CHAIN_HORIZONTAL_BROKEN;
      toggle_data->help_text_on  = _("Toggle <b>on</b>: Automatically updates with\n"
                                    "changes to parametric and writable active brushes.");
      toggle_data->help_text_off = _("Toggle <b>off</b>: No longer linked to\n"
                                    "changes in the active parametric brush.");
            toggle_data->link_prop_name     = link_prop_name;

      return create_resource_toggle (config, toggle_data);
    }
  else
    {
      return create_blank_toggle (toggle_data);
    }

  return NULL;
}

static GtkWidget *
gimp_paint_options_gui_scale_with_buttons (BrushSliderOptions *slider_options)
{
  GtkWidget       *hbox;
  GtkWidget       *scale;
  GtkWidget       *button;

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);

  /* Store the callback and user data on the config object */
  g_object_set_data (G_OBJECT (slider_options->config), "callback", slider_options->reset_slider_callback);
  g_object_set_data (G_OBJECT (slider_options->config), "user_data", slider_options->config);

  /* Create and configure scale widget */
  scale = create_scale_widget (slider_options->config, slider_options->prop_name,
                               slider_options->step_increment, slider_options->page_increment,
                               slider_options->digits, slider_options->scale_min,
                               slider_options->scale_max, slider_options->factor,
                               slider_options->gamma, slider_options->constrain_drag);

  gtk_box_pack_start (GTK_BOX (hbox), scale, TRUE, TRUE, 0);

  /* Clear the callback data */
  g_object_set_data (G_OBJECT (slider_options->config), "callback", NULL);
  g_object_set_data (G_OBJECT (slider_options->config), "user_data", NULL);

  if (slider_options->show_brush_reset)
    {
      button = create_reset_button (slider_options->reset_button_callback,
                                    slider_options->config, slider_options->reset_tooltip);
      gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
    }

  if (slider_options->show_brush_linked)
    {
      button = create_link_resource_toggle (slider_options->config,
                                            slider_options->link_prop_name);
      gtk_size_group_add_widget (slider_options->link_group, button);
      gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
    }

  return hbox;
}
