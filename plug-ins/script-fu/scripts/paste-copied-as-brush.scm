; GIMP - The GNU Image Manipulation Program
; Copyright (C) 1995 Spencer Kimball and Peter Mattis
;
; script-fu-paste-copied-as-brush
; Based on select-to-brush by Copyright (c) 1997 Adrian Likins
; Adapted by Mark Sweeney 2024;
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.


(define (script-fu-paste-copied-as-brush image drawables copy clipboard name filename spacing)

  (let* ((brush-image 0)
         (brush-draw 0)
         (type 0)
         (path 0)
         (active-layer (vector-ref drawables 0))
         (valid_source #t)
         )

    (when (not (car (gimp-image-id-is-valid image)))
      (set! valid_source #f)
      (gimp-message _"Select a valid image to copy from")
    )

    (when (not(car (gimp-item-id-is-valid active-layer)))
      (set! valid_source #f)
      (gimp-message _"Select a valid layer to copy from")
    )

    (if (and valid_source (= copy 1) (= clipboard 0))
      (gimp-edit-copy 1 (vector active-layer))
    )

    (set! brush-image (car (gimp-edit-paste-as-new-image)))

    (if (= TRUE (car (gimp-image-id-is-valid brush-image)))
      (begin
        (set! brush-draw (aref (cadr (gimp-image-get-selected-drawables brush-image)) 0))
        (set! type (car (gimp-drawable-type brush-draw)))
        (set! path (string-append gimp-directory
                                  "/brushes/"
                                  filename
                                  (number->string brush-image)
                                  ".gbr"))

        (if (= type GRAYA-IMAGE)
            (begin
                (gimp-context-push)
                (gimp-context-set-background '(255 255 255))
                (set! brush-draw (car (gimp-image-flatten brush-image)))
                (gimp-context-pop)
            )
        )

        (file-gbr-export RUN-NONINTERACTIVE
                         brush-image
                         path
                         -1   ; NULL ExportOptions
                         spacing name)

        (gimp-image-delete brush-image)

        (gimp-brushes-refresh)
        (gimp-context-set-brush (car (gimp-brush-get-by-name name)))
      )
      (gimp-message _"There is no image data in the clipboard to paste.")
    )
  )
)

(script-fu-register-filter "script-fu-paste-copied-as-brush"
  _"Copy and Paste as New _Brush..."
  _"Copy and Paste the active selection into a new brush"
  "Michael Natterer <mitch@gimp.org>, adapted by Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2024"
  "*"
  SF-ONE-DRAWABLE
  SF-TOGGLE     _"Cop_y Selection" TRUE
  SF-TOGGLE     _"_Use Clipboard" FALSE
  SF-STRING     _"_Brush name" _"My Brush"
  SF-STRING     _"_File name"  _"mybrush"
  SF-ADJUSTMENT _"Spac_ing"    '(25 0 1000 1 2 1 0)
)

(script-fu-menu-register "script-fu-paste-copied-as-brush"
                         "<Brushes>/Brushes Menu"
)

