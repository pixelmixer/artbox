#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Create a new named layer from an active selection and name list
;;
;; A Script-Fu plugin that creates a new layer from an active selection
;;
;; Access this plugin from Layer > Layer from Selection
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; A list of common names used for layers, add more "stuff"
(define layer-names
  (list "new-layer #1" "construction" "eyes" "face" "neck" "hair" "pupils" "mouth"
        "brow" "nose" "fur" "jumper" "pants" "shorts" "dress" "skirt" "tongue"
        "shoes" "socks" "t-shirt" "highlight" "background" "sky" "ground" "stuff"))

;; =============================================================================
;; Main
(define (script-fu-new-layer-from-selection image
                                            drawables
                                            name-string
                                            name-index
                                            mode-index
                                            mask-index)
  (let* ((layer (ensure-layer drawables))
         (alist (create-alist-from-list layer-names))
         (name (if (string=? name-string "")
                 (get-string-from-alist alist name-index)
                 name-string))
         (blend-mode (cond ((= mode-index 0) LAYER-MODE-NORMAL)
                           ((= mode-index 1) LAYER-MODE-MULTIPLY)))
         (mask-mode (cond ((= mask-index 0) ADD-MASK-BLACK)
                          ((= mask-index 1) ADD-MASK-WHITE)
                          ((= mask-index 2) #f)))
         (new-layer 0))
    (gimp-context-push)
    (gimp-image-undo-group-start image)
    (set! new-layer (new-layer-from-selection image layer name blend-mode mask-mode))
    (when mask-mode (add-mask-to-layer new-layer mask-mode))
    (set-selected-layers image new-layer)
    (gimp-selection-none image)
    (gimp-displays-flush)
    (gimp-image-undo-group-end image)
    (gimp-context-pop)))

;; -----------------------------------------------------------------------------

(script-fu-register-filter
  "script-fu-new-layer-from-selection"
  "Layer From Selection"
  "Creates a new layer from the selection area size"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-DRAWABLE
  SF-STRING "_Named"           ""
  SF-OPTION "_Pick list"       layer-names
  SF-OPTION "La_yer Mode"      (list "normal" "multiply")
  SF-OPTION "_Mask Option"     (list "black mask" "white mask" "no mask"))

(script-fu-menu-register
  "script-fu-new-layer-from-selection"
  "<Image>/Layer")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define opaque 100)

(define warning #t)

;; Purpose: Ensures the given input is a vector.
;; Converts a list to a vector or wraps other single values in a vector if needed.
(define (ensure-vector input)
  (cond
    ;; If the input is already a vector, return it as-is
    ((vector? input)
     input)
    ;; If the input is a list, convert it to a vector
    ((list? input)
     (list->vector input))
    ;; If the input is null, return an empty vector
    ((null? input)
     (vector))
    ;; Otherwise, wrap the input in a vector
    (else
     (vector input))))

;; Purpose: Returns the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
    #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Check if an item is valid, returning #t or #f.
(define (item-is-valid? item)
  (if (number? item)
    (= (list->item (gimp-item-id-is-valid item)) 1)
    #f))

;; Purpose: Return the name of an item.
(define (item-get-name item)
  (if (not (= item 0))
    (if (item-is-valid? item)
      (list->item (gimp-item-get-name item))
      "invalid item")
    "root"))

;; Purpose: Returns the parent of an item if valid, otherwise returns #f
(define (item-get-parent item)
  (if (item-is-valid? item)
    (list->item (gimp-item-get-parent item))
    0))

;; Purpose: Returns the items tree position or #f if the item is invalid
(define (get-item-tree-position image item)
  (if (item-is-valid? item)
    (let ((position (list->item (gimp-image-get-item-position image item))))
      (debug-message "item : " (item-get-name item) " has tree position : " position)
      position)
    #f))

;; Purpose: Returns #t if the item is a layer, otherwise returns #f
(define (item-is-layer? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer item)))))

;; Purpose: Returns #t if the item is a mask, otherwise returns #f
(define (item-is-layer-mask? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer-mask item)))))

;; Purpose: Converts a bbox vector to a string in the format "top-left (x1,y1) bottom-right (x2,y2)"
(define (bbox->string bbox)
  (let* ((bbox-list (vector->list bbox))
         (x1 (list-ref bbox-list 0))
         (y1 (list-ref bbox-list 1))
         (x2 (list-ref bbox-list 2))
         (y2 (list-ref bbox-list 3)))
    (string-append "top-left (" (number->string x1) "," (number->string y1) ") "
                   "bottom-right (" (number->string x2) "," (number->string y2) ")")))

;; Purpose: Check if a predicate is true for all elements in a list
(define (every? pred list)
  (cond
    ((null? list) #t)                            ; Empty list is always true
    ((pred (car list)) (every? pred (cdr list))) ; Check rest of the list
    (else #f)))                                  ; Predicate failed for one element

;; Purpose: Returns #t (true) if the argument is an alist, otherwise #f
(define (alist? alist)
  (and (list? alist)               ; Must be a list
       (every? pair? alist)))      ; Every element must be a pair

;; Purpose: Return a string from an alist, returning #f if no match is found
;;          Guards against invalid alist input
(define (get-string-from-alist alist key)
  (if (alist? alist)
    (let ((entry (assoc key alist)))
      (if entry
        (cdr entry) ; Return the matched value
        #f))        ; No match found, return #f
  #f))              ; Return #f if alist is not valid

;; Purpose: Create an alist from a list by associating each element with its index
(define (create-alist-from-list list)
  (let loop ((list list) (index 0) (result '()))
    (if (null? list)
      (reverse result) ; Reverse the result to maintain the original order
      (loop (cdr list)
            (+ index 1)
            (cons (cons index (car list)) result))))) ; Add index and element to the result

;; Purpose: is the color a shade of gray?

;; Purpose: Get the active brush size

;; Purpose: Get the Eraser Tool active state

;; =============================================================================
;; Purpose: Layer Utilities for Script-Fu Plugins
;;
;; This module provides a collection of layer related functions
;; =============================================================================

(define (layer-create-mask layer type)
  (list->item (gimp-layer-create-mask layer type)))

;; Purpose: Returns a layers mask id or #f for no mask, guard against a #f layer
(define (layer-get-mask layer)
  (if (item-is-layer? layer)
    (let ((mask (list->item (gimp-layer-get-mask layer))))
      (if (> mask 0)
        mask
        #f))
    #f))

;; Purpose: Adds a mask of the specified type to a layer if it doesn't already have one.
;; Returns the mask (either existing or newly created).
;; Mask types:
;; - ADD-MASK-WHITE
;; - ADD-MASK-BLACK
;; - ADD-MASK-ALPHA
;; - ADD-MASK-ALPHA-TRANSFER
;; - ADD-MASK-SELECTION
;; - ADD-MASK-COPY
;; - ADD-MASK-CHANNEL
(define (add-mask-to-layer layer type)
  (let ((mask (layer-get-mask layer)))
    (if mask
      mask
      (let ((new-mask (layer-create-mask layer type)))
        (gimp-layer-add-mask layer new-mask)
        (debug-message "added mask to layer : " (item-get-name layer))
        new-mask)))) ;; Return the newly created mask

;; Purpose: Returns a layer if given a mask of a layer, a vector of layers,
;;          a list of layers or a single layer ID.
;;          Typical use is to filter the user selection of drawables to be a
;;          single layer and not a mask.
(define (ensure-layer item)
  (cond
    ;; Case 1: `item` is a vector or a list
    ((or (vector? item) (list? item))
     (let ((layer (first-item item)))
       (if (item-is-layer-mask? layer)
           (layer-from-mask layer)
           layer)))
    ;; Case 2: `item` is a single layer mask
    ((item-is-layer-mask? item)
     (layer-from-mask item))
    ;; Case 3: `item` is a single layer
    ((item-is-layer? item)
     item)
    ;; Default case: Return #f if it does not match the above.
    (else #f)))

;; Purpose: Returns a layers mask, otherwise returns #f
(define (layer-from-mask item)
  (if (item-is-valid? item)
    (let ((mask (list->item (gimp-layer-from-mask item))))
      (if (= mask -1)
        #f
        mask))
    #f))

;; Purpose: Returns a new layer
(define (layer-new image width height type name opacity mode)
  (list->item (gimp-layer-new image name width height type opacity mode)))

;; Purpose: Sets the selected layers of an image
(define (set-selected-layers image layers)
  (if (not (vector? layers))
    (gimp-image-set-selected-layers image (vector layers))
    (gimp-image-set-selected-layers image layers)))

;; Purpose: Returns an inserted a layer into the image with given parameters
(define (insert-layer image layer parent position off-x off-y mode)
  (gimp-layer-set-offsets layer off-x off-y)
  (gimp-image-insert-layer image layer parent position)
  (gimp-layer-set-composite-space layer mode)
  layer)

;; Purpose: Returns a new layer from a selection area or the src-layer size
;;          Positions above the src-layer in the layer stack
(define (new-layer-from-selection image src-layer name blend-mode)
  (let* ((bbox (image-selection-get-bbox image))
         (width (if bbox (bbox-width bbox) (drawable-get-width src-layer)))
         (height (if bbox (bbox-height bbox) (drawable-get-height src-layer)))
         (new-layer (insert-layer image
                                  (layer-new image width height RGBA-IMAGE name
                                             opaque blend-mode)
                                  (item-get-parent src-layer)
                                  (get-item-tree-position image src-layer)
                                  (if bbox (bbox-off-x bbox) (drawable-get-offset-x src-layer))
                                  (if bbox (bbox-off-y bbox) (drawable-get-offset-y src-layer))
                                  LAYER-COLOR-SPACE-RGB-PERCEPTUAL)))
         new-layer))

;; Purpose: Get the x offset of the drawable from the origin
(define (drawable-get-offset-x layer)
  (first-item (gimp-drawable-get-offsets layer)))

;; Purpose: Get the y offset of the drawable from the origin
(define (drawable-get-offset-y layer)
  (second-item (gimp-drawable-get-offsets layer)))

;; Purpose: Get the width of the drawable
(define (drawable-get-width layer)
  (list->item (gimp-drawable-get-width layer)))

;; Purpose: Get the height of the drawable
(define (drawable-get-height layer)
  (list->item (gimp-drawable-get-height layer)))

;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Purpose: Get the bounding box of the active image selection or return #f (false)
;;          if the selection is empty, the bounding box is a vector of four points.
;;          upper left, upper right, lower left, lower right.
(define (image-selection-get-bbox image)
  (let ((bounds (gimp-selection-bounds image)))
    (if (= (first-item bounds) 0)
      (begin
        (warning-message "image has no active selection, no bbox!")
        #f)
      (begin
        (let ((bbox (list->vector (list-remove-head bounds))))
          (debug-message "bbox of active selection: " (bbox->string bbox))
          bbox)))))

;; Purpose: Return the width of the bounding box.
(define (bbox-width bbox-in)
  (let ((bbox (ensure-vector bbox-in)))
    (- (vector-ref bbox 2) (vector-ref bbox 0))))

;; Purpose: Return the height of the bounding box.
(define (bbox-height bbox-in)
  (let ((bbox (ensure-vector bbox-in)))
    (- (vector-ref bbox 3) (vector-ref bbox 1))))

;; Purpose: Return the x offset of the bounding box.
(define (bbox-off-x bbox-in)
  (let ((bbox (ensure-vector bbox-in)))
    (vector-ref bbox 0)))

;; Purpose: Return the y offset of the bounding box.
(define (bbox-off-y bbox-in)
  (let ((bbox (ensure-vector bbox-in)))
    (vector-ref bbox 1)))

