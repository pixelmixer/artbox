#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Autosave plugin (almost)
;;
;; A plugin that automatically saves all open files incrementally, if the
;; content has changed. It provides functions to activate, deactivate,
;; check status, and reset autosave settings. The plugin uses global parasites
;; to store and manage autosave parameters across sessions.
;;
;; Access this plugin from File -> Almost Autosave -> Activate
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global Variables
(define debug #f)
(define debug-time #f)
(define debug-time-delay 0.25)
(define save-index 1)
(define check-status-every 2)

;; Purpose: Populate the global `config` alist with the GUI parameters
(define (get-and-set-autosave-settings)
  (set! config
    `((save-every-minutes . ,(get-attr "global-aa-save-every-minutes"))
      (max-saves          . ,(get-attr "global-aa-max-saves"))
      (dialog-save-path   . ,(get-attr "global-aa-path-set-in-dialog"))
      (dir-name           . ,(get-attr "global-aa-dir-name"))))
  (debug-message "autosave-settings: " config))

;; =============================================================================
;; Main
(define (script-fu-almost-autosave)
  (attribute-attachment-mode-persist)
  (when (autosave-is-ready-to-activate?)
    (autosave-activate))
  (when (autosave-is-active?)
    (display-autosave-is-already-activated-message))
  (when (autosave-has-not-been-set-up)
    (set-up-autosave)))

;; -----------------------------------------------------------------------------

(script-fu-register
  "script-fu-almost-autosave"
  "Activate"
  "Saves all open files incrementally, if the content has changed"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2024"
   "")

(script-fu-menu-register
  "script-fu-almost-autosave"
  "<Image>/File/Almost Autosave")

;; -----------------------------------------------------------------------------

(script-fu-register
  "script-fu-almost-autosave-off"
  "Deactivate"
  "Deactivate Almost Autosave"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2024"
  "")

(script-fu-menu-register
  "script-fu-almost-autosave-off"
  "<Image>/File/Almost Autosave")

;; -----------------------------------------------------------------------------

(script-fu-register
  "script-fu-almost-autosave-status"
  "Status"
  "Report the active Almost Autosave settings"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2024"
  "")

(script-fu-menu-register
  "script-fu-almost-autosave-status"
  "<Image>/File/Almost Autosave/")

;; -----------------------------------------------------------------------------

;; Register and add menu item "Reset"
(script-fu-register
  "script-fu-almost-autosave-clear"
  "Factory Reset"
  "Removes Almost Autosave Parasites"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2024"
  "")

(script-fu-menu-register
  "script-fu-almost-autosave-clear"
  "<Image>/File/Almost Autosave/Reset/")

;; =============================================================================
;; Local Functions

;; Purpose: Activates the autosave process, complicated by delayed deactivation
;;          Autosave is in a wait loop, and checks every N seconds for exit
;;          We check to see if it is waiting before activating. A waiting state
;;          can briefly exist after the autosave has be deactivated.
(define (autosave-activate)
  (when (autosave-is-not-waiting?)
    (set-activation-flags)
    (start-autosave-loop)))

;; Purpose: Main loop that handles periodic autosaving
(define (start-autosave-loop)
  (while (autosave-is-active?)
    (get-and-set-autosave-settings)
    (autosave-any-changed-images)
    (autosave-wait))
  (exit-autosave-loop))

;; Purpose: Deactivates the autosave process, if it is active
(define (script-fu-almost-autosave-off)
  (if (autosave-is-active?)
    (begin
      (remove-attr "global-aa-waiting")
      (remove-attr "global-aa-status"))
    (display-autosave-is-off-message)))

;; Purpose: Exits the autosave loop when deactivated
(define (exit-autosave-loop)
  (set-attr "global-aa-waiting" #f)
  (display-autosave-is-off-message))

;; Purpose: Waits for the specified time before the next autosave cycle,
(define (autosave-wait)
  (let ((total-wait-time (time-delay))
        (elapsed-time 0))
    (while (and (< elapsed-time total-wait-time) (autosave-is-active?))
      (when debug
        (message "Checking status... Elapsed time: " elapsed-time " seconds"))
      ;; Sleep for the check interval (convert seconds to microseconds)
      (usleep (* check-status-every 1000000))
      (set! elapsed-time (+ elapsed-time check-status-every)))
    ;; The wait is over
    (autosave-increment-save-index)))

;; Purpose: Helper for calculating the total wait time in seconds.
(define (time-delay)
  (* (if (or debug-time debug)
        debug-time-delay
        (get 'save-every-minutes))
    60))  ; Convert minutes to seconds

;; Purpose: Increments save-index, cycling back to 1 after reaching max-saves
(define (autosave-increment-save-index)
  (set! save-index (if (>= save-index (get 'max-saves)) 1 (+ save-index 1)))
  (debug-message "incremented save index: " save-index))

;; Purpose: Saves all changed images
(define (autosave-any-changed-images)
  (for-each
    (lambda (image)
      (when (is-image-valid-and-changed? image)
        (autosave-save-incremental image)))
    (image-get-open-list)))

;; Purpose: Saves the image incrementally
(define (autosave-save-incremental image)
  (let* ((image-base-name (image-get-base-name image))
         (autosave-path (autosave-get-path image image-base-name))
         (autosave-filename (autosave-get-filename autosave-path)))
    (debug-message "Image base name: " image-base-name)
    (debug-message "Autosave path: " autosave-path)
    (debug-message "Autosave filename: " autosave-filename)
    (debug-message "Creating directory: " autosave-path)
    (make-dir-path autosave-path)
    (debug-message "Saving image to: " autosave-filename)
    (gimp-xcf-save 0 image autosave-filename)))

;; Purpose: Constructs and returns the absolute autosave file name
(define (autosave-get-filename autosave-folder)

  (let ((filename (string-append autosave-folder "autosave_" (number->string save-index) ".xcf")))
    (debug-message "Constructing autosave filename: " filename)
    filename))

;; Purpose: Constructs and returns the autosave base path with debug messages
(define (autosave-get-path image image-base-name)
  (let* ((path (construct-path image image-base-name)))
    (debug-message "GUI Save Path: " (get 'dialog-save-path))
    (debug-message "GUI Folder   : " (get 'dir-name))
    (debug-message "Autosave path: " path)
    path))

;; Purpose: Appends together path parts to make the final autosave path
(define (construct-path image image-base-name)
  (let* ((path-root (string-append (get 'dialog-save-path) (get 'dir-name) "/" image-base-name)))
        (debug-message "image ID : " image "\nPath root: " path-root)
    (if (image-has-no-file image)
      (string-append path-root "_" (number->string image) "/")
      (string-append path-root  "/"))))

;; Purpose: Sets up autosave settings with a helper plugin
(define (set-up-autosave)
  (let ((save-every-minutes 15)
        (max-saves 3)
        (dir-name "Autosave")
        (save-path (if (equal? "/" DIR-SEPARATOR)
                    (getenv "HOME")
                    (getenv "HOMEPATH"))))
    (if (pdb-procedure-exists? "script-fu-almost-autosave-set-up")
      (script-fu-almost-autosave-set-up #:run-mode RUN-NONINTERACTIVE
                                        #:adjustment save-every-minutes
                                        #:adjustment-2 max-saves
                                        #:dirname save-path
                                        #:string dir-name)
      (message-box "\"script-fu-almost-autosave-set-up\" is missing...\n"))))

;; Purpose: Displays the current autosave status
(define (script-fu-almost-autosave-status)
  (get-and-set-autosave-settings)
  (if (autosave-is-active?)
    (display-status-message "On")
    (display-status-message "Off")))

;; Purpose: Displays the autosave status message
(define (display-status-message autosave-state)
  (message-box (concat "Almost Autosave is: " autosave-state
                       (if (autosave-is-set-up?)
                          (concat "\nSaving every "
                                  (get 'save-every-minutes) " minutes"
                                  "\nSaves "
                                  (get 'max-saves) " versions"
                                  "\nSaved in: "
                                  (get-autosave-folder-path) "/")
                          "\nAlmost Autosave has not been set up.")
                       (if (autosave-is-waiting?)
                          "\nAlmost Autosave is in a waiting state."
                          "\nAlmost Autosave is not in a waiting state."))))

(define (display-autosave-is-off-message)
  (message-box "Autosave is off."))

(define (display-autosave-is-activated-message)
  (message-box "Activated Autosave"))

(define (display-autosave-is-already-activated-message)
  (message-box "Autosave is already active."))

(define (display-autosave-reset-message)
  (message-box "Autosave has been reset."))

(define (get-autosave-folder-path)
  (concat (get 'dialog-save-path) (get 'dir-name)))

;; Purpose: Sets parasites that act as global state variables
(define (set-activation-flags)
  (attribute-attachment-mode-session)
  (set-attr "global-aa-status" #t)
  (set-attr "global-aa-waiting" #t)
  (attribute-attachment-mode-persist)
  (display-autosave-is-activated-message))

;; Purpose: Checks if autosave has been set up
(define (autosave-is-set-up?)
  (get-attr "global-aa-set-up"))

;; Purpose: Checks if autosave has not been set up
(define (autosave-has-not-been-set-up)
  (not (get-attr "global-aa-set-up")))

;; Purpose: Checks if autosave has been set up and is currently inactive
(define (autosave-is-ready-to-activate?)
  (and (autosave-is-set-up?) (autosave-inactive?)))

;; Purpose: Checks if autosave is active
(define (autosave-is-active?)
  (get-attr "global-aa-status"))

;; Purpose: Checks if the autosave while loop is running
(define (autosave-is-waiting?)
  (get-attr "global-aa-waiting"))

;; Purpose: Checks the autosave is not running in a while loop
(define (autosave-is-not-waiting?)
  (not (get-attr "global-aa-waiting")))

;; Purpose: Checks if the autosave deactivation completed
(define (autosave-inactive?)
  (not (autosave-is-waiting?)))

;; Purpose: Removes all autosave-related global parasites
(define (autosave-remove-globals)
  (remove-attr "global-aa-status")
  (remove-attr "global-aa-set-up")
  (remove-attr "global-aa-save-every-minutes")
  (remove-attr "global-aa-max-saves")
  (remove-attr "global-aa-path-set-in-dialog")
  (remove-attr "global-aa-dir-name")
  (remove-attr "global-aa-waiting")
  (display-autosave-reset-message))

;; Purpose: Script function to reset autosave settings
(define (script-fu-almost-autosave-clear)
  (autosave-remove-globals))

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define config '())

(define warning #t)

;; Purpose: Returns the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'caddr' function into a descriptive term.
;; Returns the third item of a list or vector. Warns if the input is invalid or too short.
(define (third-item collection)
  (cond
    ;; Handle non-empty lists with at least three elements
    ((and (list? collection) (> (length collection) 2))
     (list-ref collection 2))
    ;; Handle non-empty vectors with at least three elements
    ((and (vector? collection) (> (vector-length collection) 2))
     (vector-ref collection 2))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 2))
         (and (vector? collection) (<= (vector-length collection) 2)))
     (begin
       (warning-message "third-item: Collection has fewer than 3 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "third-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Retrieve the last element of a list or vector. Warns if the input is invalid or empty.
(define (last-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (if (null? (cdr collection))
         (car collection)  ;; Base case: only one element left, return it.
         (last-item (cdr collection))))  ;; Recursive call on the rest of the list.
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection (- (vector-length collection) 1)))
    ;; Warn if the collection is empty
    ((or (null? collection)
         (and (vector? collection) (<= (vector-length collection) 0)))
     (begin
       (warning-message "last-item: Collection is empty.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "last-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Return the list without the last item. Warns if input is not a valid list or has fewer than 2 elements.
(define (list-before-last-item list)
  (if (and (list? list) (not (null? list)))
    (if (> (length list) 1)
      (reverse (list-remove-head (reverse list)))
      (begin
        (warning-message "list-before-last-item: List has fewer than 2 elements.")
        '()))
    (begin
      (warning-message "list-before-last-item: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a (()) and handle simple edge cases. Warns if the input is not a list or does not match the pattern.
(define (lists->list list)
  (if (list? list)
    (if (and (list? (car list)) (null? (cdr list)))
      (car list)
      (begin
        (warning-message "lists->list: Input is not a single-element nested list, returning the original list.")
        list))
    (begin
      (warning-message "lists->list: Expected a list, but received: " list)
      #f)))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a message in a dialog box.
(define (message-box msg)
  (gimp-message-set-handler 0)
  (message msg "\n")
  (gimp-message-set-handler 2))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
    #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Returns the value from a global config alist pair if the parameter exists,
;;          otherwise returns a special "not found" marker.
(define (get parameter)
  (let ((result (assoc parameter config)))
    (if result
      (begin
        (when debug (message "> " parameter " : " (cdr result)))
        (cdr result))
        'not-found))) ;; Use a distinct value to indicate "not found"

;; Purpose: Return #t (true) if the given procedure exists
(define (pdb-procedure-exists? procedure-name)
  (= (list->item (gimp-pdb-procedure-exists procedure-name)) 1))
;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Returns #t if an image has a file, #f if untitled.
(define (image-has-file? image)
  (let ((file-path (list->item (gimp-image-get-file image))))
    ;; (debug-message "Checking if image has file: " file-path )
    (not (equal? file-path ""))))

;; Purpose: Returns #t if the image has no file, its never been saved.
(define (image-has-no-file image)
  (not (image-has-file? image)))

;; Purpose: Return the absolute file path of an image
(define (image-get-absolute-file-path image)
  (let ((file-path (list->item (gimp-image-get-file image))))
    ;; (debug-message "Absolute file path: " file-path)
    file-path))

;; Purpose: Retrieves the short file name (image.ext) of an image from its full path,
;;          or returns an empty string if no file is associated with the image.
(define (image-get-short-file-name image)
  (if (image-has-file? image)
    (begin
      (let* ((file-path (image-get-absolute-file-path image))
             (parts (strbreakup file-path DIR-SEPARATOR)))
        ;; (debug-message "Short file : " (last-item parts))
        (last-item parts)))
    (begin
      (debug-message "Image is untitled, returning empty string")
      "")))

;; Purpose: Retrieves the base name of an image (without the file extension)
;;          or returns an "Untitled" if no file is associated.
(define (image-get-base-name image)
  (if (image-has-file? image)
    (begin
      (let* ((file (image-get-absolute-file-path image))
             (short-name (image-get-short-file-name image))
             (parts (strbreakup short-name "."))
             (base-parts (if (null? (list-remove-head parts))
                            parts
                            (list-before-last-item parts))))
        ;; (debug-message "Base name (without extension): " base-parts)
        (string-join base-parts ".")))
    (begin
      (debug-message "Image is untitled")
      "Untitled")))

;; Purpose: Returns #t if the image is valid and changed (edited)
(define (is-image-valid-and-changed? image)
  (let ((valid? (> (list->item (gimp-image-id-is-valid image)) 0))
        (changed? (> (list->item (gimp-image-is-dirty image)) 0)))
    (and valid? changed?)))

;; Purpose: Returns a list of open images
(define (image-get-open-list)
  (vector->list (first-item (gimp-get-images))))

;; =============================================================================
;; Purpose: Parasite Management Functions
;;
;; This is a collection of functions designed to manage the attachment, retrieval,
;; and removal of parasite data. Parasites store custom data in different scopes
;; (global, image, or item). These functions handle setting, getting, and detaching
;; parasites, as well as serializing and deserializing data for storage.
;;
;; Additionally, helper functions are provided for working with parasite scopes,
;; validating data, and processing parasite information.
;; =============================================================================

;; Constants for Parasites
(define PARASITE_ATTACH_MODE_SESSION_NO_UNDO 0)
(define PARASITE_ATTACH_MODE_PERSIST_NO_UNDO 1)
(define PARASITE_ATTACH_MODE_SESSION_WITH_UNDO 2)
(define PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO 3)

;; Globals for Parasites
(define parasite-attach-mode PARASITE_ATTACH_MODE_SESSION_NO_UNDO)

;; Purpose: Plug-in data in the form of parasites will be lost after exit
(define (attribute-attachment-mode-session)
  (context-set-parasite-attach-mode 'session #f))

;; Purpose: Plug-in data in the form of parasites will persist between sessions
(define (attribute-attachment-mode-persist)
  (context-set-parasite-attach-mode 'persist #f))

;; Purpose: Sets the parasite attach mode based on the provided mode (session or persist)
;;          and whether undo is enabled. Ensures that a valid symbol is passed for mode.
(define (context-set-parasite-attach-mode mode undo-attach)
  (cond
    ((parasite-attach-mode-invalid? mode)
     (warning-message "Invalid attach mode. Expected 'session or 'persist."))
    ((eq? mode 'session)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_SESSION_WITH_UNDO
                                    PARASITE_ATTACH_MODE_SESSION_NO_UNDO)) #t)
    ((eq? mode 'persist)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO
                                    PARASITE_ATTACH_MODE_PERSIST_NO_UNDO)) #t)))

;; Purpose: Checks a correct attachment mode has been used
(define (parasite-attach-mode-invalid? mode)
  (not (or (eq? mode 'session) (eq? mode 'persist))))

;; Purpose: Sets the data stored in a parasite.
;; (set-attr "global-exampleNumber" 123)
;; (set-attr "image-exampleBoolean" image #t)
(define (set-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let* ((data-element (last-item parasite-info))
            (string-data (serialize-parasite-data data-element))
            (parasite-name (first-item parasite-info))
            (image-or-item (get-image-or-item parasite-info))
            (scope (determine-parasite-scope parasite-name image-or-item)))

        (attach-parasite scope parasite-name image-or-item string-data)))
    #f))

;; Purpose: Retrieves the data stored in a parasite.
;; (get-attr "global-exampleNumber")  ; Returns 123
;; (get-attr "image-exampleBoolean" image) ; Returns #t
(define (get-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let ((parasite-data (retrieve-parasite-data parasite-info)))
      (if parasite-data
        (process-parasite-data (first-item parasite-info) parasite-data)
        #f)))
    #f))

;; Purpose: Returns a single list of all the global parasites
(define (global-get-parasite-list)
  (lists->list (gimp-get-parasite-list)))
;; Purpose: Returns a single list of all the image parasites
(define (image-get-parasite-list image)
  (lists->list (gimp-image-get-parasite-list image)))

;; Purpose: Returns a single list of all the item parasites
(define (item-get-parasite-list item)
  (lists->list (gimp-item-get-parasite-list item)))

;; Purpose: Get a single list containing the named global parasite
(define (global-get-parasite parasite-name)
  (lists->list (gimp-get-parasite parasite-name)))

;; Purpose: Get a single list containing the named image parasite
(define (image-get-parasite image parasite-name)
  (lists->list (gimp-image-get-parasite image parasite-name)))

;; Purpose: Get a single list containing the named item parasite
(define (item-get-parasite item parasite-name)
  (lists->list (gimp-item-get-parasite item parasite-name)))

;; Purpose: Get a global parasite data string
(define (global-get-parasite-data parasite-name)
  (third-item (global-get-parasite parasite-name)))

;; Purpose: Get the image parasite data string
(define (image-get-parasite-data image parasite-name)
  (third-item (image-get-parasite image parasite-name)))

;; Purpose: Get a the item parasite data string
(define (item-get-parasite-data item parasite-name)
  (third-item (item-get-parasite item parasite-name)))

;; Purpose: Check that the custom parasite format is used in the arguments
(define (validate-parasite-info parasite-info)
  (if (or (not (list? parasite-info))
          (null? parasite-info)
          (not (string? (first-item parasite-info))))
    (warning-message "validate-parasite-info: parasite-info must be a non-empty
                    list with a string parasite name")))

;; Purpose: Attaches the parasite to the appropriate scope.
(define (attach-parasite scope parasite-name image-or-item data)
  (cond
    ((equal? scope "global")
       (gimp-attach-parasite (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "image")
       (gimp-image-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "item")
       (gimp-item-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    (else
       (warning-message "attach-parasite: Invalid scope provided.\n"
                        "Expected 'global', 'image', or 'item'."))))

;; Purpose: Extracts the image or item from parasite-info, defaults to #f if missing.
(define (get-image-or-item parasite-info)
  (if (> (length parasite-info) 1)
      (second-item parasite-info)
      #f))

;; Purpose: Determines the scope of the parasite based on its name and image-or-item.
(define (determine-parasite-scope parasite-name image-or-item)
  (let ((scope (first-item (strbreakup parasite-name "-"))))
    (cond
      ((or (equal? scope "image") (equal? scope "item"))
       (if image-or-item
           scope
           (warning-message "determine-parasite-scope: Extra parameter is
                             required for image/item scope.")))
      ((equal? scope "global")
       scope)
      (else
       (warning-message "determine-parasite-scope: Unknown scope.")))))

;; Purpose: Converts native data types to their string representations for storage.
(define (serialize-parasite-data data)
  (cond
    ((string? data) data)
    ((number? data) (number->string data))
    ((boolean? data) (if data "#t" "#f"))
    (else
     (warning-message "serialize-parasite-data: Unsupported data type."))))

;; Purpose: Identifies the type of the parasite data.
(define (detect-parasite-type parasite-data)
  (cond
    ((not (equal? (string->number parasite-data) #f)) "number")
    ((or (equal? parasite-data "#t") (equal? parasite-data "#f")) "boolean")
    (else "string")))

;; Purpose: Converts serialized data back to its native type based on detected type.
(define (get-parasite-return-value data-type parasite-data)
  (cond
    ((equal? data-type "number") (string->number parasite-data))
    ((equal? data-type "boolean") (equal? parasite-data "#t"))
    ((equal? data-type "string") parasite-data)
    (else (warning-message "get-parasite-return-value: Invalid data type."))))

;; Purpose: Retrieves the parasite data based on paraItem, returns #f if the
;;          parasite data is not found
(define (retrieve-parasite-data parasite-info)
  (let* ((parasite-name (first-item parasite-info))
         (image-or-item (get-image-or-item parasite-info))
         (scope (determine-parasite-scope parasite-name image-or-item))
         (parasite-list (get-parasite-list-from-scope scope image-or-item))
         (parasite-data (find-parasite-data parasite-name scope image-or-item parasite-list)))
    (if parasite-data parasite-data #f)))

;; Purpose: Searches for parasite data in the parasite list.
(define (find-parasite-data parasite-name scope image-or-item parasite-list)
  (cond
    ((null? parasite-list)
     (debug-message "Parasite not found: " parasite-name)
     #f)
    ((equal? (first-item parasite-list) parasite-name)
     (get-parasite-data-from-scope scope parasite-name image-or-item))
    (else
     (find-parasite-data parasite-name scope image-or-item (list-remove-head parasite-list)))))

;; Purpose: Retrieves parasite data based on its scope.
(define (get-parasite-data-from-scope scope parasite-name image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-data parasite-name))
    ((equal? scope "image") (image-get-parasite-data image-or-item parasite-name))
    ((equal? scope "item") (item-get-parasite-data image-or-item parasite-name))
    (else (warning-message "get-parasite-data-from-scope: Invalid scope."))))

;; Purpose: Retrieves the list of parasites based on scope and imageOrItem.
(define (get-parasite-list-from-scope scope image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-list))
    ((equal? scope "image") (image-get-parasite-list image-or-item))
    ((equal? scope "item") (item-get-parasite-list image-or-item))
    (else '())))

;; Purpose: Detects parasite data type and returns it in native form.
(define (process-parasite-data parasite-name parasite-data)
  (let* ((data-type (detect-parasite-type parasite-data))
         (return-value (get-parasite-return-value data-type parasite-data)))
    (debug-message parasite-name " : type : " data-type " : " return-value)
    return-value))

;; Purpose: Removes the specified parasite attribute.
;; (remove-attr "global-exampleNumber")
;; (remove-attr "image-exampleBoolean" image)
(define (remove-attr . parasite-info)
  (validate-parasite-info parasite-info)
  (let* ((parasite-name (first-item parasite-info))
         (image-or-item (get-image-or-item parasite-info))
         (scope (determine-parasite-scope parasite-name image-or-item)))
    ;; Detach the parasite based on scope
    (cond
      ((equal? scope "global")
       (gimp-detach-parasite parasite-name))
      ((equal? scope "image")
       (gimp-image-detach-parasite image-or-item parasite-name))
      ((equal? scope "item")
       (gimp-item-detach-parasite image-or-item parasite-name))
      (else
       (message "remove-attr: Invalid scope")))))

;; =============================================================================
;; Purpose: File and Directory Utilities for Script-Fu Plugins
;;
;; This module provides a collection of file related functions
;; =============================================================================

;; Purpose: Makes a directory in the "home" directory with a string "/path/like/this"
(define (make-dir-path path)
  (let* ((pathParts (cdr (strbreakup path "/")))  ; Skip the empty element ?
         (currentPath (string-append "/" (first-item pathParts)))) ; /home
    (debug-message "Initial path parts: " pathParts)
    ;; Create the rest of the directories step-by-step
    (for-each
     (lambda (part)
       (set! currentPath (string-append currentPath "/" part))
       (debug-message "Creating directory: " currentPath)
       (dir-make currentPath))
     (list-remove-head pathParts))))

