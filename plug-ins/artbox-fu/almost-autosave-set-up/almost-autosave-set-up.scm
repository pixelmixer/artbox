#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Helper for the Almost Autosave plugin
;;
;; A Script-Fu plugin that provides a GUI for configuring the Almost-Autosave feature.
;; Users can set parameters like autosave interval, number of saves, save path, and
;; folder name. These settings are stored as global parasites, allowing the autosave
;; plugin to access them.
;;
;; Access this plugin from File -> Almost Autosave -> Set Up
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; Main
(define (script-fu-almost-autosave-set-up save-every-minutes max-saves path dir-name)
  (let ((has-set-up? (has-attribute? "global-aa-set-up"))) ; prior state
  ;; User may not have set a path, default to the users home directory
  (when (path-is-invalid? path)
    (set! path (get-home-dir)))
  ;; If dir-name is empty, default to "Autosave"
  (when (folder-name-is-invalid? dir-name)
    (set! dir-name "Autosave"))
  (attribute-attachment-mode-persist)
  (set-attr "global-aa-set-up" #t)
  (set-attr "global-aa-save-every-minutes" save-every-minutes)
  (set-attr "global-aa-max-saves" max-saves)
  (set-attr "global-aa-path-set-in-dialog" path)
  (set-attr "global-aa-dir-name" (string-append "/" dir-name))
  (display-set-up-message has-set-up? path dir-name)))

;; -----------------------------------------------------------------------------

(script-fu-register
  "script-fu-almost-autosave-set-up"
  "Set Up"
  "Edit the Almost Autosave Settings"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  ""
  SF-ADJUSTMENT "Save E_very...(minutes)" (list 15 1 60 1 5 0 SF-SPINNER)
  SF-ADJUSTMENT "Nu_mber of Saves"        (list 3 1 20 1 10 0 SF-SPINNER)
  SF-DIRNAME    "Loca_tion of Storage"    ""
  SF-STRING     "Storage _Directory Name" "Autosave")

(script-fu-menu-register
  "script-fu-almost-autosave-set-up"
  "<Image>/File/Almost Autosave")

;; =============================================================================
;; Local Functions

;; Purpose: Displays a message after initial set up
(define (display-set-up-message has-set-up? path dir-name)
  (message-box (concat "Almost Autosave keeps files here:\n" path "/" dir-name
                       (if has-set-up?
                         "\n\n"
                         (begin
                           (concat "\n\nAfter this initial set up, select\n"
                                   "'Almost Autosave ► Activate'\n\n")))
                       "Set up values can be changed by\n"
                       "selecting 'Almost Autosave ► Set Up'")))

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Return the users home directory location
(define (get-home-dir)
  (if (equal? "/" DIR-SEPARATOR)
    (getenv "HOME")
    (getenv "HOMEPATH")))

;; Purpose: Returns the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'caddr' function into a descriptive term.
;; Returns the third item of a list or vector. Warns if the input is invalid or too short.
(define (third-item collection)
  (cond
    ;; Handle non-empty lists with at least three elements
    ((and (list? collection) (> (length collection) 2))
     (list-ref collection 2))
    ;; Handle non-empty vectors with at least three elements
    ((and (vector? collection) (> (vector-length collection) 2))
     (vector-ref collection 2))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 2))
         (and (vector? collection) (<= (vector-length collection) 2)))
     (begin
       (warning-message "third-item: Collection has fewer than 3 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "third-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Retrieve the last element of a list or vector. Warns if the input is invalid or empty.
(define (last-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (if (null? (cdr collection))
         (car collection)  ;; Base case: only one element left, return it.
         (last-item (cdr collection))))  ;; Recursive call on the rest of the list.
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection (- (vector-length collection) 1)))
    ;; Warn if the collection is empty
    ((or (null? collection)
         (and (vector? collection) (<= (vector-length collection) 0)))
     (begin
       (warning-message "last-item: Collection is empty.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "last-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a (()) and handle simple edge cases. Warns if the input is not a list or does not match the pattern.
(define (lists->list list)
  (if (list? list)
    (if (and (list? (car list)) (null? (cdr list)))
      (car list)
      (begin
        (warning-message "lists->list: Input is not a single-element nested list, returning the original list.")
        list))
    (begin
      (warning-message "lists->list: Expected a list, but received: " list)
      #f)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a message in a dialog box.
(define (message-box msg)
  (gimp-message-set-handler 0)
  (message msg "\n")
  (gimp-message-set-handler 2))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
    #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; =============================================================================
;; Purpose: Parasite Management Functions
;;
;; This is a collection of functions designed to manage the attachment, retrieval,
;; and removal of parasite data. Parasites store custom data in different scopes
;; (global, image, or item). These functions handle setting, getting, and detaching
;; parasites, as well as serializing and deserializing data for storage.
;;
;; Additionally, helper functions are provided for working with parasite scopes,
;; validating data, and processing parasite information.
;; =============================================================================

;; Constants for Parasites
(define PARASITE_ATTACH_MODE_SESSION_NO_UNDO 0)
(define PARASITE_ATTACH_MODE_PERSIST_NO_UNDO 1)
(define PARASITE_ATTACH_MODE_SESSION_WITH_UNDO 2)
(define PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO 3)

;; Globals for Parasites
(define parasite-attach-mode PARASITE_ATTACH_MODE_SESSION_NO_UNDO)

;; Purpose: Plug-in data in the form of parasites will persist between sessions
(define (attribute-attachment-mode-persist)
  (context-set-parasite-attach-mode 'persist #f))

;; Purpose: Sets the parasite attach mode based on the provided mode (session or persist)
;;          and whether undo is enabled. Ensures that a valid symbol is passed for mode.
(define (context-set-parasite-attach-mode mode undo-attach)
  (cond
    ((parasite-attach-mode-invalid? mode)
     (warning-message "Invalid attach mode. Expected 'session or 'persist."))
    ((eq? mode 'session)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_SESSION_WITH_UNDO
                                    PARASITE_ATTACH_MODE_SESSION_NO_UNDO)) #t)
    ((eq? mode 'persist)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO
                                    PARASITE_ATTACH_MODE_PERSIST_NO_UNDO)) #t)))

;; Purpose: Checks a correct attachment mode has been used
(define (parasite-attach-mode-invalid? mode)
  (not (or (eq? mode 'session) (eq? mode 'persist))))

;; Purpose: Returns #t if the parasite is found, otherwise #f
;; (has-attribute? "global-exampleNumber")
;; (has-attribute? "image-exampleBoolean" image)
(define (has-attribute? . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let ((parasite-data (retrieve-parasite-data parasite-info)))
      (if parasite-data #t #f)))
    #f))

;; Purpose: Sets the data stored in a parasite.
;; (set-attr "global-exampleNumber" 123)
;; (set-attr "image-exampleBoolean" image #t)
(define (set-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let* ((data-element (last-item parasite-info))
            (string-data (serialize-parasite-data data-element))
            (parasite-name (first-item parasite-info))
            (image-or-item (get-image-or-item parasite-info))
            (scope (determine-parasite-scope parasite-name image-or-item)))

        (attach-parasite scope parasite-name image-or-item string-data)))
    #f))

;; Purpose: Returns a single list of all the global parasites
(define (global-get-parasite-list)
  (lists->list (gimp-get-parasite-list)))
;; Purpose: Returns a single list of all the image parasites
(define (image-get-parasite-list image)
  (lists->list (gimp-image-get-parasite-list image)))

;; Purpose: Returns a single list of all the item parasites
(define (item-get-parasite-list item)
  (lists->list (gimp-item-get-parasite-list item)))

;; Purpose: Get a single list containing the named global parasite
(define (global-get-parasite parasite-name)
  (lists->list (gimp-get-parasite parasite-name)))

;; Purpose: Get a single list containing the named image parasite
(define (image-get-parasite image parasite-name)
  (lists->list (gimp-image-get-parasite image parasite-name)))

;; Purpose: Get a single list containing the named item parasite
(define (item-get-parasite item parasite-name)
  (lists->list (gimp-item-get-parasite item parasite-name)))

;; Purpose: Get a global parasite data string
(define (global-get-parasite-data parasite-name)
  (third-item (global-get-parasite parasite-name)))

;; Purpose: Get the image parasite data string
(define (image-get-parasite-data image parasite-name)
  (third-item (image-get-parasite image parasite-name)))

;; Purpose: Get a the item parasite data string
(define (item-get-parasite-data item parasite-name)
  (third-item (item-get-parasite item parasite-name)))

;; Purpose: Check that the custom parasite format is used in the arguments
(define (validate-parasite-info parasite-info)
  (if (or (not (list? parasite-info))
          (null? parasite-info)
          (not (string? (first-item parasite-info))))
    (warning-message "validate-parasite-info: parasite-info must be a non-empty
                    list with a string parasite name")))

;; Purpose: Attaches the parasite to the appropriate scope.
(define (attach-parasite scope parasite-name image-or-item data)
  (cond
    ((equal? scope "global")
       (gimp-attach-parasite (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "image")
       (gimp-image-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "item")
       (gimp-item-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    (else
       (warning-message "attach-parasite: Invalid scope provided.\n"
                        "Expected 'global', 'image', or 'item'."))))

;; Purpose: Extracts the image or item from parasite-info, defaults to #f if missing.
(define (get-image-or-item parasite-info)
  (if (> (length parasite-info) 1)
      (second-item parasite-info)
      #f))

;; Purpose: Determines the scope of the parasite based on its name and image-or-item.
(define (determine-parasite-scope parasite-name image-or-item)
  (let ((scope (first-item (strbreakup parasite-name "-"))))
    (cond
      ((or (equal? scope "image") (equal? scope "item"))
       (if image-or-item
           scope
           (warning-message "determine-parasite-scope: Extra parameter is
                             required for image/item scope.")))
      ((equal? scope "global")
       scope)
      (else
       (warning-message "determine-parasite-scope: Unknown scope.")))))

;; Purpose: Converts native data types to their string representations for storage.
(define (serialize-parasite-data data)
  (cond
    ((string? data) data)
    ((number? data) (number->string data))
    ((boolean? data) (if data "#t" "#f"))
    (else
     (warning-message "serialize-parasite-data: Unsupported data type."))))

;; Purpose: Retrieves the parasite data based on paraItem, returns #f if the
;;          parasite data is not found
(define (retrieve-parasite-data parasite-info)
  (let* ((parasite-name (first-item parasite-info))
         (image-or-item (get-image-or-item parasite-info))
         (scope (determine-parasite-scope parasite-name image-or-item))
         (parasite-list (get-parasite-list-from-scope scope image-or-item))
         (parasite-data (find-parasite-data parasite-name scope image-or-item parasite-list)))
    (if parasite-data parasite-data #f)))

;; Purpose: Searches for parasite data in the parasite list.
(define (find-parasite-data parasite-name scope image-or-item parasite-list)
  (cond
    ((null? parasite-list)
     (debug-message "Parasite not found: " parasite-name)
     #f)
    ((equal? (first-item parasite-list) parasite-name)
     (get-parasite-data-from-scope scope parasite-name image-or-item))
    (else
     (find-parasite-data parasite-name scope image-or-item (list-remove-head parasite-list)))))

;; Purpose: Retrieves parasite data based on its scope.
(define (get-parasite-data-from-scope scope parasite-name image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-data parasite-name))
    ((equal? scope "image") (image-get-parasite-data image-or-item parasite-name))
    ((equal? scope "item") (item-get-parasite-data image-or-item parasite-name))
    (else (warning-message "get-parasite-data-from-scope: Invalid scope."))))

;; Purpose: Retrieves the list of parasites based on scope and imageOrItem.
(define (get-parasite-list-from-scope scope image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-list))
    ((equal? scope "image") (image-get-parasite-list image-or-item))
    ((equal? scope "item") (item-get-parasite-list image-or-item))
    (else '())))

;; =============================================================================
;; Purpose: File and Directory Utilities for Script-Fu Plugins
;;
;; This module provides a collection of file related functions
;; =============================================================================

;; Purpose: Check if a string contains a specific character
(define (string-contains? str char)
  (let loop ((i 0))
    (cond
      ((>= i (string-length str)) #f)
      ((char=? (string-ref str i) (string-ref char 0)) #t)
      (else (loop (+ i 1))))))

;; Purpose: Check if any element in the list satisfies the predicate
(define (any? pred lst)
  (cond
    ((null? lst) #f)
    ((pred (car lst)) #t)
    (else (any? pred (cdr lst)))))

;; Purpose: Check if the path string is valid (non-empty, no invalid characters)
(define (path-is-valid? path)
  (let ((invalid-chars '("*" "?" "<" ">" "|")))  ;; invalid characters
    ;; Check if the path is empty or contains only whitespace
    (let ((trimmed-path (string-trim path)))
      (if (equal? trimmed-path "")
        (begin
          (debug-message "Path is empty or contains only whitespace.")
          #f)
        (if (any? (lambda (char) (string-contains? path char)) invalid-chars)
          (begin
            (debug-message "Path contains invalid characters.")
            #f)
          (begin
            #t))))))

;; Purpose: Check if the path string is invalid
(define (path-is-invalid? path)
  (not (path-is-valid? path)))

;; Purpose: Check if the folder name is valid (non-empty, no invalid characters)
(define (folder-name-is-valid? name)
  (let ((invalid-chars '("*" "?" "\"" "<" ">" "|" "/" "\\" ":")))  ;; invalid characters for folder names
    (debug-message "Original folder name: '" name "'")
    (let ((trimmed-name (string-trim name)))
      (if (equal? trimmed-name "")
        (begin
          (debug-message "Folder name is empty or contains only whitespace.")
          #f)
        ;; Check if the folder name contains any invalid characters
        (if (any? (lambda (char) (string-contains? trimmed-name char)) invalid-chars)
          (begin
            (debug-message "Folder name contains invalid characters.")
            #f)
          (begin
            #t))))))

;; Purpose: Check if the folder name is invalid
(define (folder-name-is-invalid? name)
  (not (folder-name-is-valid? name)))
