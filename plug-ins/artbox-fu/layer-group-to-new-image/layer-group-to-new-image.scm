#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Copies the active Layer Group to a new image
;;
;; A plug-in that takes a group of layers and creates a new image from it.
;; This new image can be edited without the performance issues of the full layer stack.
;; Access this plugin from Layers -> Layer Group to New Image
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global Variables
(define debug #f)

(define (script-fu-layer-group-to-new-image image drawables)
  (let ((layer (ensure-layer drawables)))
    (when (item-is-group? layer)
      (edit-copy layer)
      (let* ((isolated-image (edit-paste-as-new-image))
             (isolated-display (display-new isolated-image)))
        (gimp-image-resize-to-layers isolated-image)))))

;; -----------------------------------------------------------------------------

(script-fu-register-filter "script-fu-layer-group-to-new-image"
  "Layer Group to New Image"
  "Creates a seperate image for the active layer group"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-DRAWABLE)

(script-fu-menu-register
  "script-fu-layer-group-to-new-image"
  "<Image>/Layer")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Ensures the given input is a vector.
;; Converts a list to a vector or wraps other single values in a vector if needed.
(define (ensure-vector input)
  (cond
    ;; If the input is already a vector, return it as-is
    ((vector? input)
     input)
    ;; If the input is a list, convert it to a vector
    ((list? input)
     (list->vector input))
    ;; If the input is null, return an empty vector
    ((null? input)
     (vector))
    ;; Otherwise, wrap the input in a vector
    (else
     (vector input))))

;; Purpose: Returns the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
    #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Check if an item is valid, returning #t or #f.
(define (item-is-valid? item)
  (if (number? item)
    (= (list->item (gimp-item-id-is-valid item)) 1)
    #f))

;; Purpose: Return the name of an item.
(define (item-get-name item)
  (if (not (= item 0))
    (if (item-is-valid? item)
      (list->item (gimp-item-get-name item))
      "invalid item")
    "root"))

;; Purpose: Returns #t if the item is a group, otherwise returns #f
(define (item-is-group? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-is-group item)))))

;; Purpose: Returns #t if the item is a layer, otherwise returns #f
(define (item-is-layer? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer item)))))

;; Purpose: Returns #t if the item is a mask, otherwise returns #f
(define (item-is-layer-mask? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer-mask item)))))

;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Purpose: Returns an image pasted from the clipboard or #f if no image is created
(define (edit-paste-as-new-image)
  (let ((image (list->item (gimp-edit-paste-as-new-image))))
    (if (> image 0)
      image
      #f)))

;; Purpose: Returns a new display
(define (display-new image)
  (list->item (gimp-display-new image)))

;; =============================================================================
;; Purpose: Parasite Management Functions
;;
;; This is a collection of functions designed to manage the attachment, retrieval,
;; and removal of parasite data. Parasites store custom data in different scopes
;; (global, image, or item). These functions handle setting, getting, and detaching
;; parasites, as well as serializing and deserializing data for storage.
;;
;; Additionally, helper functions are provided for working with parasite scopes,
;; validating data, and processing parasite information.
;; =============================================================================

;; Constants for Parasites
(define PARASITE_ATTACH_MODE_SESSION_NO_UNDO 0)
(define PARASITE_ATTACH_MODE_PERSIST_NO_UNDO 1)
(define PARASITE_ATTACH_MODE_SESSION_WITH_UNDO 2)
(define PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO 3)

;; =============================================================================
;; Purpose: File and Directory Utilities for Script-Fu Plugins
;;
;; This module provides a collection of file related functions
;; =============================================================================

;; =============================================================================
;; Purpose: Layer Utilities for Script-Fu Plugins
;;
;; This module provides a collection of layer related functions
;; =============================================================================

;; Purpose: Returns a layer if given a mask of a layer, a vector of layers,
;;          a list of layers or a single layer ID.
;;          Typical use is to filter the user selection of drawables to be a
;;          single layer and not a mask.
(define (ensure-layer item)
  (cond
    ;; Case 1: `item` is a vector or a list
    ((or (vector? item) (list? item))
     (let ((layer (first-item item)))
       (if (item-is-layer-mask? layer)
           (layer-from-mask layer)
           layer)))
    ;; Case 2: `item` is a single layer mask
    ((item-is-layer-mask? item)
     (layer-from-mask item))
    ;; Case 3: `item` is a single layer
    ((item-is-layer? item)
     item)
    ;; Default case: Return #f if it does not match the above.
    (else #f)))

;; Purpose: Returns a layers mask, otherwise returns #f
(define (layer-from-mask item)
  (if (item-is-valid? item)
    (let ((mask (list->item (gimp-layer-from-mask item))))
      (if (= mask -1)
        #f
        mask))
    #f))

;; Purpose: Copies drawables to the buffer
(define (edit-copy src-drawable)
  (debug-message "edit-copy " (item-get-name src-drawable))
  (gimp-edit-copy (ensure-vector src-drawable)))

