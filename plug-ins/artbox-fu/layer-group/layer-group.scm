#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Group a set of selected layers
;;
;; A Script-Fu plugin that groups a set of selected layers in a particular way.
;;
;; Access this plugin from Layer > Stack > Group Layers
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; Main
(define (script-fu-layer-group image drawables)
  (let* ((root-items (get-root-items image drawables))
         (item (first-item root-items))
         (parent (item-get-parent item))
         (position (get-item-tree-position image item)))
    (gimp-image-undo-group-start image)
    (add-items-to-group image
                        root-items
                        (create-group-layer image "group" parent position))
    (gimp-image-undo-group-end image)))

;; -----------------------------------------------------------------------------

(script-fu-register-filter
  "script-fu-layer-group"
  "Group Selected Layers"
  "Groups the selected layers in a pass-through group"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-OR-MORE-DRAWABLE)

(script-fu-menu-register
  "script-fu-layer-group"
  "<Image>/Layer/Stack")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Ensures the given input is a list.
;; Converts a vector to a list or wraps other single values in a list if needed.
(define (ensure-list input)
  (cond
    ;; If the input is already a list, return it as-is
    ((list? input)
     input)
    ;; If the input is a vector, convert it to a list
    ((vector? input)
     (vector->list input))
    ;; If the input is null, return an empty list
    ((null? input)
     '())
    ;; Otherwise, wrap the input in a list
    (else
     (list input))))

;; Purpose: Returnss the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
  #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Check if an item is valid, returning #t or #f.
(define (item-is-valid? item)
  (if (number? item)
    (= (list->item (gimp-item-id-is-valid item)) 1)
    #f))

;; Purpose: Return the name of an item.
(define (item-get-name item)
  (if (not (= item 0))
    (if (item-is-valid? item)
      (list->item (gimp-item-get-name item))
      "invalid item")
    "root"))

;; Purpose: Returns the parent of an item if valid, otherwise returns #f
(define (item-get-parent item)
  (if (item-is-valid? item)
    (list->item (gimp-item-get-parent item))
    0))

;; Purpose: Returns the items tree position of #f if the item is invalid
(define (get-item-tree-position image item)
  (if (item-is-valid? item)
    (let ((position (list->item (gimp-image-get-item-position image item))))
      (debug-message "item : " (item-get-name item) " has tree position : " position)
      position)
    #f))

;; Purpose: Checks if any element in the list satisfies the given condition
(define (any-true? condition list)
  (cond
    ;; If the list is empty, return #f
    ((null? list) #f)
    ;; If the condition is true for the first element, return #t
    ((condition (first-item list)) #t)
    ;; Otherwise, recursively check the rest of the list
    (else (any-true? condition (list-remove-head list)))))

;; Purpose: is the color a shade of gray?
;; =============================================================================
;; Purpose: Layer Utilities for Script-Fu Plugins
;;
;; This module provides a collection of layer related functions
;; =============================================================================

;; Purpose: Returns a list containing the root items from a vector of tree items.
(define (get-root-items image items)
  (if (or (not (vector? items)) (zero? (vector-length items)))
    (begin
      (warning-message "get-root-items called with invalid or empty vector")
      '())  ;; Return empty vector for invalid input
    (let ((items-list (ensure-list items)))
      (let loop ((remaining-items items-list)
                 (root-items '()))
        (if (null? remaining-items)
          root-items  ;; Return accumulated root items as a vector
          (let* ((item (first-item remaining-items))
                  (all-item-parents (item-get-all-parents image item))
                  (item-has-parent
                  (any-true? (lambda (parent) (member parent items-list))
                             all-item-parents)))
            (if item-has-parent
              (begin
                (debug-message (item-get-name item) " is not a root item")
                (loop (list-remove-head remaining-items) root-items))  ;; Skip
              (loop (list-remove-head remaining-items)
                    (cons item root-items)))))))))  ;; Add root item to the list

;; Purpose: Returns a list of all parent IDs for a given item
(define (item-get-all-parents image item)
  (let loop ((parent (item-get-parent item))
             (parents '()))
    (cond
      ((eq? parent #f)
       (warning-message "Can't get parent of invalid item")
       parents)  ;; Return the accumulated list
      ((= parent -1)
       parents)  ;; Stop recursion at root (-1) and return the accumulated list
      (else
       (debug-message "item > " (item-get-name item) " has ancestor > "
                      (item-get-name parent))
       (loop (item-get-parent parent)
             (cons parent parents))))))

;; Purpose: Creates a group layer in the image with specified properties
;; and inserts it into the image hierarchy.
(define (create-group-layer image group-name parent position)
  (let ((group (list->item (gimp-group-layer-new image group-name))))
    (gimp-image-insert-layer image group parent position)
    (gimp-layer-set-mode group LAYER-MODE-PASS-THROUGH)
    (gimp-layer-set-composite-space group LAYER-COLOR-SPACE-RGB-PERCEPTUAL)
    group))

;; Purpose: Reorders items from a vector list into a specified group
(define (add-items-to-group image items group)
  (for-each
    (lambda (item)
      (when debug
        (debug-message "Adding " (item-get-name item) " to " (item-get-name group)))
      (gimp-image-reorder-item image item group 0))
    (ensure-list items)))

