#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Set the foreground color to the active layer's average color.
;;
;; This plugin calculates the average color of a layer's content and sets it as
;; the foreground color. It supports alpha and non-alpha layers, avoiding the need
;; to manually configure the Color Picker tool's mode (merged or non-merged) and
;; select a point on the layer.
;;
;; Assign this plugin to a shortcut to quickly sample and set the foreground
;; color based on the layer's averaged content, centered on its active area.
;;
;; Access this plugin from Tools -> Paint Tools -> Get Layer Color
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; The calling plug-in is a wrapper for a general function that gets the colour
(define (script-fu-get-layer-colour image drawables)
  (let ((sample-radius 256)
        (layer (ensure-layer drawables)))
    (gimp-context-set-foreground (get-layer-color-core image layer sample-radius))))

;; -----------------------------------------------------------------------------

(script-fu-register-filter "script-fu-get-layer-colour"
  "Get Layer Colour"
  "Samples a layer for its average colour"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-DRAWABLE)

(script-fu-menu-register
  "script-fu-get-layer-colour"
  "<Image>/Tools/Paint Tools")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define color-black (list 0 0 0))

(define warning #t)

;; Purpose: Returnss the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Return the first item of a list, for readability
(define (x-coord pixel-coords)
  (first-item pixel-coords))

;; Purpose: Return the first item of a list, for readability
(define (y-coord pixel-coords)
  (second-item pixel-coords))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'list-ref' function into a descriptive term.
;; Returns the Nth item of a list or vector (0-indexed). Warns if the input is invalid or too short.
(define (nth-item collection n)
  (cond
    ;; Handle valid lists with enough elements
    ((and (list? collection) (>= (length collection) (+ n 1)))
     (list-ref collection n))
    ;; Handle valid vectors with enough elements
    ((and (vector? collection) (> (vector-length collection) n))
     (vector-ref collection n))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (< (length collection) (+ n 1)))
         (and (vector? collection) (<= (vector-length collection) n)))
     (begin
       (warning-message "nth-item: Collection does not have enough elements for index " n)
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "nth-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a (()) and handle simple edge cases. Warns if the input is not a list or does not match the pattern.
(define (lists->list list)
  (if (list? list)
    (if (and (list? (car list)) (null? (cdr list)))
      (car list)
      (begin
        (warning-message "lists->list: Input is not a single-element nested list, returning the original list.")
        list))
    (begin
      (warning-message "lists->list: Expected a list, but received: " list)
      #f)))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
  #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Check if an item is valid, returning #t or #f.
(define (item-is-valid? item)
  (if (number? item)
    (= (list->item (gimp-item-id-is-valid item)) 1)
    #f))

;; Purpose: Return the name of an item.
(define (item-get-name item)
  (if (not (= item 0))
    (if (item-is-valid? item)
      (list->item (gimp-item-get-name item))
      "invalid item")
    "root"))

;; Purpose: Returns #t if the item is a group, otherwise returns #f
(define (item-is-group? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-is-group item)))))

;; Purpose: Returns #t if the item is a layer, otherwise returns #f
(define (item-is-layer? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer item)))))

;; Purpose: Returns #t if the item is a mask, otherwise returns #f
(define (item-is-layer-mask? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer-mask item)))))

;; Purpose: Returns #t if there is a selection
(define (has-a-selection? image)
  (= 0 (list->item (gimp-selection-is-empty image))))

;; Purpose: Converts a color list to a string in the format "(r,g,b,a)"
(define (color->string color)
  (string-append "(" (string-join (map number->string color) ",") ")"))

;; Purpose: Converts a bbox vector to a string in the format "top-left (x1,y1) bottom-right (x2,y2)"
(define (bbox->string bbox)
  (let* ((bbox-list (vector->list bbox))
         (x1 (list-ref bbox-list 0))
         (y1 (list-ref bbox-list 1))
         (x2 (list-ref bbox-list 2))
         (y2 (list-ref bbox-list 3)))
    (string-append "top-left (" (number->string x1) "," (number->string y1) ") "
                   "bottom-right (" (number->string x2) "," (number->string y2) ")")))

;; Purpose: Returns an average pixel colour sampled at a (px py) location
;;          gimp-image-pick-color expects image coordinates
(define (get-average-pixel-color image layer pixel-coords sample-radius)
  (let* ((sample-merged FALSE)
         (sample-average (if (> sample-radius 1) TRUE FALSE))
         (px (x-coord pixel-coords))
         (py (y-coord pixel-coords))
         ;;gimp-drawable-get-pixel
         (color (lists->list (gimp-image-pick-color image
                                                    (vector layer)
                                                    px
                                                    py
                                                    sample-merged
                                                    sample-average
                                                    sample-radius))))
    (debug-message "pixel pick layer : " (item-get-name layer))
    (debug-message "at pixel coords : " "(" px "," py ")")
    (debug-message "with sample radius : " sample-radius)
    (debug-message "average rgba : " (color->string color))
    color))

;; Purpose: sets a pixel to a colour, drawable-set-pixel expects layer-local coordinates
(define (drawable-set-pixel drawable pixel-coords color)
  (gimp-drawable-set-pixel drawable
                           (x-coord pixel-coords)
                           (y-coord pixel-coords)
                           color)
  (gimp-drawable-update drawable 0 0 (drawable-get-width drawable)
                                     (drawable-get-height drawable)))

;; Purpose: Returns the average colour at the mid-point of a drawable
(define (color-at-drawable-mid-point image drawable sample-radius)
  (let* ((mid-point (drawable-get-mid-point drawable)))
    (when (pixel-inside-image-bounds? image mid-point)
      (color-pick image drawable mid-point sample-radius))))

;; Purpose: Returns the color at (px, py), optionally draws a sample point,
;;          sets the foreground colour, and clears the selection.
(define (color-pick image drawable pixel-coords sample-radius)
  (let ((layer-px (- (x-coord pixel-coords) (drawable-get-offset-x drawable)))
        (layer-py (- (y-coord pixel-coords) (drawable-get-offset-y drawable)))
        (draw-debug #t))
    (when (and draw-debug (not (item-is-group? drawable)))
      (drawable-set-pixel drawable (list layer-px layer-py) color-black))
    (get-average-pixel-color image drawable pixel-coords sample-radius)))

;; Purpose: Returns the average colour at the mid-point of an alpha layers content
(define (color-at-content-mid-point image layer sample-radius)
  (let* ((bbox (image-selection-get-bbox image))
         (mid-point (get-content-mid-point image layer))
         (mid-x (x-coord mid-point))
         (mid-y (y-coord mid-point)))
    (color-pick image layer mid-point sample-radius)))

;; Purpose: is the color a shade of gray?
;; =============================================================================
;; Purpose: Layer Utilities for Script-Fu Plugins
;;
;; This module provides a collection of layer related functions
;; =============================================================================

;; Purpose: Returns a layer if given a mask of a layer, a vector of layers,
;;          a list of layers or a single layer ID.
;;          Typical use is to filter the user selection of drawables to be a
;;          single layer and not a mask.
(define (ensure-layer item)
  (cond
    ;; Case 1: `item` is a vector or a list
    ((or (vector? item) (list? item))
     (let ((layer (first-item item)))
       (if (item-is-layer-mask? layer)
           (layer-from-mask layer)
           layer)))
    ;; Case 2: `item` is a single layer mask
    ((item-is-layer-mask? item)
     (layer-from-mask item))
    ;; Case 3: `item` is a single layer
    ((item-is-layer? item)
     item)
    ;; Default case: Return #f if it does not match the above.
    (else #f)))

;; Purpose: Returns a layers mask, otherwise returns #f
(define (layer-from-mask item)
  (if (item-is-valid? item)
    (let ((mask (list->item (gimp-layer-from-mask item))))
      (if (= mask -1)
        #f
        mask))
    #f))

;; Purpose: Get the x offset of the drawable from the origin
(define (drawable-get-offset-x layer)
  (first-item (gimp-drawable-get-offsets layer)))

;; Purpose: Get the y offset of the drawable from the origin
(define (drawable-get-offset-y layer)
  (second-item (gimp-drawable-get-offsets layer)))

;; Purpose: Get the width of the drawable
(define (drawable-get-width layer)
  (list->item (gimp-drawable-get-width layer)))

;; Purpose: Get the height of the drawable
(define (drawable-get-height layer)
  (list->item (gimp-drawable-get-height layer)))

;; Purpose: Returns #t (true) if the layer has an alpha
(define (layer-has-alpha? layer)
  (if (> (list->item (gimp-drawable-has-alpha layer)) 0)
    (begin
      (debug-message "layer has alpha : " (item-get-name layer))
      #t)
    (begin
      (debug-message "layer has no alpha : " (item-get-name layer))
      #f)))

;; Purpose: Returns #t (true) if the layer has no alpha value to select, 0,0,0,0
;;          NOTE: gimp-image-select-item is slow on larger layers
(define (layer-has-selectable-alpha? image layer)
  (if (layer-has-alpha? layer)
    (begin
      (gimp-image-select-item image CHANNEL-OP-REPLACE layer)
      (has-a-selection? image))
    #f))

;; Purpose: Selects the alpha area of a layer, or inverted alpha,
(define (select-layer-outline image layer invert)
  (if (layer-has-selectable-alpha? image layer)
    (if invert
      (begin
        (gimp-image-select-item image CHANNEL-OP-REPLACE layer)
        (gimp-selection-invert image))
      (gimp-image-select-item image CHANNEL-OP-REPLACE layer))
    #f))

;; Purpose: Sets the foreground colour by sampling the active layer colour
;;          Handles alpha and non alpha layers
(define (get-layer-color-core image layer sample-radius)
  (if (layer-has-selectable-alpha? image layer)
    (color-at-content-mid-point image layer sample-radius)
    (color-at-drawable-mid-point image layer sample-radius)))

;; Purpose: Returns the mid-point of the content within the bounding box
(define (get-content-mid-point image layer)
  (let ((mid-point (list 0 0)))
  (select-layer-outline image layer #f)
  (set! mid-point (get-bounding-box-mid-point (image-selection-get-bbox image)))
  (gimp-selection-none image)
  mid-point))

;; Purpose: Returns the mid-point (x, y) of a drawable
(define (drawable-get-mid-point drawable)
  (let* ((off-x (drawable-get-offset-x drawable))
         (off-y (drawable-get-offset-y drawable))
         (mid-x (+ off-x (/ (drawable-get-width drawable) 2)))
         (mid-y (+ off-y (/ (drawable-get-height drawable) 2))))
    (list mid-x mid-y)))
;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Purpose: Returns the image width
(define (image-get-width image)
  (list->item (gimp-image-get-width image)))

;; Purpose: Returns the image height
(define (image-get-height image)
  (list->item (gimp-image-get-height image)))

;; Purpose: Get the bounding box of the active image selection or return #f (false)
;;          if the selection is empty, the bounding box is a vector of four points.
;;          upper left, upper right, lower left, lower right.
(define (image-selection-get-bbox image)
  (let ((bounds (gimp-selection-bounds image)))
    (if (= (first-item bounds) 0)
      (begin
        (warning-message "image has no active selection, no bbox!")
        #f)
      (begin
        (let ((bbox (list->vector (list-remove-head bounds))))
          (debug-message "bbox of active selection: " (bbox->string bbox))
          bbox)))))

;; Purpose: Returns the mid-point of a bounding box vector
(define (get-bounding-box-mid-point bbox)
  (let* ((mid-x (/ (+ (nth-item bbox 0) (nth-item bbox 2)) 2))
         (mid-y (/ (+ (nth-item bbox 1) (nth-item bbox 3)) 2)))
    (debug-message "bounding box mid-point : (" mid-x "," mid-y ")")
    (list mid-x mid-y)))

;; Purpose: Returns #t (true) if the given pixel (list x y) is inside the image bounds
(define (pixel-inside-image-bounds? image pixel)
  (let* ((x (first-item pixel))
         (y (second-item pixel))
         (width  (image-get-width image))
         (height (image-get-height image)))
    (and (>= x 0) (< x width)
         (>= y 0) (< y height))))
