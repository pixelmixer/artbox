#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Renames all the layers in the stack, typically for animated brushes
;;
;; A Script-Fu plugin renames all the layers in the stack, typically for
;; renaming animated brush layers.
;;
;; Access this plugin from Layer > Stack > Rename Layers
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; Main
(define (script-fu-rename-layers image drawables name all-layers)
  (let ((layers (if (is-true? all-layers)
                  (get-layers-recursive (image-get-layer-list image) #t #t)
                  (get-layers-recursive (ensure-list drawables) #t #t))))
    (gimp-image-undo-group-start image)
    (rename-layer-list layers name)
    (gimp-image-undo-group-end image)))

;; -----------------------------------------------------------------------------

(script-fu-register-filter "script-fu-rename-layers"
  "Rename Layers"
  "Renames all the layers in the stack, or the selected layers"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-OR-MORE-DRAWABLE
  SF-STRING "Na_me"              ""
  SF-TOGGLE "Rename all la_yers" 1)

(script-fu-menu-register
  "script-fu-rename-layers"
  "<Image>/Layer/Stack")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Ensures the given input is a list.
;; Converts a vector to a list or wraps other single values in a list if needed.
(define (ensure-list input)
  (cond
    ;; If the input is already a list, return it as-is
    ((list? input)
     input)
    ;; If the input is a vector, convert it to a list
    ((vector? input)
     (vector->list input))
    ;; If the input is null, return an empty list
    ((null? input)
     '())
    ;; Otherwise, wrap the input in a list
    (else
     (list input))))

;; Purpose: Returnss the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
  #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Check if an item is valid, returning #t or #f.
(define (item-is-valid? item)
  (if (number? item)
    (= (list->item (gimp-item-id-is-valid item)) 1)
    #f))

;; Purpose: Returns #t if the item is a group, otherwise returns #f
(define (item-is-group? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-is-group item)))))

;; Purpose: Returns #t if the item is a layer, otherwise returns #f
(define (item-is-layer? item)
  (and (item-is-valid? item)
       (= 1 (list->item (gimp-item-id-is-layer item)))))

;; Purpose: Returns #t for non-zero values, otherwise #f
(define (is-true? value)
  (not (= value 0)))

;; Purpose: is the color a shade of gray?
;; =============================================================================
;; Purpose: Layer Utilities for Script-Fu Plugins
;;
;; This module provides a collection of layer related functions
;; =============================================================================

;; Purpose: Renames all the given layers
(define (rename-layer-list lstL name)
  (let ((layers (if (vector? lstL)
                  (vector->list lstL)
                  lstL)))
    ;; Initialize the naming base with " #1"
    (let ((name-index (string-append name " #1")))
      (for-each
        (lambda (layer)
          (when (item-is-layer? layer)
            (gimp-item-set-name layer name-index)))
        layers))))

;; Returns the base level layers of an item as a vector-list or empty list
(define (item-get-children item)
  (if (item-is-group? item)
    (list->item (gimp-item-get-children item))
    '()))

;; Helper function to recursively collect layers into an accumulated list of layers
;; For each layer, collect its children, then continue with its siblings.
(define (collect-layers layers acc-layers-list include-groups)
  (if (null? layers)
    acc-layers-list
    (let ((current-layer (car layers)))
      (if (item-is-group? current-layer)
        (collect-layers (cdr layers)
                        (collect-layers (vector->list (item-get-children current-layer))
                                        (if include-groups
                                          (cons current-layer acc-layers-list)
                                          acc-layers-list)
                                        include-groups)
                        include-groups)
        (collect-layers (cdr layers)
                        (cons current-layer acc-layers-list)
                        include-groups)))))

;; Purpose: Return all layers including groups and sub-layers from a list or
;;          vector of layers. Optionally reverse list order and include groups.
(define (get-layers-recursive layers reverse-order include-groups)
  (let ((initial-layers (ensure-list layers)))
    (if (null? initial-layers)
      '()
      ;; Recursively collect all layers
      (let ((collected-layers (collect-layers initial-layers '() include-groups)))
        (if reverse-order
          (reverse collected-layers)
          collected-layers)))))

;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Purpose: Returns a list of layers for the image
(define (image-get-layer-list image)
  (vector->list (first-item (gimp-image-get-layers image))))

