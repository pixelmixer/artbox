#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Incremental save plugin
;;
;; A Script-Fu plugin that saves an image incrementally to a sub-folder.
;; It does not replace the original save, it's purpose is to provide backup saves.
;; Each image can be given a different maximum number of saves using the helper
;; plugin, File -> Incremental Save -> Set Wrap Number
;;
;; Access this plugin from File -> Incremental Save -> Save as Increment
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; Main
(define (script-fu-incremental-save image drawables)
  (attribute-attachment-mode-persist)
  (check-image-has-file image)
  (save-image-as-increment image))

;; -----------------------------------------------------------------------------

(script-fu-register-filter
  "script-fu-incremental-save"
  "Save as Increment"
  "Saves the file incrementally, wrapping around after a set number"
  "Mark Sweeney"
  "Under GNU GENERAL PUBLIC LICENSE Version 3"
  "2025"
  "*"
  SF-ONE-OR-MORE-DRAWABLE)

(script-fu-menu-register
  "script-fu-incremental-save"
  "<Image>/File/Incremental Save")

;; =============================================================================
;; Local Functions

(define (check-image-has-file image)
  (when (image-has-no-file image)
    (exit-message "Please save the file before\nusing the Incremental Save")))

(define (save-image-as-increment image)
  (increment-current-save image)
  (message "Saving: " (construct-save-name image))
  (gimp-xcf-save 0 image (construct-save-name image)))

;; Purpose: Increments the current save number for an image.
(define (increment-current-save image)
  (let* ((current-save (get-current-save-index image))
         (max-saves (get-max-incremental-saves image))
         (final-save (wrap-around-increment current-save max-saves)))
    (store-current-save-index image final-save)))

;; Purpose: Constructs the complete save name and ensures the save directory exists.
(define (construct-save-name image)
  (let* ((base-name (image-get-base-name image))
         (file-path (image-get-file-location image))
         (save-directory (sub-folder-name file-path base-name))
         (save-name (construct-path image file-path base-name)))
    ;; Ensure the save directory exists
    (make-dir-path save-directory)
    (debug-message "save name: " save-name)
    save-name))

;; Purpose: Constructs the full save file path
(define (construct-path image file-path base-name )
  (string-append (sub-folder-name file-path base-name) "/" base-name "_"
                 (number->string (get-attr "image-current-incremental-save-index" image))
                 ".xcf"))

(define (get-current-save-index image)
  (if (has-attribute? "image-current-incremental-save-index" image)
    (get-attr "image-current-incremental-save-index" image)
    1))

(define (store-current-save-index image index)
  (debug-message "current save increment: " index)
  (set-attr "image-current-incremental-save-index" image index))

(define (get-max-incremental-saves image)
  (let ((default-max-saves 6))
    (if (has-attribute? "image-incremental-max-saves" image)
      (get-attr "image-incremental-max-saves" image)
      (begin
        (set-attr "image-incremental-max-saves" image default-max-saves)
        default-max-saves))))

(define (sub-folder-name file-path base-name)
  (string-append file-path base-name "_saves"))

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Returns the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'caddr' function into a descriptive term.
;; Returns the third item of a list or vector. Warns if the input is invalid or too short.
(define (third-item collection)
  (cond
    ;; Handle non-empty lists with at least three elements
    ((and (list? collection) (> (length collection) 2))
     (list-ref collection 2))
    ;; Handle non-empty vectors with at least three elements
    ((and (vector? collection) (> (vector-length collection) 2))
     (vector-ref collection 2))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 2))
         (and (vector? collection) (<= (vector-length collection) 2)))
     (begin
       (warning-message "third-item: Collection has fewer than 3 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "third-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Retrieve the last element of a list or vector. Warns if the input is invalid or empty.
(define (last-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (if (null? (cdr collection))
         (car collection)  ;; Base case: only one element left, return it.
         (last-item (cdr collection))))  ;; Recursive call on the rest of the list.
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection (- (vector-length collection) 1)))
    ;; Warn if the collection is empty
    ((or (null? collection)
         (and (vector? collection) (<= (vector-length collection) 0)))
     (begin
       (warning-message "last-item: Collection is empty.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "last-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cdr' function into a descriptive term, remove the head.
;; Warns if the input is not a list or is empty.
(define (list-remove-head list)
  (if (and (list? list) (not (null? list)))
    (cdr list)
    (begin
      (warning-message "list-remove-head: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Return the list without the last item. Warns if input is not a valid list or has fewer than 2 elements.
(define (list-before-last-item list)
  (if (and (list? list) (not (null? list)))
    (if (> (length list) 1)
      (reverse (list-remove-head (reverse list)))
      (begin
        (warning-message "list-before-last-item: List has fewer than 2 elements.")
        '()))
    (begin
      (warning-message "list-before-last-item: Expected a non-empty list, but received: " list)
      #f)))

;; Purpose: Unwrap a (()) and handle simple edge cases. Warns if the input is not a list or does not match the pattern.
(define (lists->list list)
  (if (list? list)
    (if (and (list? (car list)) (null? (cdr list)))
      (car list)
      (begin
        (warning-message "lists->list: Input is not a single-element nested list, returning the original list.")
        list))
    (begin
      (warning-message "lists->list: Expected a list, but received: " list)
      #f)))

;; Purpose: Unwrap a single-element () list or handle empty lists. Warns if the input is not a valid list or not a single-element list.
(define (list->item list)
  (cond
    ((null? list)
     (debug-message "list->item: Input is an empty list, returning '()")
     '())
    ((and (list? list) (not (list? (car list))) (= 1 (length list)))
     (car list))
    (else
     (debug-message "list->item: Input is not a single-element list, returning the list.")
     list)))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a message in a dialog box.
(define (message-box msg)
  (gimp-message-set-handler 0)
  (message msg "\n")
  (gimp-message-set-handler 2))

;; Purpose: Display a debug message.
(define (debug-message . items)
  (when debug (message "> " (apply concat items))))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
    #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: Display a message in a box and exit the script.
(define (exit-message msg)
  (message-box msg)
  (quit))

;; Purpose: Increments `current` value and wraps around to 1 if it exceeds `max-value`.
(define (wrap-around-increment current max-value)
  (let ((new-value (+ current 1)))
    (if (> new-value max-value)
        1
        new-value)))

;; =============================================================================
;; Purpose: Parasite Management Functions
;;
;; This is a collection of functions designed to manage the attachment, retrieval,
;; and removal of parasite data. Parasites store custom data in different scopes
;; (global, image, or item). These functions handle setting, getting, and detaching
;; parasites, as well as serializing and deserializing data for storage.
;;
;; Additionally, helper functions are provided for working with parasite scopes,
;; validating data, and processing parasite information.
;; =============================================================================

;; Constants for Parasites
(define PARASITE_ATTACH_MODE_SESSION_NO_UNDO 0)
(define PARASITE_ATTACH_MODE_PERSIST_NO_UNDO 1)
(define PARASITE_ATTACH_MODE_SESSION_WITH_UNDO 2)
(define PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO 3)

;; Globals for Parasites
(define parasite-attach-mode PARASITE_ATTACH_MODE_SESSION_NO_UNDO)

;; Purpose: Plug-in data in the form of parasites will persist between sessions
(define (attribute-attachment-mode-persist)
  (context-set-parasite-attach-mode 'persist #f))

;; Purpose: Sets the parasite attach mode based on the provided mode (session or persist)
;;          and whether undo is enabled. Ensures that a valid symbol is passed for mode.
(define (context-set-parasite-attach-mode mode undo-attach)
  (cond
    ((parasite-attach-mode-invalid? mode)
     (warning-message "Invalid attach mode. Expected 'session or 'persist."))
    ((eq? mode 'session)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_SESSION_WITH_UNDO
                                    PARASITE_ATTACH_MODE_SESSION_NO_UNDO)) #t)
    ((eq? mode 'persist)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO
                                    PARASITE_ATTACH_MODE_PERSIST_NO_UNDO)) #t)))

;; Purpose: Checks a correct attachment mode has been used
(define (parasite-attach-mode-invalid? mode)
  (not (or (eq? mode 'session) (eq? mode 'persist))))

;; Purpose: Returns #t if the parasite is found, otherwise #f
;; (has-attribute? "global-exampleNumber")
;; (has-attribute? "image-exampleBoolean" image)
(define (has-attribute? . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let ((parasite-data (retrieve-parasite-data parasite-info)))
      (if parasite-data #t #f)))
    #f))

;; Purpose: Sets the data stored in a parasite.
;; (set-attr "global-exampleNumber" 123)
;; (set-attr "image-exampleBoolean" image #t)
(define (set-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let* ((data-element (last-item parasite-info))
            (string-data (serialize-parasite-data data-element))
            (parasite-name (first-item parasite-info))
            (image-or-item (get-image-or-item parasite-info))
            (scope (determine-parasite-scope parasite-name image-or-item)))

        (attach-parasite scope parasite-name image-or-item string-data)))
    #f))

;; Purpose: Retrieves the data stored in a parasite.
;; (get-attr "global-exampleNumber")  ; Returns 123
;; (get-attr "image-exampleBoolean" image) ; Returns #t
(define (get-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let ((parasite-data (retrieve-parasite-data parasite-info)))
      (if parasite-data
        (process-parasite-data (first-item parasite-info) parasite-data)
        #f)))
    #f))

;; Purpose: Returns a single list of all the global parasites
(define (global-get-parasite-list)
  (lists->list (gimp-get-parasite-list)))
;; Purpose: Returns a single list of all the image parasites
(define (image-get-parasite-list image)
  (lists->list (gimp-image-get-parasite-list image)))

;; Purpose: Returns a single list of all the item parasites
(define (item-get-parasite-list item)
  (lists->list (gimp-item-get-parasite-list item)))

;; Purpose: Get a single list containing the named global parasite
(define (global-get-parasite parasite-name)
  (lists->list (gimp-get-parasite parasite-name)))

;; Purpose: Get a single list containing the named image parasite
(define (image-get-parasite image parasite-name)
  (lists->list (gimp-image-get-parasite image parasite-name)))

;; Purpose: Get a single list containing the named item parasite
(define (item-get-parasite item parasite-name)
  (lists->list (gimp-item-get-parasite item parasite-name)))

;; Purpose: Get a global parasite data string
(define (global-get-parasite-data parasite-name)
  (third-item (global-get-parasite parasite-name)))

;; Purpose: Get the image parasite data string
(define (image-get-parasite-data image parasite-name)
  (third-item (image-get-parasite image parasite-name)))

;; Purpose: Get a the item parasite data string
(define (item-get-parasite-data item parasite-name)
  (third-item (item-get-parasite item parasite-name)))

;; Purpose: Check that the custom parasite format is used in the arguments
(define (validate-parasite-info parasite-info)
  (if (or (not (list? parasite-info))
          (null? parasite-info)
          (not (string? (first-item parasite-info))))
    (warning-message "validate-parasite-info: parasite-info must be a non-empty
                    list with a string parasite name")))

;; Purpose: Attaches the parasite to the appropriate scope.
(define (attach-parasite scope parasite-name image-or-item data)
  (cond
    ((equal? scope "global")
       (gimp-attach-parasite (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "image")
       (gimp-image-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "item")
       (gimp-item-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    (else
       (warning-message "attach-parasite: Invalid scope provided.\n"
                        "Expected 'global', 'image', or 'item'."))))

;; Purpose: Extracts the image or item from parasite-info, defaults to #f if missing.
(define (get-image-or-item parasite-info)
  (if (> (length parasite-info) 1)
      (second-item parasite-info)
      #f))

;; Purpose: Determines the scope of the parasite based on its name and image-or-item.
(define (determine-parasite-scope parasite-name image-or-item)
  (let ((scope (first-item (strbreakup parasite-name "-"))))
    (cond
      ((or (equal? scope "image") (equal? scope "item"))
       (if image-or-item
           scope
           (warning-message "determine-parasite-scope: Extra parameter is
                             required for image/item scope.")))
      ((equal? scope "global")
       scope)
      (else
       (warning-message "determine-parasite-scope: Unknown scope.")))))

;; Purpose: Converts native data types to their string representations for storage.
(define (serialize-parasite-data data)
  (cond
    ((string? data) data)
    ((number? data) (number->string data))
    ((boolean? data) (if data "#t" "#f"))
    (else
     (warning-message "serialize-parasite-data: Unsupported data type."))))

;; Purpose: Identifies the type of the parasite data.
(define (detect-parasite-type parasite-data)
  (cond
    ((not (equal? (string->number parasite-data) #f)) "number")
    ((or (equal? parasite-data "#t") (equal? parasite-data "#f")) "boolean")
    (else "string")))

;; Purpose: Converts serialized data back to its native type based on detected type.
(define (get-parasite-return-value data-type parasite-data)
  (cond
    ((equal? data-type "number") (string->number parasite-data))
    ((equal? data-type "boolean") (equal? parasite-data "#t"))
    ((equal? data-type "string") parasite-data)
    (else (warning-message "get-parasite-return-value: Invalid data type."))))

;; Purpose: Retrieves the parasite data based on paraItem, returns #f if the
;;          parasite data is not found
(define (retrieve-parasite-data parasite-info)
  (let* ((parasite-name (first-item parasite-info))
         (image-or-item (get-image-or-item parasite-info))
         (scope (determine-parasite-scope parasite-name image-or-item))
         (parasite-list (get-parasite-list-from-scope scope image-or-item))
         (parasite-data (find-parasite-data parasite-name scope image-or-item parasite-list)))
    (if parasite-data parasite-data #f)))

;; Purpose: Searches for parasite data in the parasite list.
(define (find-parasite-data parasite-name scope image-or-item parasite-list)
  (cond
    ((null? parasite-list)
     (debug-message "Parasite not found: " parasite-name)
     #f)
    ((equal? (first-item parasite-list) parasite-name)
     (get-parasite-data-from-scope scope parasite-name image-or-item))
    (else
     (find-parasite-data parasite-name scope image-or-item (list-remove-head parasite-list)))))

;; Purpose: Retrieves parasite data based on its scope.
(define (get-parasite-data-from-scope scope parasite-name image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-data parasite-name))
    ((equal? scope "image") (image-get-parasite-data image-or-item parasite-name))
    ((equal? scope "item") (item-get-parasite-data image-or-item parasite-name))
    (else (warning-message "get-parasite-data-from-scope: Invalid scope."))))

;; Purpose: Retrieves the list of parasites based on scope and imageOrItem.
(define (get-parasite-list-from-scope scope image-or-item)
  (cond
    ((equal? scope "global") (global-get-parasite-list))
    ((equal? scope "image") (image-get-parasite-list image-or-item))
    ((equal? scope "item") (item-get-parasite-list image-or-item))
    (else '())))

;; Purpose: Detects parasite data type and returns it in native form.
(define (process-parasite-data parasite-name parasite-data)
  (let* ((data-type (detect-parasite-type parasite-data))
         (return-value (get-parasite-return-value data-type parasite-data)))
    (debug-message parasite-name " : type : " data-type " : " return-value)
    return-value))

;; =============================================================================
;; Purpose: Image Handling Utilities
;;
;; This module provides a collection of image-related utility functions. These
;; functions can be used to retrieve information about images, such as file paths,
;; short file names, base names, and image states (e.g., whether the image is valid
;; or has been modified). The utilities are designed to help manage image-related
;; operations efficiently across various use cases.
;; =============================================================================

;; Returns #t if an image has a file, #f if untitled.
(define (image-has-file? image)
  (let ((file-path (list->item (gimp-image-get-file image))))
    ;; (debug-message "Checking if image has file: " file-path )
    (not (equal? file-path ""))))

;; Purpose: Returns #t if the image has no file, its never been saved.
(define (image-has-no-file image)
  (not (image-has-file? image)))

;; Purpose: Return the absolute file path of an image
(define (image-get-absolute-file-path image)
  (let ((file-path (list->item (gimp-image-get-file image))))
    ;; (debug-message "Absolute file path: " file-path)
    file-path))

;; Purpose: Retrieves the short file name (image.ext) of an image from its full path,
;;          or returns an empty string if no file is associated with the image.
(define (image-get-short-file-name image)
  (if (image-has-file? image)
    (begin
      (let* ((file-path (image-get-absolute-file-path image))
             (parts (strbreakup file-path DIR-SEPARATOR)))
        ;; (debug-message "Short file : " (last-item parts))
        (last-item parts)))
    (begin
      (debug-message "Image is untitled, returning empty string")
      "")))

;; Purpose: Retrieves the base name of an image (without the file extension)
;;          or returns an "Untitled" if no file is associated.
(define (image-get-base-name image)
  (if (image-has-file? image)
    (begin
      (let* ((file (image-get-absolute-file-path image))
             (short-name (image-get-short-file-name image))
             (parts (strbreakup short-name "."))
             (base-parts (if (null? (list-remove-head parts))
                            parts
                            (list-before-last-item parts))))
        ;; (debug-message "Base name (without extension): " base-parts)
        (string-join base-parts ".")))
    (begin
      (debug-message "Image is untitled")
      "Untitled")))

;; Purpose: Retrieves the directory path (file location) of an image without the file name.
;;          Returns an empty string if no file is associated with the image.
(define (image-get-file-location image)
  (if (image-has-file? image)
    (begin
      (let* ((file-path (image-get-absolute-file-path image))
             (short-file-name (image-get-short-file-name image))
             (file-location (first-item (strbreakup file-path short-file-name))))
        ;; (debug-message "File location (without name): " file-location)
        file-location))
    (begin
      (debug-message "Image is untitled, it has no location")
      "")))

;; =============================================================================
;; Purpose: File and Directory Utilities for Script-Fu Plugins
;;
;; This module provides a collection of file related functions
;; =============================================================================

;; Purpose: Makes a directory in the "home" directory with a string "/path/like/this"
(define (make-dir-path path)
  (let* ((pathParts (cdr (strbreakup path "/")))  ; Skip the empty element ?
         (currentPath (string-append "/" (first-item pathParts)))) ; /home
    (debug-message "Initial path parts: " pathParts)
    ;; Create the rest of the directories step-by-step
    (for-each
     (lambda (part)
       (set! currentPath (string-append currentPath "/" part))
       (debug-message "Creating directory: " currentPath)
       (dir-make currentPath))
     (list-remove-head pathParts))))

