#!/usr/bin/env gimp-script-fu-interpreter-3.0
;; =============================================================================
;; Purpose: Helper for the Incremental Save plugin
;;
;; A plugin that provides a GUI for configuring the 'Incremental Save' plugin.
;; Users can set the maximum number of incremental saves per image before they
;; wrap around and replace the initial saves.
;;
;; Access this plugin from File -> Incremental Save -> Set Wrap Number
;; =============================================================================

;; Copyright 2024, Mark Sweeney, Under GNU GENERAL PUBLIC LICENSE Version 3

;; Global variables
(define debug #f)

;; =============================================================================
;; Main
(define (script-fu-incremental-save-set-up image drawables max-saves)
  (attribute-attachment-mode-persist)
  (set-attr "image-incremental-max-saves" image max-saves))

;; -----------------------------------------------------------------------------

(script-fu-register-filter "script-fu-incremental-save-set-up"
 "Set Image Save Total"
 "Edit the maximum number of incremental saves stored by the image before wrapping"
 "Mark Sweeney"
 "Under GNU GENERAL PUBLIC LICENSE Version 3"
 "2024"
 "*"
 SF-ONE-OR-MORE-DRAWABLE
 SF-ADJUSTMENT "Maximum nu_mber of saves" (list 6 1 20 1 10 0 SF-SPINNER))
(script-fu-menu-register "script-fu-incremental-save-set-up" "<Image>/File/Incremental Save")

;; =============================================================================
;; Library Functions

;; Purpose: Utility operations and helper functions for Script-Fu plugins
;;
;; This is a collection of utility functions for common operations
;; in Script-Fu plugins, including path management, list manipulation, messaging,
;; and control flow utilities. These functions can be loaded into any Script-Fu
;; plugin to provide reusable utilities, helping to streamline the development of plugins.
;; =============================================================================

(define warning #t)

;; Purpose: Returnss the first item of a list or vector. Warns if the input is
;;          invalid or empty.
(define (first-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (list-ref collection 0))
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection 0))
    ;; Invalid or empty input
    (else
     (begin
       (warning-message "first-item: Expected a non-empty list or vector, but received: " collection)
       #f))))

;; Purpose: Abstract the 'cadr' function into a descriptive term.
;; Returns the second item of a list or vector. Warns if the input is invalid or too short.
(define (second-item collection)
  (cond
    ;; Handle non-empty lists with at least two elements
    ((and (list? collection) (> (length collection) 1))
     (list-ref collection 1))
    ;; Handle non-empty vectors with at least two elements
    ((and (vector? collection) (> (vector-length collection) 1))
     (vector-ref collection 1))
    ;; Warn if the collection is too short
    ((or (null? collection)
         (and (list? collection) (<= (length collection) 1))
         (and (vector? collection) (<= (vector-length collection) 1)))
     (begin
       (warning-message "second-item: Collection has fewer than 2 elements.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "second-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Retrieve the last element of a list or vector. Warns if the input is invalid or empty.
(define (last-item collection)
  (cond
    ;; Handle non-empty lists
    ((and (list? collection) (not (null? collection)))
     (if (null? (cdr collection))
         (car collection)  ;; Base case: only one element left, return it.
         (last-item (cdr collection))))  ;; Recursive call on the rest of the list.
    ;; Handle non-empty vectors
    ((and (vector? collection) (> (vector-length collection) 0))
     (vector-ref collection (- (vector-length collection) 1)))
    ;; Warn if the collection is empty
    ((or (null? collection)
         (and (vector? collection) (<= (vector-length collection) 0)))
     (begin
       (warning-message "last-item: Collection is empty.")
       #f))
    ;; Invalid input
    (else
     (begin
       (warning-message "last-item: Expected a list or vector, but received: " collection)
       #f))))

;; Purpose: Display a message in GIMP's message console.
(define (message . items)
  (gimp-message (apply concat items)))

;; Purpose: Display a warning message.
(define (warning-message . items)
  (if warning
    (message "Warning: " (apply concat items)))
  #f)

;; Purpose: Serialize a list of items into a single string, handling numbers,
;;          booleans, and nested lists.
(define (list->string list)
  (if (list? list)
      (string-append "list: \n" (string-join (map serialize-item list) "\n"))
      (warning-message "list->string: Input is not a list!")))

;; Purpose: Converts various Scheme data types (lists, vectors, pairs, etc.)
;;          into a string representation
(define (serialize-item item)
  (cond
    ((and (list? item) (null? item)) "\"\"")           ; Empty list
    ((and (string? item) (string=? item "")) "\"\"")   ; Empty string
    ((list? item) (list->string item))                ; Nested list
    ((vector? item)                                   ; Handle vectors
     (string-append "#("
                    (string-join (map serialize-item (vector->list item)) " ")
                    ")"))
    ((pair? item)                                     ; Handle pairs
     (string-append "("
                    (serialize-item (car item))
                    " . "
                    (serialize-item (cdr item))
                    ")"))
    ((number? item) (number->string item))            ; Numbers
    ((symbol? item) (symbol->string item))            ; Symbols
    ((boolean? item) (if item "#t" "#f"))             ; Booleans
    ((string? item) item)                             ; Strings
    (else (warning-message "serialize-item: Unsupported item type!" item))))

;; Purpose: Concatenate multiple items into a single string, handling numbers,
;;          booleans, lists, and other types.
(define (concat . items)
  (apply string-append (map serialize-item items)))

;; Purpose: Join a list of strings with a delimiter
(define (string-join list delimiter)
  (let loop ((remaining list)
             (result ""))
    (cond
      ((null? remaining) result)  ;; Base case: no more elements
      ((null? (cdr remaining))    ;; Last element: add without delimiter
       (string-append result (car remaining)))
      (else
       (loop (cdr remaining)
             (string-append result (car remaining) delimiter))))))

;; Purpose: is the color a shade of gray?
;; =============================================================================
;; Purpose: Parasite Management Functions
;;
;; This is a collection of functions designed to manage the attachment, retrieval,
;; and removal of parasite data. Parasites store custom data in different scopes
;; (global, image, or item). These functions handle setting, getting, and detaching
;; parasites, as well as serializing and deserializing data for storage.
;;
;; Additionally, helper functions are provided for working with parasite scopes,
;; validating data, and processing parasite information.
;; =============================================================================

;; Constants for Parasites
(define PARASITE_ATTACH_MODE_SESSION_NO_UNDO 0)
(define PARASITE_ATTACH_MODE_PERSIST_NO_UNDO 1)
(define PARASITE_ATTACH_MODE_SESSION_WITH_UNDO 2)
(define PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO 3)

;; Globals for Parasites
(define parasite-attach-mode PARASITE_ATTACH_MODE_SESSION_NO_UNDO)

;; Purpose: Plug-in data in the form of parasites will persist between sessions
(define (attribute-attachment-mode-persist)
  (context-set-parasite-attach-mode 'persist #f))

;; Purpose: Sets the parasite attach mode based on the provided mode (session or persist)
;;          and whether undo is enabled. Ensures that a valid symbol is passed for mode.
(define (context-set-parasite-attach-mode mode undo-attach)
  (cond
    ((parasite-attach-mode-invalid? mode)
     (warning-message "Invalid attach mode. Expected 'session or 'persist."))
    ((eq? mode 'session)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_SESSION_WITH_UNDO
                                    PARASITE_ATTACH_MODE_SESSION_NO_UNDO)) #t)
    ((eq? mode 'persist)
     (set! parasite-attach-mode (if undo-attach
                                    PARASITE_ATTACH_MODE_PERSIST_WITH_UNDO
                                    PARASITE_ATTACH_MODE_PERSIST_NO_UNDO)) #t)))

;; Purpose: Checks a correct attachment mode has been used
(define (parasite-attach-mode-invalid? mode)
  (not (or (eq? mode 'session) (eq? mode 'persist))))

;; Purpose: Sets the data stored in a parasite.
;; (set-attr "global-exampleNumber" 123)
;; (set-attr "image-exampleBoolean" image #t)
(define (set-attr . parasite-info)
  (if (validate-parasite-info parasite-info)
    (begin
      (let* ((data-element (last-item parasite-info))
            (string-data (serialize-parasite-data data-element))
            (parasite-name (first-item parasite-info))
            (image-or-item (get-image-or-item parasite-info))
            (scope (determine-parasite-scope parasite-name image-or-item)))

        (attach-parasite scope parasite-name image-or-item string-data)))
    #f))

;; Purpose: Check that the custom parasite format is used in the arguments
(define (validate-parasite-info parasite-info)
  (if (or (not (list? parasite-info))
          (null? parasite-info)
          (not (string? (first-item parasite-info))))
    (warning-message "validate-parasite-info: parasite-info must be a non-empty
                    list with a string parasite name")))

;; Purpose: Attaches the parasite to the appropriate scope.
(define (attach-parasite scope parasite-name image-or-item data)
  (cond
    ((equal? scope "global")
       (gimp-attach-parasite (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "image")
       (gimp-image-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    ((equal? scope "item")
       (gimp-item-attach-parasite image-or-item (list parasite-name parasite-attach-mode data))
       #t)
    (else
       (warning-message "attach-parasite: Invalid scope provided.\n"
                        "Expected 'global', 'image', or 'item'."))))

;; Purpose: Extracts the image or item from parasite-info, defaults to #f if missing.
(define (get-image-or-item parasite-info)
  (if (> (length parasite-info) 1)
      (second-item parasite-info)
      #f))

;; Purpose: Determines the scope of the parasite based on its name and image-or-item.
(define (determine-parasite-scope parasite-name image-or-item)
  (let ((scope (first-item (strbreakup parasite-name "-"))))
    (cond
      ((or (equal? scope "image") (equal? scope "item"))
       (if image-or-item
           scope
           (warning-message "determine-parasite-scope: Extra parameter is
                             required for image/item scope.")))
      ((equal? scope "global")
       scope)
      (else
       (warning-message "determine-parasite-scope: Unknown scope.")))))

;; Purpose: Converts native data types to their string representations for storage.
(define (serialize-parasite-data data)
  (cond
    ((string? data) data)
    ((number? data) (number->string data))
    ((boolean? data) (if data "#t" "#f"))
    (else
     (warning-message "serialize-parasite-data: Unsupported data type."))))

